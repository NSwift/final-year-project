//
//  ViewController.h
//  OpenCVTests2
//
//  Created by Daniel Parkin on 04/10/2014.
//  Copyright (c) 2014 DanielParkin. All rights reserved.
//

#import <opencv2/highgui/cap_ios.h>
#include "opencv2/opencv.hpp"
#include "opencv2/core/core_c.h"
#include "opencv2/core/core.hpp"
#include "opencv2/flann/miniflann.hpp"
#include "opencv2/imgproc/imgproc_c.h"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/video/video.hpp"
#include "opencv2/features2d/features2d.hpp"
#include "opencv2/objdetect/objdetect.hpp"
#include "opencv2/calib3d/calib3d.hpp"
#include "opencv2/ml/ml.hpp"
#include "opencv2/highgui/highgui_c.h"
#include "opencv2/highgui/highgui.hpp"
#import <UIKit/UIKit.h>
using namespace cv;

@interface ViewController : UIViewController <CvVideoCameraDelegate>
{
    CvVideoCamera* videoCamera;
    NSString* _hcFrontalFace;
    NSString* _hcEyeTreeGlasses;
    CascadeClassifier face_cascade;
    CascadeClassifier mouth_cascade;
    Mat face;
}

@property (nonatomic, retain) CvVideoCamera* videoCamera;

- (IBAction)getFaceTapped:(id)sender;
- (void)layoutPreviewLayer;
- (UIImage *)UIImageFromCVMat:(Mat)cvMat;

@end

