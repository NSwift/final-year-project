//
//  AppDelegate.h
//  OpenCVTests2
//
//  Created by Daniel Parkin on 04/10/2014.
//  Copyright (c) 2014 DanielParkin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

