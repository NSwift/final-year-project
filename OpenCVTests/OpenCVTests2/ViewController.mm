//
//  ViewController.m
//  OpenCVTests2
//
//  Created by Daniel Parkin on 04/10/2014.
//  Copyright (c) 2014 DanielParkin. All rights reserved.
//

#import "ViewController.h"

#define DEGREES_RADIANS(angle) ((angle) / 180.0 * M_PI)

@interface ViewController ()

@property (weak, nonatomic) IBOutlet UIImageView *ivVideoFeed;

@end

@implementation ViewController

@synthesize videoCamera, ivVideoFeed;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _hcFrontalFace = [[NSBundle mainBundle] pathForResource:@"haarcascade_frontalface_alt2.xml" ofType:nil];
    
    _hcEyeTreeGlasses = [[NSBundle mainBundle] pathForResource:@"haarcascade_mcs_mouth.xml" ofType:nil];
    
    face_cascade.load([_hcFrontalFace UTF8String]);
    mouth_cascade.load([_hcEyeTreeGlasses UTF8String]);
    
    self.videoCamera = [[CvVideoCamera alloc] initWithParentView:ivVideoFeed];
    self.videoCamera.delegate = self;
    self.videoCamera.defaultAVCaptureDevicePosition = AVCaptureDevicePositionFront;
    self.videoCamera.defaultAVCaptureSessionPreset = AVCaptureSessionPresetHigh;
    self.videoCamera.defaultAVCaptureVideoOrientation = AVCaptureVideoOrientationPortraitUpsideDown;
    
    self.videoCamera.defaultFPS = 30;
    self.videoCamera.grayscaleMode = NO;
    
    [self.videoCamera start];
    [self layoutPreviewLayer];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (IBAction)getFaceTapped:(id)sender {
    //self.ivFace.image = [self UIImageFromCVMat:face];
}

- (void)layoutPreviewLayer
{
    CALayer* layer = self.ivVideoFeed.layer;
    CGRect bounds = self.ivVideoFeed.layer.bounds;
    int rotation_angle = 0;
        
    switch (self.videoCamera.defaultAVCaptureVideoOrientation) {
        case AVCaptureVideoOrientationLandscapeRight:
            rotation_angle = 180;
            break;
        case AVCaptureVideoOrientationPortraitUpsideDown:
            rotation_angle = 270;
            break;
        case AVCaptureVideoOrientationPortrait:
            rotation_angle = 90;
        case AVCaptureVideoOrientationLandscapeLeft:
            break;
        default:
            break;
    }

    layer.affineTransform = CGAffineTransformMakeRotation( DEGREES_RADIANS(rotation_angle) );
    layer.bounds = bounds;

}

-(UIImage *)UIImageFromCVMat:(Mat)cvMat
{
    NSData *data = [NSData dataWithBytes:cvMat.data length:cvMat.elemSize()*cvMat.total()];
    CGColorSpaceRef colorSpace;
    
    if (cvMat.elemSize() == 1) {
        colorSpace = CGColorSpaceCreateDeviceGray();
    } else {
        colorSpace = CGColorSpaceCreateDeviceRGB();
    }
    
    CGDataProviderRef provider = CGDataProviderCreateWithCFData((__bridge CFDataRef)data);
    
    // Creating CGImage from cv::Mat
    CGImageRef imageRef = CGImageCreate(cvMat.cols,                                 //width
                                        cvMat.rows,                                 //height
                                        8,                                          //bits per component
                                        8 * cvMat.elemSize(),                       //bits per pixel
                                        cvMat.step[0],                              //bytesPerRow
                                        colorSpace,                                 //colorspace
                                        kCGImageAlphaNone|kCGBitmapByteOrderDefault,//bitmap info
                                        provider,                                   //CGDataProviderRef
                                        NULL,                                       //decode
                                        false,                                      //should interpolate
                                        kCGRenderingIntentDefault                   //intent
                                        );
    
    
    // Getting UIImage from CGImage
    UIImage *finalImage = [UIImage imageWithCGImage:imageRef];
    CGImageRelease(imageRef);
    CGDataProviderRelease(provider);
    CGColorSpaceRelease(colorSpace);
    
    return finalImage;
}

#pragma mark - Protocol CvVideoCameraDelegate

#ifdef __cplusplus

// delegate method for processing image frames
- (void)processImage:(Mat&)image {
    
    std::vector<cv::Rect> faces;
    Mat image_gray;
    
    cvtColor( image, image_gray, CV_BGR2GRAY );

    // detect the face region using 'haarcascade_frontalface_alt2.xml'
    face_cascade.detectMultiScale(image_gray,
                                  faces,
                                  1.7,
                                  0,
                                  0 | CV_HAAR_FIND_BIGGEST_OBJECT,
                                  cv::Size(30, 30)
                                  );
    
    // iterate over each face region detected and attempt to
    // detect mouth region
    for(size_t i = 0; i < faces.size(); i++)
    {
        cv::Point center(faces[i].x + faces[i].width * 0.5,
                         faces[i].y + faces[i].height);
        
        cv::ellipse(image, center,
                    cv::Size(faces[i].width * 0.6,
                             faces[i].height * 0.6),
                    0, 0, 360,
                    Scalar(255, 0, 255),
                    4, 8, 0);
        
        Mat faceROI = image(faces[i]);
        
        if(!faceROI.empty()) {

            std::vector<cv::Rect> mouth;
            
            // use mouth haar cascade to detect mouth areas in
            // the previously detected face area
            // low accuracy settings used to increase detection speed
            // detect mouth using 'haarcascade_mcs_mouth.xml'
            mouth_cascade.detectMultiScale(
                                           faceROI,
                                           mouth,
                                           1.7,
                                           0, 0 | CV_HAAR_FIND_BIGGEST_OBJECT,
                                           cv::Size(30, 30)
                                           );
            
            // draw a circle for each area detected as the mouth region
            for( size_t j = 0; j < mouth.size(); j++ )
            {
                // create point to draw mouth circle relative to the face
                cv::Point center(faces[i].x + mouth[j].x + mouth[j].width*0.5,
                                 faces[i].y + mouth[j].y + mouth[j].height*0.5);
                
                // draw circle over point where mouth region was detected
                int radius = cvRound((mouth[j].width + mouth[j].height)*0.25);
                circle(image, center, radius, Scalar( 255, 0, 0 ), 4, 8, 0 );
            }
            
        }

    }
  
    
    
    
    
/*
    std::vector<cv::Rect> faces;
    Mat image_gray;
    
    cvtColor( image, image_gray, CV_BGR2GRAY );
    equalizeHist( image_gray, image_gray );
    
    //-- Detect faces
    face_cascade.detectMultiScale( image_gray, faces, 1.1, 2, 0|CV_HAAR_FIND_BIGGEST_OBJECT, cv::Size(30, 30) );
    
    for( size_t i = 0; i < faces.size(); i++ )
    {
        cv::Point center( faces[i].x + faces[i].width * 0.5, faces[i].y + faces[i].height );
        cv::ellipse( image, center, cv::Size( faces[i].width * 0.6, faces[i].height * 0.6), 0, 0, 360, Scalar( 255, 0, 255 ), 4, 8, 0 );
        
        Mat faceROI = image( faces[i] );
        
        if(!faceROI.empty()) {
            //self.ivFace.image = [self UIImageFromCVMat:faceROI];
        }
        else {
            printf("Face is empty");
        }
        
        std::vector<cv::Rect> eyes;
    
        //-- In each face, detect eyes
        eye_cascade.detectMultiScale( faceROI, eyes, 1.1, 2, 0 |CV_HAAR_FIND_BIGGEST_OBJECT, cv::Size(30, 30) );
        
        for( size_t j = 0; j < eyes.size(); j++ )
        {
            //printf("test");
            cv::Point center( faces[i].x + eyes[j].x + eyes[j].width*0.5, faces[i].y + eyes[j].y + eyes[j].height*0.5 );
            int radius = cvRound( (eyes[j].width + eyes[j].height)*0.25 );
            circle( image, center, radius, Scalar( 255, 0, 0 ), 4, 8, 0 );
        }
        
    }
*/
}
#endif

@end
