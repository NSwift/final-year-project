//
//  main.m
//  OpenCVTests2
//
//  Created by Daniel Parkin on 04/10/2014.
//  Copyright (c) 2014 DanielParkin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
