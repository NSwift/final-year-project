//
//  AnswerPresenterOption.m
//  DynamicAssessmentFramework
//
//  Created by Daniel Parkin on 19/02/2015.
//  Copyright (c) 2015 DanielParkin. All rights reserved.
//

#import "AnswerPresenterOption.h"
#import "CustomAnswerPresenter.h"


@implementation AnswerPresenterOption

@dynamic assetURL;
@dynamic height;
@dynamic width;
@dynamic x;
@dynamic y;
@dynamic parentPresenter;

@end
