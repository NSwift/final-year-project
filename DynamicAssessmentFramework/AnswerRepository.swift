//
//  AnswerRepository.swift
//  DynamicAssessmentFramework
//
//  Created by Daniel Parkin on 08/01/2015.
//  Copyright (c) 2015 DanielParkin. All rights reserved.
//

class AnswerRepository: CoreDataRepository {
    
    override init() {
        super.init()
    }
    
    func getZombieAnswers() -> [Answer] {
        
        let request = NSFetchRequest(entityName: "Answer")
        let predicate = NSPredicate(format: "parentQuestion == nil")
        request.returnsObjectsAsFaults = false
        request.predicate = predicate
        
        var results:NSArray = self.context.executeFetchRequest(request, error: nil)!
        
        return results as [Answer]
    }
    
}
