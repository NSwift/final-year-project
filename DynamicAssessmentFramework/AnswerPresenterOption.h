//
//  AnswerPresenterOption.h
//  DynamicAssessmentFramework
//
//  Created by Daniel Parkin on 19/02/2015.
//  Copyright (c) 2015 DanielParkin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class CustomAnswerPresenter;

@interface AnswerPresenterOption : NSManagedObject

@property (nonatomic, retain) NSString * assetURL;
@property (nonatomic, retain) NSNumber * height;
@property (nonatomic, retain) NSNumber * width;
@property (nonatomic, retain) NSNumber * x;
@property (nonatomic, retain) NSNumber * y;
@property (nonatomic, retain) CustomAnswerPresenter *parentPresenter;

@end
