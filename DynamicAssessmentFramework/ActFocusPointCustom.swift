//
//  ActCountdown.swift
//  DynamicAssessmentFramework
//
//  Created by Daniel Parkin on 21/08/2014.
//  Copyright (c) 2014 Echoyo. All rights reserved.
//

import Foundation
import UIKit

class ActFocusPointCustom:NSObject, AssessmentActProtocol, MediaPlayerDelegate {
    
    weak var delegate:ActResponseDelegate?
    
    var seconds:Int = 0
    var timer:NSTimer!
    var controls:[NSObject]!

    private var _focusPoint:FocusPoint!
    
    private let _screenBounds = ScreenBounds()
    private let _colorHelper = ColorHelper()
    private let _mediaPlayer:MediaPlayerImageNoCompression!
    
    init(focusPoint:FocusPoint, seconds:Int, delegate:ActResponseDelegate) {
        super.init()
        self.seconds = seconds
        self.timer = NSTimer()
        self.delegate = delegate
        self.controls = []
        self._focusPoint = focusPoint
        self._mediaPlayer = MediaPlayerImageNoCompression(appState: ApplicationState.Review, delegate: self)
    }
    
    func beginDisplayOnView(view: UIView) {
        
        let actView = UIView(frame: _screenBounds.bounds())
        var imageView = UIImageView()
        
        let colorName = _focusPoint.backgroundColor
        actView.backgroundColor = _colorHelper.colorNamed(colorName)
        
        let viewFrame = CGRect(
            x: CGFloat(_focusPoint.x),
            y: CGFloat(_focusPoint.y),
            width: CGFloat(_focusPoint.width),
            height: CGFloat(_focusPoint.height)
        )
        
        imageView.frame = viewFrame
        
        var _assetURL = NSURL(string: _focusPoint.assetURL)
        _mediaPlayer.playMedia(_assetURL!, onView: imageView)
        
        // do rotation
        /*
        imageView.transform = CGAffineTransformRotate(
            imageView.transform,
            CGFloat(_focusPoint.rotation)
        )
        */
        
        actView.addSubview(imageView)
        
        view.addSubview(actView)
        
        // begin timer
        self.timer = NSTimer.scheduledTimerWithTimeInterval(
            1.0,
            target: self,
            selector: "updateTimer:",
            userInfo: nil,
            repeats: true)
    }
    
    func updateTimer(timer:NSTimer) {
        if seconds > 1 {
            seconds--
        }
        else {
            timer.invalidate()
            let status = ActResponseStatus.CompleteGetNextAct
            let response = ActResponse(status: status, bundle: nil)
            delegate?.actDidRespond(response)
        }
    }
    
    // MARK: MediaPlayerDelegate
    func didFinishPlayingMedia() {
        // no implementation
    }
    
}