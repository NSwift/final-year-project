//
//  CustomAnswerPresenter.m
//  DynamicAssessmentFramework
//
//  Created by Daniel Parkin on 18/02/2015.
//  Copyright (c) 2015 DanielParkin. All rights reserved.
//

#import "CustomAnswerPresenter.h"
#import "AnswerPresenterOption.h"
#import "Question.h"


@implementation CustomAnswerPresenter

@dynamic name;
@dynamic questions;
@dynamic options;

@end
