//  CameraOperations.m
//
//  Created by Daniel Parkin on 19/08/2014.

#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>
#import <QuartzCore/QuartzCore.h>
#import <CoreMedia/CoreMedia.h>

@interface CameraOperations : NSObject <AVCaptureFileOutputRecordingDelegate>

@property BOOL didFinishRecording;
@property BOOL didCallBack;
@property int fileNameID;
@property int id;

// static integer used for camera id
+ (int) cameraId;
+ (void) setCameraId:(int)val;
+ (void) incrememtCameraId;

-(id)initWithDelegate:(id)delegate;
-(id)initWithPreviewEnabled:(BOOL)previewEnabled;
-(id)initWithRootLayer:(CALayer *)rootLayer;

-(void)setDelegate:(id)delegate;

-(void)rootLayer:(CALayer *)rootLayer;

-(void)startRecording;
-(NSURL*)stopRecording;

-(void)stopSession;

-(void)startPreview;


@end
