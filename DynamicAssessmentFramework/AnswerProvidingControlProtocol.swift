//
//  AnswerProvidingControlProtocol.swift
//  DynamicAssessmentFramework
//
//  Created by Daniel Parkin on 10/01/2015.
//  Copyright (c) 2015 DanielParkin. All rights reserved.
//

import Foundation
import UIKit

@objc
protocol AnswerProvidingControlProtocol {
    weak var answer:Answer? { get set }
    func provideAnswer() -> Answer
    func getFrame() -> CGRect
    func getView() -> UIView
}