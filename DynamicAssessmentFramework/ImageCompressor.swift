//
//  ImageCompressor.swift
//  DynamicAssessmentFramework
//
//  Created by Daniel Parkin on 03/03/2015.
//  Copyright (c) 2015 DanielParkin. All rights reserved.
//
import UIKit

class ImageCompressor {
    
    func compress(#image:UIImage, newSize:CGSize, quality:CGFloat) -> UIImage {
        
        let rect = CGRectMake(0, 0, newSize.width, newSize.height)
        UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
        image.drawInRect(rect)
        
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        let resizedImageData = UIImageJPEGRepresentation(newImage, quality)
        
        return UIImage(data: resizedImageData)!
    }
    
    func sizeFromView(#view:UIView) -> CGSize {
        return CGSize(width: view.frame.width, height: view.frame.height)
    }
    
}