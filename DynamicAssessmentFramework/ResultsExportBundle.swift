//
//  ResultsExportBundle.swift
//  DynamicAssessmentFramework
//
//  Created by Daniel Parkin on 16/02/2015.
//  Copyright (c) 2015 DanielParkin. All rights reserved.
//

import Foundation

class ResultsExportBundle {
    
    var data:NSData!
    var filename:String!
    var fileType:FileExportType
    var mimeType:String!
    
    init(data:NSData, filename:String, fileType:FileExportType) {
        self.data = data
        self.fileType = fileType
        self.filename = "\(filename).\(fileType.rawValue)"
        self.mimeType = generateMimeType()
    }
    
    private func generateMimeType() -> String {
        
        var mimeType:String = ""
        
        switch(self.fileType) {
        case FileExportType.CSV:
            self.mimeType = "text/csv"
        case FileExportType.TXT:
            self.mimeType = "text/plain"
        case FileExportType.PNG:
            self.mimeType = "image/png"
        case FileExportType.PDF:
            self.mimeType = "image/png"
        default:
            self.mimeType = "text/plain"
        }
        
        return mimeType
    }
    
}