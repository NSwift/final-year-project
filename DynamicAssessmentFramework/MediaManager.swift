//
//  MediaManager.swift
//  DynamicAssessmentFramework
//
//  Created by Daniel Parkin on 01/10/2014.
//  Copyright (c) 2014 DanielParkin. All rights reserved.
//

import UIKit

class MediaManager: NSObject {
    
    var importer:MediaImporterProtocol
    var player:MediaPlayerProtocol
    
    init(importer:MediaImporterProtocol, player:MediaPlayerProtocol) {
        self.importer = importer
        self.player = player
    }
    
}
