//
//  DPSideMenuDelegate.swift
//  DynamicAssessmentFramework
//
//  Created by Daniel Parkin on 12/01/2015.
//  Copyright (c) 2015 DanielParkin. All rights reserved.
//

import UIKit

protocol DPSideMenuDelegate : class {
    func didSelectMenuItem(itemIndex:Int)
}