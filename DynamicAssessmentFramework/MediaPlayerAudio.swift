//
//  MediaPlayerAudio.swift
//  DynamicAssessmentFramework
//
//  Created by Daniel Parkin on 01/10/2014.
//  Copyright (c) 2014 DanielParkin. All rights reserved.
//

import UIKit
import MediaPlayer

class MediaPlayerAudio: NSObject, MediaPlayerProtocol, AVAudioPlayerDelegate {

    weak var mediaDelegate:MediaPlayerDelegate?
    weak var audioDelegate:AVAudioPlayerDelegate?
    weak var view:UIView!
    weak var assetURL:NSURL!
    
    var audioPlayer = AVAudioPlayer()
    
    private var _appState:ApplicationState = ApplicationState.Review
    private var _previewBtnHeight:CGFloat = 50
    private var _previewBtnWidth:CGFloat = 50
    private var _previewBtnPlayXOffset:CGFloat = 0
    private var _previewBtnPauseXOffset:CGFloat = 0
    private var _previewBtnYOffset:CGFloat = 0
    private let _previewBtnYPadding:CGFloat = 10
    private let _previewBtnXPadding:CGFloat = 10
    
    private let _colorHelper = ColorHelper()
    private let _screenBounds = ScreenBounds()
    private let _fontHelper = FontHelper()
    
    let screenBounds = ScreenBounds()
    
    init(appState:ApplicationState, delegate:MediaPlayerDelegate)
    {
        self.mediaDelegate = delegate
        self._appState = appState
    }
    
    func removeAllSubviews() {
        self.view.subviews.map { $0.removeFromSuperview() }
    }
    
    func addReplayButton() {
        
        var btnReplay:DPUIButtonAnswer = DPUIButtonAnswer(
            frame: CGRect(
                x: 50,
                y: 50,
                width: 200,
                height: 100),
            rounded:true,
            animates: false
        )
        
        btnReplay.backgroundColor = UIColor.whiteColor()
        
        btnReplay.setTitleColor(UIColor.blackColor(), forState: UIControlState.Normal)
        
        btnReplay.setTitle("Replay", forState: UIControlState.Normal)
        btnReplay.addTarget(self, action: "replayMedia", forControlEvents: UIControlEvents.TouchUpInside)
        
        btnReplay.clipsToBounds = true
        
        screenBounds.positionViewCentral(
            thisView: btnReplay,
            inCenterOf: self.view
        )
        
        self.view.addSubview(btnReplay)
        
    }
    
    func addPlayButton() {
        
        var btnPlay:UIButton = UIButton(
            frame: CGRect(
                x: _previewBtnPlayXOffset,
                y: _previewBtnYOffset,
                width: _previewBtnWidth,
                height: _previewBtnHeight)
        )
        
        btnPlay.titleLabel!.font = _fontHelper.fontGlyphicons(10)
        btnPlay.backgroundColor = _colorHelper.colorPrimary2()
        btnPlay.setTitleColor(UIColor.blackColor(), forState: UIControlState.Normal)
        btnPlay.setTitle("b", forState: UIControlState.Normal)
        btnPlay.addTarget(self, action: "play", forControlEvents: UIControlEvents.TouchUpInside)
        
        btnPlay.clipsToBounds = true
        
        self.view.addSubview(btnPlay)
        
    }
    
    func addStopButton() {
        
        var btnStop:UIButton = UIButton(
            frame: CGRect(
                x: _previewBtnPauseXOffset,
                y: _previewBtnYOffset,
                width: _previewBtnWidth,
                height: _previewBtnHeight)
        )
        
        btnStop.titleLabel!.font = _fontHelper.fontGlyphicons(10)
        btnStop.backgroundColor = _colorHelper.colorPrimary3()
        btnStop.setTitleColor(UIColor.blackColor(), forState: UIControlState.Normal)
        btnStop.setTitle("g", forState: UIControlState.Normal)
        btnStop.addTarget(self, action: "stop", forControlEvents: UIControlEvents.TouchUpInside)
        
        btnStop.clipsToBounds = true
        
        self.view.addSubview(btnStop)
        
    }
    
    func calculateControlButtonSizing() {
        let viewHeight = self.view.frame.height
        let viewWidth = self.view.frame.width
        
        _previewBtnHeight = viewHeight / CGFloat(4)
        _previewBtnWidth = viewWidth / CGFloat(2)
        _previewBtnYOffset = viewHeight - _previewBtnHeight
        _previewBtnPlayXOffset = 0
        _previewBtnPauseXOffset = _previewBtnPlayXOffset + _previewBtnWidth
    }
    
    func play() {
        self.drawIcon()
        
        if _appState == ApplicationState.Preview {
            self.addPlayButton()
            self.addStopButton()
        }
        
        self.audioPlayer.play()
    }
    
    func stop() {
        self.audioPlayer.stop()
    }
    
    func replayMedia() {
        self.mediaDelegate!.didReplayMedia!()
        self.play()
    }
    
    func playMedia(url: NSURL, onView view: UIView){
        
        self.assetURL = url
        self.view = view
        
        let audioAsset = AVURLAsset.assetWithURL(url) as AVURLAsset
        self.audioPlayer = AVAudioPlayer(contentsOfURL: url, error: nil)
        self.audioPlayer.delegate = self
        
        switch (_appState) {
        case .Assessment:
            // play audio file
            self.play()
            
        case .Review:
            
            self.drawIcon()
            
            // draw filename onto view
            var filenameLabel = UILabel(
                frame: CGRect(
                    x: 0,
                    y: 50,
                    width: 0,
                    height: 0)
            )
            
            filenameLabel.text = filenameForAsset(assetURL)
            filenameLabel.font = _fontHelper.fontPrimary(40)
            filenameLabel.sizeToFit()
            filenameLabel.textColor = UIColor.whiteColor()
            
            _screenBounds.positionViewHorizontalCentral(
                thisView: filenameLabel,
                inCenterOf: self.view
            )
            
            self.view.addSubview(filenameLabel)
            
        case .Preview:
            
            calculateControlButtonSizing()
            self.drawIcon()
            self.addPlayButton()
            self.addStopButton()
        }

    }
    
    // notify delegate that audio finished
    func audioPlayerDidFinishPlaying(player: AVAudioPlayer!, successfully flag: Bool) {

        if _appState == ApplicationState.Assessment {
            self.removeAllSubviews()
            self.addReplayButton()
        }

        self.mediaDelegate!.didFinishPlayingMedia()
    }
    
    func filenameForAsset(assetURL:NSURL) -> String {
     
        var filename = "No filename available";
        
        var array = assetURL.absoluteString!.componentsSeparatedByString("=")
        var id:String = array[1]
        
        var query = MPMediaQuery()
        var predicate = MPMediaPropertyPredicate(value: id, forProperty: MPMediaItemPropertyPersistentID)
        
        query.addFilterPredicate(predicate)
        
        var items = query.items as [MPMediaItem]
        var mediaAsset:MPMediaItem = items[0]
        
        return mediaAsset.title
    }
    
    func drawIcon() {
        
        self.removeAllSubviews()
        
        // Add speaker icon
        let speakerImage = UIImage(named: "speaker-icon")
        let speakerView = UIImageView(image: speakerImage)
        
        switch(_appState) {
        case .Assessment:
            speakerView.frame = CGRectMake(0, 0, 100, 100)
            
        case .Review:
            speakerView.frame = CGRectMake(0, 0, 100, 100)
            
        case .Preview:
            speakerView.frame = CGRectMake(0, 0, 40, 40)
        }
        
        _screenBounds.positionViewCentral(
            thisView: speakerView,
            inCenterOf: self.view
        )
        
        self.view.backgroundColor = UIColor.darkGrayColor()
        self.view.setNeedsDisplay()
        self.view.addSubview(speakerView)
    }

    deinit {
        //self.audioPlayer.pause()
    }
    
}
