//
//  AssessmentCamera.swift
//  DynamicAssessmentFramework
//
//  Created by Daniel Parkin on 16/02/2015.
//  Copyright (c) 2015 DanielParkin. All rights reserved.
//


class AssessmentCamera : CameraResponseDelegate {
    
    private var _cameraMap:[String:Question]!
    
    var activeCameras:[String: CameraOperations] = [:]
    var activeCamera:CameraOperations?
    var cameraCount:Int32 = 0
    var videoURLS:[Question: NSURL] = [:]
    var outputFileCount:Int = 0

    func beginRecording(question:Question) {

        // create a new camera and add it to the camera dictionary
        let newCam = self.createNewCamera()
        let newCamId = String(newCam.id)
        
        // set the new camera as the active camera
        self.activeCamera = newCam
        
        // add the new camera to the active cameras collection
        self.activeCameras[newCamId] = newCam
        
        // map the new cameras id to the question being recorded
        self._cameraMap[newCamId] = question
        
        // set the new camera recording
        newCam.startRecording()
    }
    
    func stopRecording() {
        activeCamera!.stopRecording()
    }
    
    func createNewCamera() -> CameraOperations {
        
        //var uuid = NSUUID().UUIDString
        //println(uuid)
        
        var newCam = CameraOperations(delegate: self)
        
        // increment outputfile count
        outputFileCount++
        
        // incremement the camera count
        self.cameraCount++
        
        newCam.id = self.cameraCount
        newCam.fileNameID = Int32(outputFileCount)
        newCam.didFinishRecording = false
        
        return newCam
    }
    
    func cameraDidFinishRecording(url: NSURL) {
        
        // get the id of the camera that is calling back from the returned url
        let urlString:String = url.absoluteString!
        let urlLength:Int = countElements(urlString)
        let idIndex = urlLength - 5
        
        println(urlString)
        // regex test
        var pattern = "(?<=cam&)(.*)(?=&)"
        var error: NSError? = nil
        
        var regex = NSRegularExpression(
            pattern: pattern,
            options: NSRegularExpressionOptions.DotMatchesLineSeparators,
            error: &error
        )
        
        var results:[NSTextCheckingResult] = regex?.matchesInString(
            urlString,
            options: nil,
            range: NSMakeRange(0, countElements(urlString))
            ) as [NSTextCheckingResult]
        
        let nsStringUrl = urlString as NSString
        var cameraId = nsStringUrl.substringWithRange(results[0].range)
        println(cameraId)
        
        //let cameraId = String(urlString[advance(urlString.startIndex, idIndex)]) // get character at index 4
        println("Camera ID: \(cameraId)")
        
        // get the reference to the question that maps to the camera calling back
        let question = _cameraMap[cameraId]
        
        // add the NSURL object into the video urls dictionary against the correct question
        videoURLS[question!] = url
        
        // mark the camera as having called back ready for pruning
        self.activeCameras.removeValueForKey(cameraId)
        
        // camera debug info
        
        /*
        println("---------------------------------------------")
        println("The video URL count is \(self.videoURLS.count)")
        println("---------------------------------------------")
        
        println("Did complete with URL: \(url.absoluteString)")
        */
    }
    
    
}