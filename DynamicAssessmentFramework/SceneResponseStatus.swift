//
//  SceneResponseEnum.swift
//  DynamicAssessmentFramework
//
//  Created by Daniel Parkin on 20/08/2014.
//  Copyright (c) 2014 Echoyo. All rights reserved.
//

import Foundation

enum SceneResponseStatus {
    case CompleteGetNextScene
    case CompletedWithAnswer
    case CompletedWithBundle
}