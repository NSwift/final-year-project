//
//  Assessment.m
//  DynamicAssessmentFramework
//
//  Created by Daniel Parkin on 18/02/2015.
//  Copyright (c) 2015 DanielParkin. All rights reserved.
//

#import "Assessment.h"
#import "FocusPoint.h"
#import "Question.h"
#import "Result.h"


@implementation Assessment

@dynamic title;
@dynamic usesCustomFocusPoint;
@dynamic hasStartScreen;
@dynamic focusPoint;
@dynamic questions;
@dynamic results;

@end
