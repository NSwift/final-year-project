//
//  CustomAnswerPresenterDelegate.swift
//  DynamicAssessmentFramework
//
//  Created by Daniel Parkin on 19/02/2015.
//  Copyright (c) 2015 DanielParkin. All rights reserved.
//

protocol CustomAnswerPresenterDelegate {
    func didReturnAnswerPresenter(presenter:CustomAnswerPresenter)
}
