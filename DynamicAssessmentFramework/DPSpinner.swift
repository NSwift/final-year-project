//
//  DPSpinner.swift
//  DynamicAssessmentFramework
//
//  Created by Daniel Parkin on 16/02/2015.
//  Copyright (c) 2015 DanielParkin. All rights reserved.
//

import UIKit

class DPSpinner {
    
    private let _colorHelper = ColorHelper()
    private let _fontHelper = FontHelper()
    private let _screenBounds = ScreenBounds()
    
    private var _spinnerImage:UIImageView!
    private var _messageLabel:UILabel!
    private var _message:String!
    private var _view:UIView!
    private var _height:CGFloat!
    private var _width:CGFloat!
    private var _fontSize:CGFloat!
    
    init(message:String, height:CGFloat? = 200, width:CGFloat? = 200) {
        self._height = height
        self._width = width
        self._fontSize = 20
        self._message = message
        
        self._messageLabel = UILabel(
            frame: CGRect(
                x: 0,
                y: 20,
                width: 100,
                height: 50
            )
        )
    }
    
    func showInView(superView:UIView) {
        
        self._view = UIView(
            frame: CGRect(
                x: 0,
                y: 0,
                width: _width,
                height: _height
            )
        )
        
        // round edges
        _view.layer.cornerRadius = 20
        _view.layer.masksToBounds = true
        
        _screenBounds.positionViewCentral(thisView: _view, inCenterOf: superView)
        
        _view.backgroundColor = _colorHelper.colorPrimary3()
        
        // add label to view
        _messageLabel.text = _message
        _messageLabel.font = _fontHelper.fontPrimary(_fontSize)
        _messageLabel.sizeToFit()
        _messageLabel.textColor = UIColor.whiteColor()
        
        _screenBounds.positionViewHorizontalCentral(thisView: _messageLabel, inCenterOf: _view)
        
        _view.addSubview(_messageLabel)
 
        let spinnerX = (_width / 2) - 25
        let spinnerY = (_height / 2) - 25
        
        // setup spinner image
        _spinnerImage = UIImageView(frame: CGRect(x: spinnerX, y: spinnerY, width: 50, height: 50))
        
        _spinnerImage.animationImages = self.animationImages()
        _spinnerImage.animationDuration = 1.5
        _spinnerImage.startAnimating()
        
        //_screenBounds.positionViewCentral(thisView: _spinnerImage, inCenterOf: _view)
        //_spinnerImage.center = _view.center
        
        _view.addSubview(_spinnerImage)
        //_view.bringSubviewToFront(_spinnerImage)
        
        superView.addSubview(_view)
        
    }
    
    func animationImages() -> [UIImage] {
        let imageArray = [
            UIImage(named: "frame_000")!,
            UIImage(named:  "frame_001")!,
            UIImage(named:  "frame_002")!,
            UIImage(named:  "frame_003")!,
            UIImage(named:  "frame_004")!,
            UIImage(named:  "frame_005")!,
            UIImage(named:  "frame_006")!,
            UIImage(named:  "frame_007")!,
            UIImage(named:  "frame_008")!,
            UIImage(named:  "frame_009")!,
            UIImage(named:  "frame_010")!,
            UIImage(named:  "frame_011")!,
            UIImage(named:  "frame_012")!,
            UIImage(named:  "frame_013")!,
            UIImage(named:  "frame_014")!,
            UIImage(named:  "frame_015")!,
            UIImage(named:  "frame_016")!,
            UIImage(named:  "frame_017")!,
            UIImage(named:  "frame_018")!,
            UIImage(named:  "frame_019")!,
            UIImage(named:  "frame_020")!,
            UIImage(named:  "frame_021")!,
            UIImage(named:  "frame_022")!,
            UIImage(named:  "frame_023")!,
            UIImage(named:  "frame_024")!,
            UIImage(named:  "frame_025")!,
            UIImage(named:  "frame_026")!,
            UIImage(named:  "frame_027")!,
            UIImage(named:  "frame_028")!,
            UIImage(named:  "frame_029")!,
            UIImage(named:  "frame_030")!
        ]
        
        return imageArray
    }
    
    func remove() {
        _view.removeFromSuperview()
    }
    
    func updateMessage(message:String) {
        _messageLabel.text = message
    }
    
}