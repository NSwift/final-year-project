//
//  QuestionsViewController.swift
//  QuestionsTests
//
//  Created by Daniel Parkin on 19/08/2014.
//  Copyright (c) 2014 Echoyo. All rights reserved.
//

import UIKit

class AssessmentViewController: UIViewController, AssessmentResponseDelegate {

    // MARK: Properties
    var director:AssessmentDirector!
    var assessment:Assessment!
    var participant:Participant!
    var btnBack:UIButton?
    var mediaPlayers:[MediaPlayerVideo]! = []
    
    weak var delegate:AssessmentViewControllerDelegate!
    
    let colorHelper = ColorHelper()
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder);
    }

    // MARK: View methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.setNavigationBarHidden(true, animated: false)

        if let existingAssessment = assessment {
            
            director = AssessmentDirector(
                assessment: existingAssessment,
                rootView: self.view,
                delegate: self
            )
            
            director.getNextScene()
        }

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()

    }
    
    // hide status bar
    override func prefersStatusBarHidden() -> Bool {
        return true
    }
    
    // MARK: AssessmentResponseDelegate methods
    func assessmentDidRespond(response: AssessmentResponse) {
        
        // instantiate results generator and save results
        let resultsGenerator = AssessmentResultGenerator(
            participant: self.participant,
            assessment: self.assessment,
            answerBundles: response.answerBundles,
            videoURLs: response.videoURLS)
        
        resultsGenerator.createResults()
        
        // remove all subviews from the assessments view
        self.view.subviews.map { $0.removeFromSuperview() }
        
        // inform the assessment overview controller that the
        // assessment is complete
        delegate.didCompleteAssessment()
        
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        self.navigationController?.popToRootViewControllerAnimated(true)
        
        /*
        // create the temporary back button to get out of the assessment controller
        self.drawBackButton()
    
        var x = 0;
        var y = 100;
        var height = 220;
        var width = 250;
        
        var views:[UIView] = []
        
        for(question, url) in response.videoURLS {
    
            println(url.absoluteString)
            
            var view = UIView(frame: CGRect(x: x, y: y, width: width, height: height))
            view.backgroundColor = UIColor.grayColor()
            self.view.addSubview(view)
            
            var mediaPlayer = MediaPlayerVideo()
            self.mediaPlayers.append(mediaPlayer)
            mediaPlayer.playMedia(url, onView: view)
            
            if y >= height * 3 {
                y = 100
                x = x + width
            }
            else {
                y = y + height;
            }
            
        }
        */
    
    }
    
    // setup back button
    func drawBackButton() {
        btnBack = UIButton(frame: CGRect(x: 0, y: 0, width: 250, height: 100))
        btnBack!.backgroundColor = self.colorHelper.colorPrimary4()
        btnBack!.setTitle("Back", forState: UIControlState.Normal)
        btnBack!.addTarget(self, action: "backTapped", forControlEvents: UIControlEvents.TouchUpInside)
        
        self.view.addSubview(btnBack!)
    }
    
    func backTapped() {
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        self.navigationController?.popToRootViewControllerAnimated(true)
    }
    
    deinit {
        println("Assessment view controller dealloced");
    }

}

