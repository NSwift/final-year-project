//
//  CoreDataBase.swift
//  DynamicAssessmentFramework
//
//  Created by Daniel Parkin on 13/01/2015.
//  Copyright (c) 2015 DanielParkin. All rights reserved.
//

import UIKit
import CoreData

enum CDEntity:String {
    case Assessment = "Assessment"
    case Question = "Question"
    case Answer = "Answer"
    case Result = "Result"
    case FocusPoint = "FocusPoint"
    case Participant = "Participant"
    case AnswerPresenterOption = "AnswerPresenterOption"
    case CustomAnswerPresenter = "CustomAnswerPresenter"
}

class CoreDataBase : NSObject {
    
    let context:NSManagedObjectContext
    
    override init() {
        let appDel = UIApplication.sharedApplication().delegate as AppDelegate
        context = appDel.managedObjectContext!
    }
    
}