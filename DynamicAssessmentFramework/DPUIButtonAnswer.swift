//
//  DPUIButton.swift
//  DynamicAssessmentFramework
//
//  Created by Daniel Parkin on 05/01/2015.
//  Copyright (c) 2015 DanielParkin. All rights reserved.
//

import UIKit

class DPUIButtonAnswer: DPUIButton, AnswerProvidingControlProtocol {
    
    weak var answer:Answer?
    
    override init(frame: CGRect, rounded:Bool = false, animates:Bool = true) {
        super.init(frame: frame, rounded:rounded, animates: animates)
        
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func provideAnswer() -> Answer {
        return self.answer!
    }

    func getFrame() -> CGRect {
        return self.frame
    }
    
    func getView() -> UIView {
        return self
    }
}
