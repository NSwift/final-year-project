//
//  ButtonConfigOneRow.swift
//  DynamicAssessmentFramework
//
//  Created by Daniel Parkin on 28/02/2015.
//  Copyright (c) 2015 DanielParkin. All rights reserved.
//
import UIKit

class ButtonConfigOneRow : NSObject, ButtonConfigViewProtocol {
    
    weak var delegate:ButtonConfigViewDelegate!
    private var _answers:[Answer]!
    private var _shouldAnimate:Bool!
    private var _appState:ApplicationState!
    private var _controls:[UIButton] = []
    
    private let _fontHelper = FontHelper()
    
    init(answers:[Answer], shouldAnimate:Bool, appState:ApplicationState) {
        self._answers = answers
        self._shouldAnimate = shouldAnimate
        self._appState = appState
    }
    
    func displayOnView(view: UIView) {
      
        // create NSArray in order to use indexOfObject for tag
        var array = NSArray()
        
        // loop through answers and create buttons
        // increment ypos on each iteration
        let screenBounds = ScreenBounds()
        let totalAnswers = _answers.count
        let spacing = 20
        let buttonWidth = 150
        let buttonHeight = 250
        var xPos = 10;
        
        // create containing view for buttons so they can be centred
        let buttonViewWidth = (totalAnswers * (buttonWidth + spacing))
        
        var buttonView = UIView(
            frame: CGRect(
                x: 0,
                y: 60,
                width: buttonViewWidth,
                height: buttonHeight
            )
        )
        
        screenBounds.positionViewHorizontalCentral(
            thisView: buttonView,
            inCenterOf: view
        )
        
        for answer in _answers {
            
            // button
            var button:DPUIButtonAnswer = DPUIButtonAnswer(
                frame: CGRect(
                    x: xPos,
                    y: 50,
                    width: buttonWidth,
                    height: buttonHeight),
                rounded:true,
                animates: _shouldAnimate
            )
            
            // assign the answer to the button
            button.answer = answer as Answer
            
            // set button tag
            button.tag = array.indexOfObject(answer)
            
            button.backgroundColor = UIColor.whiteColor()
            
            button.setTitleColor(UIColor.blackColor(), forState: UIControlState.Normal)
            
            button.setTitle(answer.value, forState: UIControlState.Normal)
            button.addTarget(self, action: "provideAnswer:", forControlEvents: UIControlEvents.TouchUpInside)
            
            button.clipsToBounds = true
            
            button.exclusiveTouch = true
            
            //button.titleLabel!.font = _fontHelper.fontPrimary(20)
            button.titleLabel!.numberOfLines = 2
            button.titleLabel!.adjustsFontSizeToFitWidth = true
            button.titleLabel!.lineBreakMode = NSLineBreakMode.ByWordWrapping
            button.titleLabel!.textAlignment = NSTextAlignment.Center
            
            let size = button.titleLabel!.font.pointSize
            button.titleLabel!.font = _fontHelper.fontPrimary(size)
            
            // add controls to array to retain reference
            _controls.append(button)
            
            buttonView.addSubview(button)

            // increment yPos
            xPos += (buttonWidth + spacing)
        }
        
        // overlay answers if neccessary
        if _appState == ApplicationState.Review {
            let answerOverlay = AssessmentAnswerOverlay()
            answerOverlay.overlayAnswerOnView(buttonView, controls: _controls)
        }
        
        view.addSubview(buttonView)
        
    }
    
    func provideAnswer(sender:AnyObject) {
        if sender is DPUIButtonAnswer {
            
            let answerButton = sender as DPUIButtonAnswer
            let selectedAnswer = answerButton.answer!
            
            delegate.buttonViewDidProvideAnswer(selectedAnswer)
        }
    }
    
}
