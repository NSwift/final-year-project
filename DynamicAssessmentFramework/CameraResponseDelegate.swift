//
//  CameraResponseDelegate.swift
//  DynamicAssessmentFramework
//
//  Created by Daniel Parkin on 16/10/2014.
//  Copyright (c) 2014 DanielParkin. All rights reserved.
//

import UIKit

protocol CameraResponseDelegate : class {
    func cameraDidFinishRecording(url:NSURL)
}
