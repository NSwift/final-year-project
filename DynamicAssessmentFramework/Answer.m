//
//  Answer.m
//  DynamicAssessmentFramework
//
//  Created by Daniel Parkin on 15/02/2015.
//  Copyright (c) 2015 DanielParkin. All rights reserved.
//

#import "Answer.h"
#import "Question.h"
#import "Result.h"


@implementation Answer

@dynamic value;
@dynamic parentQuestion;
@dynamic result;
@dynamic correctForQuestion;

@end
