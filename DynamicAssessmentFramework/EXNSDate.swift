//
//  EXNSDate.swift
//  DynamicAssessmentFramework
//
//  Created by Daniel Parkin on 09/01/2015.
//  Copyright (c) 2015 DanielParkin. All rights reserved.
//

extension NSDate
{
    convenience
    init(dateString:String) {
        let dateStringFormatter = NSDateFormatter()
        //dateStringFormatter.dateFormat = "yyyy-MM-dd"
        dateStringFormatter.dateFormat = "dd/MM/yyyy"
        dateStringFormatter.locale = NSLocale(localeIdentifier: "en_US_POSIX")
        let d = dateStringFormatter.dateFromString(dateString)
        self.init(timeInterval:0, sinceDate:d!)
    }
}