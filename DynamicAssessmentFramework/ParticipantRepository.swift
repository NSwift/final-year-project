//
//  QuestionRepository.swift
//  DynamicAssessmentFramework
//
//  Created by Daniel Parkin on 08/01/2015.
//  Copyright (c) 2015 DanielParkin. All rights reserved.
//

class ParticipantRepository: CoreDataRepository {
    
    override init() {
        super.init()
    }
    
    func getAllParticipants() -> [Participant] {
        
        let request = NSFetchRequest(entityName: "Participant")
        request.returnsObjectsAsFaults = false
        
        var results:NSArray = self.context.executeFetchRequest(request, error: nil)!
        
        return results as [Participant]
    }
    
    func getParticipantsByAssessment(assessment:Assessment) -> [Participant] {
        let request = NSFetchRequest(entityName: "Result")
        let predicate = NSPredicate(format: "assessment == %@", assessment)
        request.returnsObjectsAsFaults = false
        request.predicate = predicate
        request.resultType = NSFetchRequestResultType.DictionaryResultType
        request.returnsDistinctResults = true
        request.propertiesToFetch = ["participant"]
        
        var results:NSArray = self.context.executeFetchRequest(request, error: nil)!
        
        var objectIDs:[NSManagedObjectID] = results.valueForKeyPath("participant") as [NSManagedObjectID]

        let participants = participantsFromObjIds(objectIDs)

        return participants
    }
    
    func getParticipantCountForAssessment(assessment:Assessment) -> Int {
        return getParticipantsByAssessment(assessment).count
    }
    
    func getParticipantById(#id:String) -> Participant {
        
        let request = NSFetchRequest(entityName: "Participant")
        let predicate = NSPredicate(format: "id == [c]%@", id)
        request.returnsObjectsAsFaults = false
        request.predicate = predicate
        
        var results:NSArray = self.context.executeFetchRequest(request, error: nil)!
        
        return results[0] as Participant
    }
    
    func participantAlreadyExists(participantId:String) -> Bool {
        
        let request = NSFetchRequest(entityName: "Participant")
        let predicate = NSPredicate(format: "id == [c]%@", participantId)
        request.returnsObjectsAsFaults = false
        request.predicate = predicate
        
        var results:[Participant] = self.context.executeFetchRequest(request, error: nil) as [Participant]
        
        return results.count > 0
    }
    
    func participantsFromObjIds(objectIds:[NSManagedObjectID]) -> [Participant] {
        return self.entitiesFromObjectIDs(objectIds) as [Participant]
    }
    
}
