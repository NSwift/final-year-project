//
//  MediaFilenameGeneratorAudio.swift
//  DynamicAssessmentFramework
//
//  Created by Daniel Parkin on 14/02/2015.
//  Copyright (c) 2015 DanielParkin. All rights reserved.
//

import AssetsLibrary
import MediaPlayer

class MediaFilenameGeneratorAudio : MediaFilenameGeneratorProtocol {
    
    weak var delegate:MediaFilenameGeneratorDelegate!
    
    init(delegate:MediaFilenameGeneratorDelegate) {
        self.delegate = delegate
    }
    
    func getFilename(assetURL: NSURL) {
        
        var filename = "No filename available";
        
        var array = assetURL.absoluteString!.componentsSeparatedByString("=")
        var id:String = array[1]
        
        var query = MPMediaQuery()
        var predicate = MPMediaPropertyPredicate(value: id, forProperty: MPMediaItemPropertyPersistentID)
        
        query.addFilterPredicate(predicate)
        
        var items = query.items as [MPMediaItem]
        var mediaAsset:MPMediaItem = items[0]
        
        filename = mediaAsset.title
        
        var response = MediaFilenameResponse(
            assetURL: assetURL,
            filename: filename,
            status: MediaFilenameGeneratorStatus.DidGenerateFilename
        )
        
        delegate.didGenerateFilenameForAssetURL(response)
    }
    
}