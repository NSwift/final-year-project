//
//  Question.m
//  DynamicAssessmentFramework
//
//  Created by Daniel Parkin on 03/03/2015.
//  Copyright (c) 2015 DanielParkin. All rights reserved.
//

#import "Question.h"
#import "Answer.h"
#import "Assessment.h"
#import "CustomAnswerPresenter.h"
#import "Result.h"


@implementation Question

@dynamic answerDisplayBehavior;
@dynamic answerType;
@dynamic assetURL;
@dynamic countdown;
@dynamic filename;
@dynamic hasFocusPoint;
@dynamic introText;
@dynamic mediaType;
@dynamic mediaURL;
@dynamic pauseTime;
@dynamic position;
@dynamic questionText;
@dynamic questionType;
@dynamic scaleSize;
@dynamic shouldRecord;
@dynamic answers;
@dynamic correctAnswer;
@dynamic customAnswerPresenter;
@dynamic parentAssessment;
@dynamic results;

@end
