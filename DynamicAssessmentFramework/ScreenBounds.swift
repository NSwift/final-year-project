//
//  ScreenOperations.swift
//  DynamicAssessmentFramework
//
//  Created by Daniel Parkin on 24/08/2014.
//  Copyright (c) 2014 Echoyo. All rights reserved.
//

import Foundation
import UIKit

class ScreenBounds {
   
    let screenBounds = UIScreen.mainScreen().bounds
    
    func bounds() -> CGRect {
        return screenBounds
    }
    
    func height() -> CGFloat {
        return bounds().height
    }
    
    func width() -> CGFloat {
        return bounds().width
    }
    
    func topHalf() -> CGRect {
        return CGRect(x: 0, y: 0, width: Int(screenBounds.width), height: Int(screenBounds.height / 2))
    }
    
    func bottomHalf() -> CGRect {
        return CGRect(x: 0, y: Int(screenBounds.height / 2), width: Int(screenBounds.width), height: Int(screenBounds.height / 2))
    }
    
    func viewCentre(view:UIView) -> CGPoint
    {
        var x = screenBounds.width / 2
        var y = screenBounds.height / 2
        return CGPointMake(x, y)
    }
    
    func viewTopCentre(view:UIView) -> CGPoint {
        var x = screenBounds.width / 2
        var y = (screenBounds.height / 3 ) / 2
        return CGPointMake(x, y)
    }
    
    func viewBottomCentre(view:UIView) -> CGPoint {
        var x = screenBounds.width / 2
        var y = ((screenBounds.height / 3 ) / 2 ) * 5
        return CGPointMake(x, y)
    }
    
    func centreFromDimensions(#width: CGFloat, height:CGFloat) -> (x:CGFloat, y:CGFloat)
    {
        var x = (screenBounds.width / 2) - (width / 2)
        var y = (screenBounds.height / 2 ) - (height / 2)
        return (x, y)
    }
    
    func positionViewCentral(#thisView:UIView, inCenterOf:UIView) {
        let parentView = inCenterOf
        let childView = thisView
        let parentCentre = self.viewCentre(parentView)
        let childHeight = childView.frame.size.height
        let childWidth = childView.frame.size.height
        //childView.frame.origin.x = parentCentre.x - (childWidth / 2)
        //childView.frame.origin.x = (parentView.frame.width - childView.frame.width) / 2
        //childView.frame.origin.y = parentCentre.y - (childHeight / 2)
        childView.center = parentView.convertPoint(parentView.center, fromView: parentView.superview)
    }
    
    func positionViewHorizontalCentral(#thisView:UIView, inCenterOf:UIView) {
        let parentView = inCenterOf
        let childView = thisView
        let parentCentre = self.viewCentre(parentView)
        let childHeight = childView.frame.size.height
        let childWidth = childView.frame.size.height
        //childView.frame.origin.x = parentCentre.x - (childWidth / 2)
        childView.frame.origin.x = (parentView.frame.width - childView.frame.width) / 2
    }
    
    func positionViewVerticalCentral(#thisView:UIView, inCenterOf:UIView) {
        let parentView = inCenterOf
        let childView = thisView
        let parentCentre = self.viewCentre(parentView)
        let childHeight = childView.frame.size.height
        let childWidth = childView.frame.size.height
        childView.frame.origin.y = (parentView.frame.height - childView.frame.height) / 2
    }
    
}



