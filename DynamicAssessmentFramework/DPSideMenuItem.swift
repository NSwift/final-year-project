//
//  DPModalMenuItem.swift
//  DynamicAssessmentFramework
//
//  Created by Daniel Parkin on 11/01/2015.
//  Copyright (c) 2015 DanielParkin. All rights reserved.
//

import UIKit

class DPSideMenuItem : NSObject {
    
    var itemIndex:Int = 0
    var height:CGFloat = 0
    var width:CGFloat = 0
    var fontSize:CGFloat = 30
    var itemText = ""
    var iconCharacter = ""
    var backgroundColor = UIColor.whiteColor()
    var foregroundColor = UIColor.whiteColor()
    var itemView:UIButton!
    
    let alpha:CGFloat = 0.5
    
    let colorHelper = ColorHelper()
    let fontHelper = FontHelper()
    let screenBounds = ScreenBounds()
    
    weak var parent:DPSideMenu!
    weak var delegate:DPSideMenuDelegate?
    
    init(itemText:String, iconCharacter:String) {
        self.itemText = itemText
        self.iconCharacter = iconCharacter
        self.backgroundColor = colorHelper.colorPrimary4()
    }
    
    
    func view() -> UIView {
        
        var button = UIButton(
            frame: CGRect(
                x: 0,
                y: 0,
                width: self.width,
                height: self.height)
        )
        
        var label = UILabel(
            frame: CGRect(
                x: 20,
                y: 0,
                width: 50,
                height: 50)
        )
        
        label.font = fontHelper.fontGlyphicons(self.fontSize)
        label.textColor = self.foregroundColor
        label.text = self.iconCharacter
        
        screenBounds.positionViewVerticalCentral(thisView: label, inCenterOf: button)
        
        button.addSubview(label)
        
        button.titleLabel!.font = fontHelper.fontPrimary(self.fontSize)
        button.contentHorizontalAlignment = UIControlContentHorizontalAlignment.Right
        button.backgroundColor = self.backgroundColor.colorWithAlphaComponent(self.alpha)
        button.setTitleColor(self.foregroundColor, forState: UIControlState.Normal)
        button.setTitle(self.itemText, forState: UIControlState.Normal)
        button.addTarget(self, action: "didSelectMenuItem:", forControlEvents: UIControlEvents.TouchUpInside)
        
        button.titleEdgeInsets = UIEdgeInsetsMake(50, 50, 50, 20)
        
        self.itemView = button
        
        return self.itemView
    }
    
    func didSelectMenuItem(sender:AnyObject) {
        delegate!.didSelectMenuItem(self.itemIndex)
        parent.view.removeFromSuperview()
    }
    
    deinit {
        println("Menu item dealloc")
    }
    
}