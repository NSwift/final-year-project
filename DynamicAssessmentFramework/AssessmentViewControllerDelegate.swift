//
//  AssessmentViewControllerDelegate.swift
//  DynamicAssessmentFramework
//
//  Created by Daniel Parkin on 17/03/2015.
//  Copyright (c) 2015 DanielParkin. All rights reserved.
//

protocol AssessmentViewControllerDelegate : class {
    func didCompleteAssessment()
}