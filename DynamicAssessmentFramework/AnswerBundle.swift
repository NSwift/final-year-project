//
//  AnswerBundle.swift
//  DynamicAssessmentFramework
//
//  Created by Daniel Parkin on 14/02/2015.
//  Copyright (c) 2015 DanielParkin. All rights reserved.
//

import Foundation

class AnswerBundle: NSObject {

    var answer:Answer!
    var responseTime:Double!
    var mediaReplays:Int!
    
    init(answer:Answer, responseTime:Double, mediaReplays:Int) {
        self.answer = answer
        self.responseTime = responseTime
        self.mediaReplays = mediaReplays
    }

}