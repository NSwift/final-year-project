//
//  Answer.h
//  DynamicAssessmentFramework
//
//  Created by Daniel Parkin on 15/02/2015.
//  Copyright (c) 2015 DanielParkin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Question, Result;

@interface Answer : NSManagedObject

@property (nonatomic, retain) NSString * value;
@property (nonatomic, retain) Question *parentQuestion;
@property (nonatomic, retain) NSSet *result;
@property (nonatomic, retain) Question *correctForQuestion;
@end

@interface Answer (CoreDataGeneratedAccessors)

- (void)addResultObject:(Result *)value;
- (void)removeResultObject:(Result *)value;
- (void)addResult:(NSSet *)values;
- (void)removeResult:(NSSet *)values;

@end
