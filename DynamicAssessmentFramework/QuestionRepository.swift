//
//  QuestionRepository.swift
//  DynamicAssessmentFramework
//
//  Created by Daniel Parkin on 08/01/2015.
//  Copyright (c) 2015 DanielParkin. All rights reserved.
//

class QuestionRepository: CoreDataRepository {

    override init() {
        super.init()
    }
    
    func getQuestionsByAssessment(assessment:Assessment, ascending:Bool) -> [Question] {

        let request = NSFetchRequest(entityName: "Question")
        let sortDescriptor = NSSortDescriptor(key: "position", ascending: ascending)
        let predicate = NSPredicate(format: "parentAssessment == %@", assessment)
        request.returnsObjectsAsFaults = false
        request.predicate = predicate
        request.sortDescriptors = [sortDescriptor]
        
        var results:NSArray = self.context.executeFetchRequest(request, error: nil)!
        
        return results as [Question]
    }
    
    func getQuestionsRequiringAnswersByAssessment(assessment:Assessment, ascending:Bool) -> [Question] {
        
        let request = NSFetchRequest(entityName: "Question")
        let sortDescriptor = NSSortDescriptor(key: "position", ascending: ascending)
        let predicate = NSPredicate(format: "parentAssessment == %@ AND questionType != %@", assessment, "Not Required")
        request.returnsObjectsAsFaults = false
        request.predicate = predicate
        request.sortDescriptors = [sortDescriptor]
        
        var results:NSArray = self.context.executeFetchRequest(request, error: nil)!
        
        return results as [Question]
    }
    
    func getZombieQuestions() -> [Question] {
        
        let request = NSFetchRequest(entityName: "Question")
        let predicate = NSPredicate(format: "parentAssessment == nil")
        request.returnsObjectsAsFaults = false
        request.predicate = predicate
        
        var results:NSArray = self.context.executeFetchRequest(request, error: nil)!
        
        return results as [Question]
    }

    
}
