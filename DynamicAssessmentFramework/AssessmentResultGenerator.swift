//
//  AssessmentResultGenerator.swift
//  DynamicAssessmentFramework
//
//  Created by Daniel Parkin on 11/01/2015.
//  Copyright (c) 2015 DanielParkin. All rights reserved.
//

import UIKit

class AssessmentResultGenerator: NSObject {

    weak var participant:Participant!
    weak var assessment:Assessment!
    var answerBundles:[Question: AnswerBundle]
    var videoURLS:[Question: NSURL]
    
    let CDA = CoreDataAccess()
    
    init(participant:Participant,
         assessment:Assessment,
         answerBundles:[Question: AnswerBundle],
         videoURLs:[Question: NSURL]) {
            
        self.participant = participant
        self.assessment = assessment
        self.answerBundles = answerBundles
        self.videoURLS = videoURLs
    }
    
    func createResults() {
        

        for (question, bundle) in self.answerBundles {
            
            var result = CDA.newObjectForEntity(CDEntity.Result) as Result
            
            let recordingUrl = videoURLS[question]?.absoluteString
            
            result.answer = bundle.answer as Answer
            result.assessment = self.assessment
            result.question = question
            result.participant = self.participant
            result.recordingUrl = recordingUrl
            result.mediaReplays = bundle.mediaReplays
            result.responseTime = bundle.responseTime
            
        }
        
        CDA.save()

    }
    
}
