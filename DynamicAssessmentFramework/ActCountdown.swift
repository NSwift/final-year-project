//
//  ActCountdown.swift
//  DynamicAssessmentFramework
//
//  Created by Daniel Parkin on 21/08/2014.
//  Copyright (c) 2014 Echoyo. All rights reserved.
//

import Foundation
import UIKit

class ActCountdown:NSObject, AssessmentActProtocol {
    
    var seconds:Int
    var timer:NSTimer
    var controls:[NSObject]
    var screenBounds:ScreenBounds
    var fontHelper = FontHelper()
    
    weak var delegate:ActResponseDelegate?
    weak var mediaDelegate:MediaPlayerDelegate?
    
    init(seconds:Int, delegate:ActResponseDelegate) {
        self.seconds = seconds
        self.timer = NSTimer()
        self.screenBounds = ScreenBounds()
        self.delegate = delegate
        self.controls = []
    }
    
    func beginDisplayOnView(view: UIView) {
        let actView = UIView(frame: screenBounds.bounds())
        
        actView.backgroundColor = UIColor.darkGrayColor()
        
        var label = UILabel(frame: CGRect(x: 0, y: 200, width: 550, height: 550))
        label.text = String(self.seconds)
        label.numberOfLines = 0
        label.font = fontHelper.fontPrimary(300)
        label.textAlignment = NSTextAlignment.Center
        label.textColor = UIColor.whiteColor()
        
        controls.append(label)
        
        screenBounds.positionViewCentral(
            thisView: label,
            inCenterOf: view
        )
        
        actView.addSubview(label)
        
        view.addSubview(actView)
        
        // begin timer
        self.timer = NSTimer.scheduledTimerWithTimeInterval(
            1.0,
            target: self,
            selector: "updateTimer:",
            userInfo: nil,
            repeats: true
        )
    }
    
    func updateTimer(timer:NSTimer) {
        if seconds > 1 {
            seconds--
            let label = controls[0] as UILabel
            label.text = String(seconds)
        }
        else {
            timer.invalidate()
            let status = ActResponseStatus.CompleteGetNextAct
            let response = ActResponse(status: status, bundle: nil)
            delegate?.actDidRespond(response)
        }
    }
    
}