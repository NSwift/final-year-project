//
//  ProtocolAnswerPresentation.swift
//  QuestionsTests
//
//  Created by Daniel Parkin on 19/08/2014.
//  Copyright (c) 2014 Echoyo. All rights reserved.
//

import Foundation
import UIKit

protocol MediaImporterProtocol {
    var mediaURL:NSURL { get set }
    weak var delegate:MediaImporterDelegate? { get set }
    func controller() -> UIViewController
}