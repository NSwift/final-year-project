//
//  FontHelper.swift
//  DynamicAssessmentFramework
//
//  Created by Daniel Parkin on 08/01/2015.
//  Copyright (c) 2015 DanielParkin. All rights reserved.
//

import UIKit

class FontHelper: NSObject {

    let fontPrimaryFileName = "FiraSans-Regular"
    let fontGlyphiconsFileName = "icomoon"
    
    func fontPrimary(size:CGFloat) -> UIFont {
        return UIFont(name: self.fontPrimaryFileName, size: size)!
    }
    
    func fontGlyphicons(size:CGFloat) -> UIFont {
        return UIFont(name: self.fontGlyphiconsFileName, size: size)!
    }
    
    func fontPrimaryName() -> String {
        return self.fontPrimaryFileName
    }
    
    func fontGlyphiconsName() -> String {
        return self.fontGlyphiconsFileName
    }
    
}
