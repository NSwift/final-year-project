//
//  MediaFilenameGeneratorStatus.swift
//  DynamicAssessmentFramework
//
//  Created by Daniel Parkin on 15/02/2015.
//  Copyright (c) 2015 DanielParkin. All rights reserved.
//

import Foundation

enum MediaFilenameGeneratorStatus {
    case DidGenerateFilename
    case DidReceiveError
}