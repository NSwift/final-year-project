//
//  DPUIButton.swift
//  DynamicAssessmentFramework
//
//  Created by Daniel Parkin on 05/01/2015.
//  Copyright (c) 2015 DanielParkin. All rights reserved.
//

import UIKit

class DPUIButton: UIButton {

    let colorHelper = ColorHelper()
 
    init(frame: CGRect, rounded:Bool = false, animates:Bool = true) {
        super.init(frame: frame)
        
        if (animates) {
            self.alpha = 0
            
            // button fade in
            UIView.animateWithDuration(1.0,
                delay: 0.0,
                options: .CurveEaseIn  & .AllowUserInteraction,
                animations: {
                    self.alpha = 1.0
                }, completion: nil)
        }
        
        if(rounded) {
            self.layer.cornerRadius = 20
            self.layer.masksToBounds = true
            
        }
        
    }
    
    override var highlighted: Bool {
        didSet {
            
            if (highlighted) {
                self.backgroundColor = self.colorHelper.colorPrimary4()
            }
            else {
                self.backgroundColor = UIColor.clearColor()
            }
        }
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
}
