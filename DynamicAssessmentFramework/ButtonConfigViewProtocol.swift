//
//  ButtonConfigViewProtocol.swift
//  DynamicAssessmentFramework
//
//  Created by Daniel Parkin on 28/02/2015.
//  Copyright (c) 2015 DanielParkin. All rights reserved.
//
import UIKit

protocol ButtonConfigViewProtocol {
    weak var delegate:ButtonConfigViewDelegate! { get set }
    func displayOnView(view:UIView)
    func provideAnswer(sender:AnyObject)
}
