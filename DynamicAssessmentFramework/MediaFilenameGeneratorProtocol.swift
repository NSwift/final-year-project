//
//  MediaFilenameGeneratorProtocol.swift
//  DynamicAssessmentFramework
//
//  Created by Daniel Parkin on 14/02/2015.
//  Copyright (c) 2015 DanielParkin. All rights reserved.
//

protocol MediaFilenameGeneratorProtocol {
    weak var delegate:MediaFilenameGeneratorDelegate! { get set }
    func getFilename(assetURL:NSURL)
}