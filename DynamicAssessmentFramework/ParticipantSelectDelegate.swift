//
//  MediaPlayerFinishedDelegate.swift
//  DynamicAssessmentFramework
//
//  Created by Daniel Parkin on 06/01/2015.
//  Copyright (c) 2015 DanielParkin. All rights reserved.
//

import UIKit

protocol ParticipantSelectDelegate : class {
    func didSelectParticipant(participant:Participant)
}