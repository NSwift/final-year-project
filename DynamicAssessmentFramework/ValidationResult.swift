//
//  ValidationBundle.swift
//  DynamicAssessmentFramework
//
//  Created by Daniel Parkin on 23/02/2015.
//  Copyright (c) 2015 DanielParkin. All rights reserved.
//

class ValidationResult {
    
    var isValid:Bool = false
    var errorMessages:[String] = []
    
    init(isValid:Bool, errorMessages:[String]) {
        self.isValid = isValid
        self.errorMessages = errorMessages
    }

}
