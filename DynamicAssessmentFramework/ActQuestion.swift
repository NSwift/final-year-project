//
//  Question.swift
//  QuestionsTests
//
//  Created by Daniel Parkin on 19/08/2014.
//  Copyright (c) 2014 Echoyo. All rights reserved.
//

import Foundation
import UIKit

class ActQuestion:NSObject, AssessmentActProtocol, MediaPlayerDelegate, AnswerPresenterDelegate {
    
    weak var delegate:ActResponseDelegate?
    weak var cameraDelegate:QuestionRecordingDelegate!
    let mediaPlayerFact = MediaPlayerFactory()
    var rootView:UIView?
    var question:Question? = nil
    var controls:[UIControl]!
    var mediaPlayer:MediaPlayerProtocol!
    var answerPresenter:AnswerPresentationProtocol?
    var presenterFactory:AnswerPresenterFactory!
    var screenBounds:ScreenBounds!
    var answerDisplayBehavior:String!
    //var viewedAsReview:Bool = false
    var startTime:NSTimeInterval!
    var endTime:NSTimeInterval!
    var mediaReplayCount:Int = 0
    var responseTime:Double = 0
    var responseTimerStarted:Bool = false
    var answerPresenterOnView:Bool = false
    
    private var _appState:ApplicationState!
    
    init(question:Question, delegate:ActResponseDelegate, appState:ApplicationState) {
        super.init()
        self._appState = appState
        self.question = question
        self.controls = [UIControl]()
        self.delegate = delegate
        self.screenBounds = ScreenBounds()
        self.presenterFactory = AnswerPresenterFactory()
        
        self.mediaPlayer = mediaPlayerFact.getPlayer(
            appState,
            type: question.mediaType,
            delegate: self
        )
        
        // get answer presenter from factory
        self.answerPresenter = presenterFactory.getPresenter(
            question,
            appState: appState,
            delegate: self
        )
        
        self.answerDisplayBehavior = self.question!.answerDisplayBehavior
    }
    
    func beginDisplayOnView(view: UIView) {
        
        // set reference to rootview for MediaPlayerFinished callback
        self.rootView = view
        
        // set the entire root view background colour to grey
        self.rootView!.backgroundColor = UIColor.darkGrayColor()
        
        // setup the act view to house the media and answer presenter views
        var actView:UIView = UIView(frame: screenBounds.bounds())
        actView.backgroundColor = UIColor.darkGrayColor()
        
        // setup the media view
        let mediaView = UIView(frame: screenBounds.topHalf())
        actView.addSubview(mediaView)
        
        // start recording the question if the cam delegate has been set
        if let camDelegate = cameraDelegate {
            camDelegate.beginRecordingQuestion(self.question!)
        }
        
        // begin playing the media file
        let assetURL = NSURL(string: question!.assetURL)
        mediaPlayer!.playMedia(assetURL!, onView: mediaView)
        
        // add the media view to the root view
        view.addSubview(mediaView)
        
        // if being reviewed add answer presenter immediately
        // then iterate over all subviews and disable interaction
        if _appState == ApplicationState.Review {
            
            self.addAnswerPresenterToView()
            
            for view in view.subviews {
                let subview = view as UIView
                subview.userInteractionEnabled = false
            }
            
        }
        else {
            
            // setup the answer presenter view
            // decide when the answer presenter should be added to the root view
            // if answer presenter should display immediately, add it to the root view
            // otherwise start timer or wait for callback
            switch(self.answerDisplayBehavior!) {
            case "Display immediately":
                
                self.addAnswerPresenterToView()
                
            case "Pause for interval":
                
                let wait:Double = question!.pauseTime as Double
                let duration:NSTimeInterval = wait
                
                var timer = NSTimer.scheduledTimerWithTimeInterval(
                    duration,
                    target: self,
                    selector: Selector("addAnswerPresenterToView"),
                    userInfo: nil,
                    repeats: false)
                
            default:
                println("Other")
            }
        }
        
    }
    
    func addAnswerPresenterToView() {
        if let rootView = self.rootView {
            answerPresenter!.displayOnView(rootView)
            self.answerPresenterOnView = true
            beginResponseTimer()
        }
    }
    
    func stopRecordingVideo() {
        // start recording the question if the cam delegate has been set
        if let camDelegate = cameraDelegate {
            camDelegate.stopRecordingQuestion(self.question!)
        }
    }
    
    // MARK: MediaPlayerDelegate
    func didFinishPlayingMedia() {
        if(self.answerDisplayBehavior! == "Wait for media"
                && !self.answerPresenterOnView)
        {
            self.addAnswerPresenterToView()
        }

    }
    
    func didReplayMedia() {
        self.mediaReplayCount++
    }
    
    // MARK: Response timer selectors
    func beginResponseTimer() {
        if(!self.responseTimerStarted) {
            self.startTime =  NSDate.timeIntervalSinceReferenceDate()
            self.responseTimerStarted = true
        }
    }
    
    // MARK: AnswerPresenterDelegate
    func didProvideAnswer(answer: Answer) {
        //println(answer.value)
        
        // calculate response time
        self.endTime = NSDate.timeIntervalSinceReferenceDate()
        self.responseTime = self.endTime - self.startTime
        
        //println("Response time: \(Double(responseTime))")
        //println("Media replays: \(self.mediaReplayCount)")
        
        // create response bundle and package it up the act
        // response
        let answerBundle = AnswerBundle(
            answer: answer,
            responseTime: self.responseTime,
            mediaReplays: self.mediaReplayCount
        )
        
        let status = ActResponseStatus.CompletedWithBundle
        let response = ActResponse(status: status, bundle: answerBundle)
        
        // stop the video recording
        self.stopRecordingVideo()
        self.delegate!.actDidRespond(response)
    }
    
    func didCompleteWithoutAnswer() {
        //println("No answer provided")
        
        let status = ActResponseStatus.CompletedWithoutAnswer
        let response = ActResponse(status: status)
        
        // stop the video recording
        self.stopRecordingVideo()
        self.delegate!.actDidRespond(response)
    }
    
    // MARK: Custom deinit method
    deinit {
        println("Act question dealloc")
    }
   
}
