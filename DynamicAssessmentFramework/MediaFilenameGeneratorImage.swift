//
//  MediaFilenameGeneratorImage.swift
//  DynamicAssessmentFramework
//
//  Created by Daniel Parkin on 14/02/2015.
//  Copyright (c) 2015 DanielParkin. All rights reserved.
//

import Foundation
import AssetsLibrary

class MediaFilenameGeneratorImage : MediaFilenameGeneratorProtocol {
    
    weak var delegate:MediaFilenameGeneratorDelegate!
    
    var filename = ""
    var filenameAcquired:Bool = false

    init(delegate:MediaFilenameGeneratorDelegate) {
        self.delegate = delegate
    }
    
    func getFilename(assetURL: NSURL) {
        
        var assetLibrary: ALAssetsLibrary = ALAssetsLibrary()

        var resultBlock:ALAssetsLibraryAssetForURLResultBlock = {
            (asset: ALAsset!) in
            
            var response:MediaFilenameResponse!
            var filename:String = ""
            var status:MediaFilenameGeneratorStatus!
            
            if asset != nil {
                
                var assetRep: ALAssetRepresentation = asset.defaultRepresentation()
                filename = assetRep.filename()
                status = MediaFilenameGeneratorStatus.DidGenerateFilename
                
            }
            else {
                status = MediaFilenameGeneratorStatus.DidReceiveError
                
            }
        
            response = MediaFilenameResponse(
                assetURL: assetURL,
                filename: filename,
                status: status
            )
            
            self.delegate.didGenerateFilenameForAssetURL(response)
        }
        
        var failureBlock:ALAssetsLibraryAccessFailureBlock = {
            (error: NSError!) in
            
            var response = MediaFilenameResponse(
                assetURL: assetURL,
                filename: "",
                status: MediaFilenameGeneratorStatus.DidReceiveError
            )
            
            self.delegate.didGenerateFilenameForAssetURL(response)
            
        }
        
        assetLibrary.assetForURL(assetURL, resultBlock, failureBlock)

    }
    
    
    
}