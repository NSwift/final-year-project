//
//  MediaPlayerSettings.swift
//  DynamicAssessmentFramework
//
//  Created by Daniel Parkin on 02/03/2015.
//  Copyright (c) 2015 DanielParkin. All rights reserved.
//

class MediaPlayerSettings {
    private var _callbackWhenMediaFinishes:Bool = false
    
    init(callback:Bool) {
        self._callbackWhenMediaFinishes = callback
    }
}
