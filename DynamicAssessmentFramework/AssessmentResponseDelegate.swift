//
//  AssessmentResponseDelegate.swift
//  DynamicAssessmentFramework
//
//  Created by Daniel Parkin on 27/08/2014.
//  Copyright (c) 2014 DanielParkin. All rights reserved.
//

import Foundation

protocol AssessmentResponseDelegate : class {
    func assessmentDidRespond(response:AssessmentResponse)
}

