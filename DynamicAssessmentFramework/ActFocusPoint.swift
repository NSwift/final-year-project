//
//  ActCountdown.swift
//  DynamicAssessmentFramework
//
//  Created by Daniel Parkin on 21/08/2014.
//  Copyright (c) 2014 Echoyo. All rights reserved.
//

import Foundation
import UIKit

class ActFocusPoint:NSObject, AssessmentActProtocol {
    
    var seconds:Int
    var timer:NSTimer
    weak var delegate:ActResponseDelegate?
    var controls:[NSObject]
    var screenBounds:ScreenBounds
    
    init(seconds:Int, delegate:ActResponseDelegate) {
        self.seconds = seconds
        self.timer = NSTimer()
        self.screenBounds = ScreenBounds()
        self.delegate = delegate
        self.controls = []
    }
    
    func beginDisplayOnView(view: UIView) {
        
        let actView = UIView(frame: screenBounds.bounds())
        actView.backgroundColor = UIColor.grayColor()
        
        var circleWidth:CGFloat = 50
        var circleHeight:CGFloat = 50
        
        var circlePos = screenBounds.centreFromDimensions(
            width: circleWidth,
            height: circleHeight
        )
        
        var circle = UIView(
            frame: CGRect(
                x: circlePos.x,
                y: 100, width:
                circleWidth,
                height: circleHeight
            )
        )
        
        circle.layer.cornerRadius = 50
        circle.backgroundColor = UIColor.orangeColor()
        
        controls.append(circle)
        
        actView.addSubview(circle)
        
        view.addSubview(actView)
        
        // begin timer
        self.timer = NSTimer.scheduledTimerWithTimeInterval(
            1.0,
            target: self,
            selector: "updateTimer:",
            userInfo: nil,
            repeats: true)
    }

    func updateTimer(timer:NSTimer) {
        if seconds > 1 {
            seconds--
        }
        else {
            timer.invalidate()
            let status = ActResponseStatus.CompleteGetNextAct
            let response = ActResponse(status: status, bundle: nil)
            delegate?.actDidRespond(response)
        }
    }
    
}