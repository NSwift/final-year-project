//
//  ActSound.swift
//  DynamicAssessmentFramework
//
//  Created by Daniel Parkin on 21/08/2014.
//  Copyright (c) 2014 Echoyo. All rights reserved.
//

import Foundation
import UIKit
import AVFoundation

class ActSound:NSObject, AssessmentActProtocol, AVAudioPlayerDelegate {
    
    weak var delegate:ActResponseDelegate?
    var audioPlayer = AVAudioPlayer()
    
    init(delegate:ActResponseDelegate)  {
        self.delegate = delegate
        super.init()
    }

    func beginDisplayOnView(view: UIView) {
        let actview = UIView(frame: CGRect(x: 0, y: 0, width: 320, height: 480))
        actview.backgroundColor = UIColor.blackColor()
        
        var label = UILabel(frame: CGRect(x: 0, y: 200, width: 320, height: 100))
        label.text = String("Playing audio")
        label.numberOfLines = 0
        label.textAlignment = NSTextAlignment.Center
        label.font = label.font.fontWithSize(30)
        label.textColor = UIColor.whiteColor()
        
        actview.addSubview(label)
        view.addSubview(view)
    }
    
    func doAction() {
        self.playAudio()
    }
    
    func playAudio() {
        
        var error:NSError?
        
        var beepSound = NSURL(fileURLWithPath: NSBundle.mainBundle().pathForResource("Beeps-6", ofType: "wav")!)
        
        // causes audio to cut out on main av recording feed
        //AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback, error: nil)
        //AVAudioSession.sharedInstance().setActive(true, error: nil)
        
        self.audioPlayer = AVAudioPlayer(contentsOfURL: beepSound, error: &error)
        self.audioPlayer.delegate = self
        self.audioPlayer.prepareToPlay()
        self.audioPlayer.play()
        
    }
    
    func audioPlayerDidFinishPlaying(player: AVAudioPlayer!, successfully flag: Bool) {
        NSLog("Finshed playing audio")
        if flag {
            let status = ActResponseStatus.CompleteGetNextAct
            let response = ActResponse(status: status, bundle: nil)
            delegate?.actDidRespond(response)
        }
    }
    
}
