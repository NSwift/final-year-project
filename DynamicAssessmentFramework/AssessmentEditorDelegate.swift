//
//  EditFocusPointDelegate.swift
//  DynamicAssessmentFramework
//
//  Created by Daniel Parkin on 17/02/2015.
//  Copyright (c) 2015 DanielParkin. All rights reserved.
//

protocol AssessmentEditorDelegate : class {
    func didSaveAssessment(assessment:Assessment)
}
