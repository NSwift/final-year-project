//
//  FocusPointRepository.swift
//  DynamicAssessmentFramework
//
//  Created by Daniel Parkin on 08/01/2015.
//  Copyright (c) 2015 DanielParkin. All rights reserved.
//

class FocusPointRepository: CoreDataRepository {
    
    override init() {
        super.init()
    }

    func getZombieFocusPoints() -> [FocusPoint] {
        
        let request = NSFetchRequest(entityName: "FocusPoint")
        let predicate = NSPredicate(format: "parentAssessment == nil")
        request.returnsObjectsAsFaults = false
        request.predicate = predicate
        
        var results:NSArray = self.context.executeFetchRequest(request, error: nil)!
        
        return results as [FocusPoint]
    }

}
