//
//  Question.h
//  DynamicAssessmentFramework
//
//  Created by Daniel Parkin on 03/03/2015.
//  Copyright (c) 2015 DanielParkin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Answer, Assessment, CustomAnswerPresenter, Result;

@interface Question : NSManagedObject

@property (nonatomic, retain) NSString * answerDisplayBehavior;
@property (nonatomic, retain) NSString * answerType;
@property (nonatomic, retain) NSString * assetURL;
@property (nonatomic, retain) NSString * countdown;
@property (nonatomic, retain) NSString * filename;
@property (nonatomic, retain) NSNumber * hasFocusPoint;
@property (nonatomic, retain) NSString * introText;
@property (nonatomic, retain) NSString * mediaType;
@property (nonatomic, retain) NSString * mediaURL;
@property (nonatomic, retain) NSNumber * pauseTime;
@property (nonatomic, retain) NSNumber * position;
@property (nonatomic, retain) NSString * questionText;
@property (nonatomic, retain) NSString * questionType;
@property (nonatomic, retain) NSNumber * scaleSize;
@property (nonatomic, retain) NSNumber * shouldRecord;
@property (nonatomic, retain) NSOrderedSet *answers;
@property (nonatomic, retain) Answer *correctAnswer;
@property (nonatomic, retain) CustomAnswerPresenter *customAnswerPresenter;
@property (nonatomic, retain) Assessment *parentAssessment;
@property (nonatomic, retain) NSOrderedSet *results;
@end

@interface Question (CoreDataGeneratedAccessors)

- (void)insertObject:(Answer *)value inAnswersAtIndex:(NSUInteger)idx;
- (void)removeObjectFromAnswersAtIndex:(NSUInteger)idx;
- (void)insertAnswers:(NSArray *)value atIndexes:(NSIndexSet *)indexes;
- (void)removeAnswersAtIndexes:(NSIndexSet *)indexes;
- (void)replaceObjectInAnswersAtIndex:(NSUInteger)idx withObject:(Answer *)value;
- (void)replaceAnswersAtIndexes:(NSIndexSet *)indexes withAnswers:(NSArray *)values;
- (void)addAnswersObject:(Answer *)value;
- (void)removeAnswersObject:(Answer *)value;
- (void)addAnswers:(NSOrderedSet *)values;
- (void)removeAnswers:(NSOrderedSet *)values;
- (void)insertObject:(Result *)value inResultsAtIndex:(NSUInteger)idx;
- (void)removeObjectFromResultsAtIndex:(NSUInteger)idx;
- (void)insertResults:(NSArray *)value atIndexes:(NSIndexSet *)indexes;
- (void)removeResultsAtIndexes:(NSIndexSet *)indexes;
- (void)replaceObjectInResultsAtIndex:(NSUInteger)idx withObject:(Result *)value;
- (void)replaceResultsAtIndexes:(NSIndexSet *)indexes withResults:(NSArray *)values;
- (void)addResultsObject:(Result *)value;
- (void)removeResultsObject:(Result *)value;
- (void)addResults:(NSOrderedSet *)values;
- (void)removeResults:(NSOrderedSet *)values;
@end
