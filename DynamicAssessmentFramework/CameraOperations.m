//  CameraOperations.m
//
//  Created by Daniel Parkin on 19/08/2014.


#import "CameraOperations.h"
#import "SuppressErrors.h"

@implementation CameraOperations {
    id _delegate;
    NSString *_outputPath;
    NSURL *_outputURL;
    CALayer *_rootLayer;
    AVCaptureSession *_session;
    AVPlayerLayer *_playerLayer;
    AVCaptureVideoPreviewLayer *_previewLayer;
    AVCaptureMovieFileOutput *_movieFileOutput;
    BOOL _previewEnabled;
}

#pragma mark - Initialisation

-(id)init {
    self = [super init];
    if (self) {
        _previewEnabled = true;
        self.didFinishRecording = false;
        self.didCallBack = false;
        self.id = cameraId + 1;
    }
    return self;
}

-(id)initWithDelegate:(id)delegate {
    self = [super init];
    if (self) {
        _delegate = delegate;
        self.didFinishRecording = false;
        self.didCallBack = false;
        self.id = cameraId++;
    }
    return self;
}

-(id)initWithPreviewEnabled:(BOOL)previewEnabled {
    self = [super init];
    if (self) {
        _previewEnabled = previewEnabled;
        self.didCallBack = false;
        self.id = cameraId++;
    }
    return self;
}

-(id)initWithRootLayer:(CALayer *)rootLayer {
    self = [super init];
    if (self) {
        _previewEnabled = true;
        _rootLayer = rootLayer;
        [self setupSession];
        self.didCallBack = false;
        self.id = cameraId++;
    }
    return self;
}

-(void)setDelegate:(id)delegate {
    _delegate = delegate;
}

#pragma mark - Implementation for static members

static int cameraId;
+ (int) cameraId { @synchronized(self) {
    return cameraId;
    }
}

+ (void) setCameraId:(int)val { @synchronized(self) {
    cameraId = val;
    }
}

+ (void) incrememtCameraId { @synchronized(self) {
    cameraId++;
    }
}

#pragma mark - Set Root Layer
-(void)rootLayer:(CALayer *)rootLayer {
    _rootLayer = rootLayer;
}

#pragma mark - Camera Recording Methods

-(void)startRecording {
    
    [self setupSession];
    
    // setup movie file output
    //NSLog(@"Adding movie file output");
    _movieFileOutput = [[AVCaptureMovieFileOutput alloc] init];
    
    //Float64 TotalSeconds = 60;			//Total seconds
    //int32_t preferredTimeScale = 30;	//Frames per second
    //CMTime maxDuration = CMTimeMakeWithSeconds(TotalSeconds, preferredTimeScale);
    
    //_movieFileOutput.maxRecordedDuration = maxDuration;
    _movieFileOutput.minFreeDiskSpaceLimit = 1024 * 1024;
    
    if ([_session canAddOutput:_movieFileOutput]) {
        [_session addOutput:_movieFileOutput];
    }
    
    // create UUID for the video
    NSString *uuid = [[NSUUID UUID] UUIDString];
    
    // create temporary URL to record to
    _outputPath = [[NSString alloc] initWithFormat:@"%@%@%@", NSTemporaryDirectory(), uuid,
                   [NSString stringWithFormat:@"-output%d-cam&%d&.mov", self.fileNameID, self.id]];
    
    NSLog(@"%@", _outputPath);
    
    _outputURL = [[NSURL alloc] initFileURLWithPath:_outputPath];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if ([fileManager fileExistsAtPath:_outputPath])
    {
        NSError *error;
        if ([fileManager removeItemAtPath:_outputPath error:&error] == NO)
        {
            // handle error if neccessary
        }
    }
    
    // start recording
    [_movieFileOutput startRecordingToOutputFileURL:_outputURL recordingDelegate:self];
    NSLog(@"Camera started recording");
    
}

-(NSURL*)stopRecording {
    [_session stopRunning];
    [_movieFileOutput stopRecording];
    NSLog(@"Camera stopped recording.");
    return _outputURL;
}

#pragma mark - Preview Methods

-(void)startPreview {
    
    [_previewLayer removeFromSuperlayer];
    [_playerLayer removeFromSuperlayer];
    
    // get and log duration
    NSURL *outputURL = [[NSURL alloc] initFileURLWithPath:_outputPath];
    AVURLAsset *videoAsset = [AVURLAsset URLAssetWithURL:outputURL options:nil];
    CMTime videoDuration = videoAsset.duration;
    float fileDurationSeconds = CMTimeGetSeconds(videoDuration);
    
    // set start and end time
    CMTime startTime = CMTimeMakeWithSeconds(10, 30);
    CMTime endTime = CMTimeMakeWithSeconds(fileDurationSeconds, 30);
    
    // output path
    AVAsset *avAsset = [AVAsset assetWithURL:[NSURL fileURLWithPath:_outputPath]];
    AVPlayerItem *avPlayerItem =[[AVPlayerItem alloc]initWithAsset:avAsset];
    avPlayerItem.forwardPlaybackEndTime = endTime;
    AVPlayer *avPlayer = [[AVPlayer alloc]initWithPlayerItem:avPlayerItem];
    _playerLayer =[AVPlayerLayer playerLayerWithPlayer:avPlayer];
    
    [self addLayerToRootLayer:_playerLayer];
    
    [avPlayer seekToTime:startTime];
    [avPlayer play];
  
}

#pragma mark - Session Setup

- (void)setupSession
{
    // session
    _session = [AVCaptureSession new];
    [_session setSessionPreset:AVCaptureSessionPresetHigh];
    
    NSError *error;
    
    // video input device
    AVCaptureDevice *videoCaptureDevice = [self frontFacingCameraOrDefault];
    
    AVCaptureDeviceInput *deviceInput = [AVCaptureDeviceInput deviceInputWithDevice:videoCaptureDevice error:&error];
    
    if ([_session canAddInput:deviceInput]) {
        [_session addInput:deviceInput];
    }
    
    // audio input device
    AVCaptureDevice *audioCaptureDevice = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeAudio];
    
    error = nil;
    
    AVCaptureDeviceInput *audioInput = [AVCaptureDeviceInput deviceInputWithDevice:audioCaptureDevice error:&error];
    
    if ([_session canAddInput:audioInput]) {
        [_session addInput:audioInput];
    }
    
    // preview layer
    _previewLayer = [[AVCaptureVideoPreviewLayer alloc] initWithSession:_session];
    [_previewLayer setVideoGravity:AVLayerVideoGravityResizeAspectFill];

    if (_previewEnabled) {
        [self addLayerToRootLayer:_previewLayer];
    }

    [_session startRunning];
    
}

#pragma mark - AVCaptureFileOutputRecordingDelegate

-(void)captureOutput:(AVCaptureFileOutput *)captureOutput didFinishRecordingToOutputFileAtURL:(NSURL *)outputFileURL fromConnections:(NSArray *)connections error:(NSError *)error
{
    
    self.didFinishRecording = true;
    [_delegate performSelector:@selector(cameraDidFinishRecording:)
                    withObject:_outputURL];
}

#pragma mark - Support Methods

-(AVCaptureDevice *)frontFacingCameraOrDefault
{
    NSArray *videoDevices = [AVCaptureDevice devicesWithMediaType:AVMediaTypeVideo];
    AVCaptureDevice *captureDevice = nil;
    for (AVCaptureDevice *device in videoDevices)
    {
        if (device.position == AVCaptureDevicePositionFront)
        {
            captureDevice = device;
            break;
        }
    }
    
    if (!captureDevice)
    {
        captureDevice = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    }
    
    return captureDevice;
}

-(void)addLayerToRootLayer:(CALayer *)layerToAdd {
    [_rootLayer setMasksToBounds:YES];
    [layerToAdd setFrame:CGRectMake(0, 0, 300, 300)];
    [_rootLayer insertSublayer:layerToAdd atIndex:10];
}

-(void)stopSession {
    
    // remove video input
    AVCaptureInput* input = [_session.inputs objectAtIndex:0];
    AVCaptureInput* input1 = [_session.inputs objectAtIndex:1];
    [_session removeInput:input];
    [_session removeInput:input1];
    
    // remove output if any
    if([_session.outputs count] > 0) {
        AVCaptureVideoDataOutput* output = [_session.outputs objectAtIndex:0];
        [_session removeOutput:output];
    }
    
    if ([_session isRunning]) {
        [_session stopRunning];
    }
}

// override dealloc method
-(void)dealloc {
    [self stopSession];
    NSLog(@"CameraOperations was dealloced!");
}

@end
