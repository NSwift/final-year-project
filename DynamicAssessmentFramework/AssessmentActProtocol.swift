//
//  AssmentSceneDisplayable.swift
//  DynamicAssessmentFramework
//
//  Created by Daniel Parkin on 20/08/2014.
//  Copyright (c) 2014 Echoyo. All rights reserved.
//

import Foundation
import UIKit


protocol AssessmentActProtocol {
    weak var delegate:ActResponseDelegate? { get set }
    func beginDisplayOnView(view:UIView)
}