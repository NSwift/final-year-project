//
//  Participant.h
//  DynamicAssessmentFramework
//
//  Created by Daniel Parkin on 09/01/2015.
//  Copyright (c) 2015 DanielParkin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Result;

@interface Participant : NSManagedObject

@property (nonatomic, retain) NSString * id;
@property (nonatomic, retain) NSString * dateOfBirth;
@property (nonatomic, retain) NSOrderedSet *results;
@end

@interface Participant (CoreDataGeneratedAccessors)

- (void)insertObject:(Result *)value inResultsAtIndex:(NSUInteger)idx;
- (void)removeObjectFromResultsAtIndex:(NSUInteger)idx;
- (void)insertResults:(NSArray *)value atIndexes:(NSIndexSet *)indexes;
- (void)removeResultsAtIndexes:(NSIndexSet *)indexes;
- (void)replaceObjectInResultsAtIndex:(NSUInteger)idx withObject:(Result *)value;
- (void)replaceResultsAtIndexes:(NSIndexSet *)indexes withResults:(NSArray *)values;
- (void)addResultsObject:(Result *)value;
- (void)removeResultsObject:(Result *)value;
- (void)addResults:(NSOrderedSet *)values;
- (void)removeResults:(NSOrderedSet *)values;
@end
