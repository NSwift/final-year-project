//
//  PreviewAnswerPresenterViewController.swift
//  DynamicAssessmentFramework
//
//  Created by Daniel Parkin on 17/02/2015.
//  Copyright (c) 2015 DanielParkin. All rights reserved.
//

import UIKit

class PreviewAnswerPresenterViewController : UIViewController {
    
    override func viewDidLoad() {
        println("Preview view loaded")
    }
    
}
