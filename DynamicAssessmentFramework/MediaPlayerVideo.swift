//
//  MediaPlayerVideo.swift
//  DynamicAssessmentFramework
//
//  Created by Daniel Parkin on 01/10/2014.
//  Copyright (c) 2014 DanielParkin. All rights reserved.
//

import UIKit
import AssetsLibrary
import Foundation
import AVFoundation

class MediaPlayerVideo: NSObject, MediaPlayerProtocol {
   
    // change to media player delegate
    weak var mediaDelegate:MediaPlayerDelegate?
    weak var view:UIView!
    var assetURL:NSURL!
    var videoPlayer = AVPlayer()
    
    let screenBounds = ScreenBounds()
    
    private var _appState:ApplicationState = ApplicationState.Assessment
    
    private var _previewBtnHeight:CGFloat = 50
    private var _previewBtnWidth:CGFloat = 50
    private var _previewBtnPlayXOffset:CGFloat = 0
    private var _previewBtnPauseXOffset:CGFloat = 0
    private var _previewBtnYOffset:CGFloat = 0
    
    private let _previewBtnYPadding:CGFloat = 10
    private let _previewBtnXPadding:CGFloat = 10
    private let _colorHelper = ColorHelper()
    private let _fontHelper = FontHelper()
    
    override init() {
    }
    
    init(appState:ApplicationState, delegate:MediaPlayerDelegate)
    {
        self.mediaDelegate = delegate
        self._appState = appState
    }
    
    func removeAllSubviews() {
        self.view.subviews.map { $0.removeFromSuperview() }
    }
    
    func addReplayButton() {
        
        var btnReplay:DPUIButtonAnswer = DPUIButtonAnswer(
            frame: CGRect(
                x: 50,
                y: 50,
                width: 200,
                height: 100),
            rounded:true,
            animates: false
        )
        
        btnReplay.backgroundColor = UIColor.whiteColor()
        
        btnReplay.setTitleColor(UIColor.blackColor(), forState: UIControlState.Normal)
        
        btnReplay.setTitle("Replay", forState: UIControlState.Normal)
        btnReplay.addTarget(self, action: "replayMedia", forControlEvents: UIControlEvents.TouchUpInside)
        
        btnReplay.clipsToBounds = true
        
        screenBounds.positionViewCentral(
            thisView: btnReplay,
            inCenterOf: self.view
        )
        
        self.view.addSubview(btnReplay)
        
    }
    
    func addPlayButton() {
        
        var btnPlay:UIButton = UIButton(
            frame: CGRect(
                x: _previewBtnPlayXOffset,
                y: _previewBtnYOffset,
                width: _previewBtnWidth,
                height: _previewBtnHeight)
        )
        
        btnPlay.titleLabel!.font = _fontHelper.fontGlyphicons(10)
        btnPlay.backgroundColor = _colorHelper.colorPrimary2()
        btnPlay.setTitleColor(UIColor.blackColor(), forState: UIControlState.Normal)
        btnPlay.setTitle("b", forState: UIControlState.Normal)
        btnPlay.addTarget(self, action: "play", forControlEvents: UIControlEvents.TouchUpInside)
        
        btnPlay.clipsToBounds = true
        
        self.view.addSubview(btnPlay)
        
    }
    
    func addPauseButton() {
        
        var btnPause:UIButton = UIButton(
            frame: CGRect(
                x: _previewBtnPauseXOffset,
                y: _previewBtnYOffset,
                width: _previewBtnWidth,
                height: _previewBtnHeight)
        )
        
        btnPause.titleLabel!.font = _fontHelper.fontGlyphicons(10)
        btnPause.backgroundColor = _colorHelper.colorPrimary3()
        btnPause.setTitleColor(UIColor.blackColor(), forState: UIControlState.Normal)
        btnPause.setTitle("g", forState: UIControlState.Normal)
        btnPause.addTarget(self, action: "pause", forControlEvents: UIControlEvents.TouchUpInside)
        
        btnPause.clipsToBounds = true
        
        self.view.addSubview(btnPause)
        
    }
    
    func play() {
        
        var assetLibrary: ALAssetsLibrary = ALAssetsLibrary()
        
        var urlString:NSString = self.assetURL.absoluteString!;
        
        if urlString.containsString("assets-library") {
            
            assetLibrary.assetForURL(self.assetURL, resultBlock: {
                (asset: ALAsset!) in
                if asset != nil {
                    
                    // Remove all subviews
                    self.view.subviews.map { $0.removeFromSuperview() }
                    
                    var assetRep: ALAssetRepresentation = asset.defaultRepresentation()
                    let url = assetRep.url()

                    let videoAsset = AVURLAsset.assetWithURL(url) as AVURLAsset
                    let item = AVPlayerItem(asset: videoAsset)
                    self.videoPlayer = AVPlayer.playerWithPlayerItem(item) as AVPlayer
                    let layer = AVPlayerLayer(player: self.videoPlayer)
                    let viewFrame:CGRect = self.view.frame
                    layer.frame = CGRectMake(0, 0, viewFrame.size.width, viewFrame.size.height)
                    layer.videoGravity = AVLayerVideoGravityResizeAspectFill
                    
                    self.view.layer.addSublayer(layer)
                    
                    if let delegate = self.mediaDelegate {
                        
                        // subscribe to the notification for video ending
                        NSNotificationCenter.defaultCenter().addObserver(self, selector: "mediaDidFinishPlaying:", name: AVPlayerItemDidPlayToEndTimeNotification, object: nil)
                        
                    }
                    
                    // if we're in preview mode add media control buttons
                    if self._appState == ApplicationState.Preview {
                        self.addPlayButton()
                        self.addPauseButton()
                    }
                    
                    self.videoPlayer.play()
                    
                }
                }, failureBlock: {
                    (error: NSError!) in
                    
                    if error != nil {
                        NSLog("Error!")
                    }
                }
            )
            
        }
        else {
            let videoAsset = AVURLAsset.assetWithURL(self.assetURL) as AVURLAsset
            let item = AVPlayerItem(asset: videoAsset)
            self.videoPlayer = AVPlayer.playerWithPlayerItem(item) as AVPlayer
            let layer = AVPlayerLayer(player: self.videoPlayer)
            let viewFrame:CGRect = view!.frame
            layer.frame = CGRectMake(0, 0, viewFrame.size.width, viewFrame.size.height)
            view!.layer.addSublayer(layer)
            self.videoPlayer.play()
        }

    }
    
    func pause() {
        self.videoPlayer.pause()
    }
    
    func playMedia(url: NSURL, onView view: UIView){
        
        self.assetURL = url
        self.view = view
        
        switch(_appState) {
        case .Assessment:
            self.play()
        case .Review:
            self.view.addSubview(getStillImage())
            
        case .Preview:
            
            // if we're in preview mode, add the loading spinner
            if self._appState == ApplicationState.Preview {
                let viewHeight = view.frame.height
                let viewWidth = view.frame.width
                let spinner = DPSpinner(message: "Loading...", height: viewHeight, width: viewWidth)
                spinner.showInView(view)
            }
            
            let stillImage = getStillImage()
            view.subviews.map { $0.removeFromSuperview() }
            
            self.view.addSubview(stillImage)
            self.calculateControlButtonSizing()
            self.addPlayButton()
            self.addPauseButton()
        }
    
    }
    
    func calculateControlButtonSizing() {
        let viewHeight = self.view.frame.height
        let viewWidth = self.view.frame.width
        
        _previewBtnHeight = viewHeight / CGFloat(4)
        _previewBtnWidth = viewWidth / CGFloat(2)
        _previewBtnYOffset = viewHeight - _previewBtnHeight
        _previewBtnPlayXOffset = 0
        _previewBtnPauseXOffset = _previewBtnPlayXOffset + _previewBtnWidth
    }
    
    func replayMedia() {
        self.mediaDelegate!.didReplayMedia!()
        self.play()
    }
    
    func getStillImage() -> UIView {
        
        var avAsset = AVURLAsset(URL: self.assetURL, options: nil)
        var imageGen = AVAssetImageGenerator(asset: avAsset)
        var time = CMTimeMake(1, 2)
        
        imageGen.appliesPreferredTrackTransform = true
        
        var imageRef:CGImageRef = imageGen.copyCGImageAtTime(time, actualTime: nil, error: nil)
        
        var importedImage:UIImage = UIImage(CGImage: imageRef)!
        
        /*
        let viewWidth = view.frame.width
        let viewHeight = view.frame.height
        
        var newSize:CGSize = CGSize(width: viewWidth, height: viewHeight)
        let rect = CGRectMake(0, 0, newSize.width, newSize.height)
        UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
        importedImage.drawInRect(rect)
        
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        let resizedImageData = UIImageJPEGRepresentation(newImage, 0.0)
        */
        
        // resize and compress image
        let imageCompressor = ImageCompressor()
        let newSize = imageCompressor.sizeFromView(view: view)
        
        let compressedImage = imageCompressor.compress(
            image: importedImage,
            newSize: newSize,
            quality: 0.2
        )

        var imageView = UIImageView(image: compressedImage)
        
        return imageView
    }
    
    // inform delegate that the media finished playing
    func mediaDidFinishPlaying(notification:NSNotification) {
        
        switch(_appState) {
        case .Assessment:
            self.removeAllSubviews()
            self.addReplayButton()
        case .Preview:
            println("Preview")
        default:
            println("Default")
        }

        NSNotificationCenter.defaultCenter().removeObserver(self)
        self.mediaDelegate!.didFinishPlayingMedia()
    }
    
    deinit {
        NSNotificationCenter.defaultCenter().removeObserver(self)
        self.videoPlayer.pause()
        println("Media player video dealloc")
    }
    
}
