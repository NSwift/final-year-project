//
//  MediaImporterImage.swift
//  DynamicAssessmentFramework
//
//  Created by Daniel Parkin on 27/09/2014.
//  Copyright (c) 2014 DanielParkin. All rights reserved.
//

import UIKit
import MediaPlayer
import AssetsLibrary

class MediaImporterAudio: NSObject, MediaImporterProtocol, MPMediaPickerControllerDelegate {
    
    // MARK: Properties
    var mediaURL:NSURL = NSURL()
    weak var delegate:MediaImporterDelegate?
    let audioPicker = MPMediaPickerController()
    
    // MARK: Init
    override init() {
        super.init()
        self.audioPicker.delegate = self
    }
    
    // MARK: Media importer protocol methods
    func controller() -> UIViewController {
        return audioPicker
    }
    
    // MARK: MPMediaPickercontrollerDelegate methods
    func mediaPicker(mediaPicker: MPMediaPickerController!, didPickMediaItems mediaItemCollection: MPMediaItemCollection!) {
        
        let url:NSURL = mediaItemCollection.representativeItem.assetURL
    
        self.getFilename(url)
        
        delegate!.didImportMedia(url)
        
        audioPicker.dismissViewControllerAnimated(true, completion: nil)

    }
    
    func mediaPickerDidCancel(mediaPicker: MPMediaPickerController!) {
        audioPicker.dismissViewControllerAnimated(true, completion: nil)
    }

    func getFilename(url: NSURL) {
        
        var array = url.absoluteString!.componentsSeparatedByString("=")
        var id:String = array[1]
        
        
        var query = MPMediaQuery()
        var predicate = MPMediaPropertyPredicate(value: id, forProperty: MPMediaItemPropertyPersistentID)
        
        query.addFilterPredicate(predicate)
        
        var items = query.items as [MPMediaItem]
        var mediaAsset:MPMediaItem = items[0]
        
        println(mediaAsset.title)
        
    }
    
}
