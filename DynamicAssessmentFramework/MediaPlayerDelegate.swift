//
//  MediaPlayerFinishedDelegate.swift
//  DynamicAssessmentFramework
//
//  Created by Daniel Parkin on 06/01/2015.
//  Copyright (c) 2015 DanielParkin. All rights reserved.
//

import UIKit

@objc protocol MediaPlayerDelegate : class {
    func didFinishPlayingMedia()
    optional func didReplayMedia()
}