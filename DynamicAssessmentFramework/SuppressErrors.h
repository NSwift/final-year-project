//
//  SuppressErrors.h
//  DynamicAssessmentFramework
//
//  Created by Daniel Parkin on 08/01/2015.
//  Copyright (c) 2015 DanielParkin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SuppressErrors : NSObject

-(void)cameraDidFinishRecording:(NSString *)url;

@end
