//
//  AssessmentResponse.swift
//  DynamicAssessmentFramework
//
//  Created by Daniel Parkin on 27/08/2014.
//  Copyright (c) 2014 DanielParkin. All rights reserved.
//

import UIKit

class AssessmentResponse:NSObject {
    var status:AssessmentResponseStatus
    var videoURLS:[Question: NSURL]
    var answerBundles:[Question: AnswerBundle]
    
    init(status:AssessmentResponseStatus, answerBundles:[Question: AnswerBundle], videoURLS:[Question: NSURL]) {
        self.status = status
        self.answerBundles = answerBundles
        self.videoURLS = videoURLS
    }
}