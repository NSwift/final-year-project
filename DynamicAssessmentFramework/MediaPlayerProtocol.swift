//
//  ProtocolAnswerPresentation.swift
//  QuestionsTests
//
//  Created by Daniel Parkin on 19/08/2014.
//  Copyright (c) 2014 Echoyo. All rights reserved.
//

import Foundation
import UIKit

protocol MediaPlayerProtocol {
    weak var mediaDelegate:MediaPlayerDelegate? { get set }
    func playMedia(url:NSURL, onView view:UIView)
}