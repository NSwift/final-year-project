//
//  FileExportTypes.swift
//  DynamicAssessmentFramework
//
//  Created by Daniel Parkin on 16/02/2015.
//  Copyright (c) 2015 DanielParkin. All rights reserved.
//

enum FileExportType:String {
    case TXT = "txt"
    case CSV = "csv"
    case PNG = "png"
    case PDF = "pdf"
}