//
//  SceneResponse.swift
//  DynamicAssessmentFramework
//
//  Created by Daniel Parkin on 20/08/2014.
//  Copyright (c) 2014 Echoyo. All rights reserved.
//

import Foundation

class SceneResponse:NSObject {
    var status:SceneResponseStatus
    var bundle:AnswerBundle?
    
    init(status:SceneResponseStatus) {
        self.status = status
    }
    
    init(status:SceneResponseStatus, bundle:AnswerBundle?) {
        self.status = status
        self.bundle = bundle
    }
}