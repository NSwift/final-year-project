//
//  AnswerPresenterOptionRepositoru.swift
//  DynamicAssessmentFramework
//
//  Created by Daniel Parkin on 19/02/2015.
//  Copyright (c) 2015 DanielParkin. All rights reserved.
//

class AnswerPresenterOptionRepository: CoreDataRepository {
    
    override init() {
        super.init()
    }
    
    func getOptionsByAnswerPresenter(answerPresenter:CustomAnswerPresenter) -> [AnswerPresenterOption] {
        
        let request = NSFetchRequest(entityName: "AnswerPresenterOption")
        let sortDescriptor = NSSortDescriptor(key: "x", ascending: true)
        let predicate = NSPredicate(format: "parentPresenter == %@", answerPresenter)
        request.returnsObjectsAsFaults = false
        request.predicate = predicate
        request.sortDescriptors = [sortDescriptor]
        
        var results:NSArray = self.context.executeFetchRequest(request, error: nil)!
        
        return results as [AnswerPresenterOption]
        
    }
    
}