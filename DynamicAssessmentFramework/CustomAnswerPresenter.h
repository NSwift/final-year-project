//
//  CustomAnswerPresenter.h
//  DynamicAssessmentFramework
//
//  Created by Daniel Parkin on 18/02/2015.
//  Copyright (c) 2015 DanielParkin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class AnswerPresenterOption, Question;

@interface CustomAnswerPresenter : NSManagedObject

@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSSet *questions;
@property (nonatomic, retain) NSSet *options;
@end

@interface CustomAnswerPresenter (CoreDataGeneratedAccessors)

- (void)addQuestionsObject:(Question *)value;
- (void)removeQuestionsObject:(Question *)value;
- (void)addQuestions:(NSSet *)values;
- (void)removeQuestions:(NSSet *)values;

- (void)addOptionsObject:(AnswerPresenterOption *)value;
- (void)removeOptionsObject:(AnswerPresenterOption *)value;
- (void)addOptions:(NSSet *)values;
- (void)removeOptions:(NSSet *)values;

@end
