//
//  FocusPoint.h
//  DynamicAssessmentFramework
//
//  Created by Daniel Parkin on 17/02/2015.
//  Copyright (c) 2015 DanielParkin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Assessment;

@interface FocusPoint : NSManagedObject

@property (nonatomic, retain) NSString * assetURL;
@property (nonatomic, retain) NSString * backgroundColor;
@property (nonatomic, retain) NSNumber * height;
@property (nonatomic, retain) NSNumber * rotation;
@property (nonatomic, retain) NSNumber * scale;
@property (nonatomic, retain) NSNumber * width;
@property (nonatomic, retain) NSNumber * x;
@property (nonatomic, retain) NSNumber * y;
@property (nonatomic, retain) Assessment *parentAssessment;

@end
