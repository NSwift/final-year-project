//
//  EditFocusPointDelegate.swift
//  DynamicAssessmentFramework
//
//  Created by Daniel Parkin on 17/02/2015.
//  Copyright (c) 2015 DanielParkin. All rights reserved.
//

protocol QuestionEditorDelegate : class {
    func didSaveQuestion(question:Question)
    func didCancelWithoutSaving()
}
