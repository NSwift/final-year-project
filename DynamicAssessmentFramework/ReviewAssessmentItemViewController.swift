//
//  ReviewAssessmentPageViewController.swift
//  DynamicAssessmentFramework
//
//  Created by Daniel Parkin on 10/01/2015.
//  Copyright (c) 2015 DanielParkin. All rights reserved.
//

import UIKit

class ReviewAssessmentItemViewController: UIViewController {
    
    // MARK: - Variables
    var itemIndex: Int = 0
    var question:ActQuestion!

    // MARK: - View Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()

        self.question!.beginDisplayOnView(self.view)
        
    }

}
