//
//  CoreDataAccess.swift
//  DynamicAssessmentFramework
//
//  Created by Daniel Parkin on 26/08/2014.
//  Copyright (c) 2014 Echoyo. All rights reserved.
//

import UIKit
import CoreData

class CoreDataAccess: CoreDataBase {
   
    func save() {
        context.save(nil)
    }
    
    func deleteObject(object:NSManagedObject) {
        context.deleteObject(object)
        self.save()
    }
    
    func newObjectForEntity(entity:CDEntity) -> NSManagedObject? {
        if let obj = createObjectFromEntity(entity) {
            self.save()
            return obj
        }
        else {
            return nil
        }
    }
    
    func createObjectFromEntity(entityName:CDEntity) -> NSManagedObject? {
        
        let entity = NSEntityDescription.entityForName(entityName.rawValue, inManagedObjectContext: context)
        
        switch(entityName) {
        case CDEntity.Assessment:
            return Assessment(entity: entity!, insertIntoManagedObjectContext: context)
        case CDEntity.Question:
            return Question(entity: entity!, insertIntoManagedObjectContext: context)
        case CDEntity.Answer:
            return Answer(entity: entity!, insertIntoManagedObjectContext: context)
        case CDEntity.Result:
            return Result(entity: entity!, insertIntoManagedObjectContext: context)
        case CDEntity.FocusPoint:
             return FocusPoint(entity: entity!, insertIntoManagedObjectContext: context)
        case CDEntity.Participant:
             return Participant(entity: entity!, insertIntoManagedObjectContext: context)
        case CDEntity.AnswerPresenterOption:
            return AnswerPresenterOption(entity: entity!, insertIntoManagedObjectContext: context)
        case CDEntity.CustomAnswerPresenter:
            return CustomAnswerPresenter(entity: entity!, insertIntoManagedObjectContext: context)
        default:
            return nil
        }
    }
    
    func assessmentRepository() -> AssessmentRepository {
        return AssessmentRepository()
    }
    
    func questionRepository() -> QuestionRepository {
        return QuestionRepository()
    }
    
    func participantRepository() -> ParticipantRepository {
        return ParticipantRepository()
    }
    
    func resultRepository() -> ResultRepository {
        return ResultRepository()
    }
    
    func answerPresenterOptionRepository() -> AnswerPresenterOptionRepository {
        return AnswerPresenterOptionRepository()
    }
    
    func focusPointRepository() -> FocusPointRepository {
        return FocusPointRepository()
    }
    
    func answerRepository() -> AnswerRepository {
        return AnswerRepository()
    }
    
    func clearZombieEntities() {
        let zombieQuestions = self.questionRepository().getZombieQuestions()
        let zombieAnswers = self.answerRepository().getZombieAnswers()
        let zombieFocusPoints = self.focusPointRepository().getZombieFocusPoints()
        let zombieResults = self.resultRepository().getZombieResults()
        
        zombieQuestions.map({self.deleteObject($0)})
        zombieAnswers.map({self.deleteObject($0)})
        zombieFocusPoints.map({self.deleteObject($0)})
        zombieResults.map({self.deleteObject($0)})        
    }
}
