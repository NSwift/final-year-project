//
//  AssessmentRepository.swift
//  DynamicAssessmentFramework
//
//  Created by Daniel Parkin on 08/01/2015.
//  Copyright (c) 2015 DanielParkin. All rights reserved.
//

class AssessmentRepository: CoreDataRepository {
    
    override init() {
        super.init()
    }
    
    func getAllAssessments() -> [Assessment] {
        
        let fetch = NSFetchRequest(entityName: CDEntity.Assessment.rawValue)
        fetch.returnsObjectsAsFaults = false
        
        return context.executeFetchRequest(fetch, error: nil) as [Assessment]
    }
    
    func getAllAssessmentsWithResults() -> [Assessment] {
        return getAllAssessments().filter({ $0.results.count > 0 })
    }
    
    func getAssessmentCount() -> Int {
        return getAllAssessments().count
    }
    
    func getAssessmentsWithBlankTitles() -> [Assessment] {
        
        let request = NSFetchRequest(entityName: "Assessment")
        //let predicate = NSPredicate(format: "title == %@", "")
        let predicate = NSPredicate(format: "title == nil")
        request.returnsObjectsAsFaults = false
        request.predicate = predicate
        
        var results:[Assessment] = self.context.executeFetchRequest(request, error: nil) as [Assessment]
        
        return results
        
    }
    
    func assessmentNameExists(name:String) -> Bool {
        
        let request = NSFetchRequest(entityName: "Assessment")
        let predicate = NSPredicate(format: "title == [c]%@", name)
        request.returnsObjectsAsFaults = false
        request.predicate = predicate
        
        var results:[Assessment] = self.context.executeFetchRequest(request, error: nil) as [Assessment]
        
        return results.count > 0
    }
    
    func getZombieAnswers() -> [Answer] {
        
        let request = NSFetchRequest(entityName: "Answer")
        let predicate = NSPredicate(format: "parentQuestion == nil")
        request.returnsObjectsAsFaults = false
        request.predicate = predicate
        
        var results:NSArray = self.context.executeFetchRequest(request, error: nil)!
        
        return results as [Answer]
    }
    
}
