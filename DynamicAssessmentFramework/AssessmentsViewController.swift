//
//  AssessmentsViewController.swift
//  DynamicAssessmentFramework
//
//  Created by Daniel Parkin on 25/08/2014.
//  Copyright (c) 2014 Echoyo. All rights reserved.
//

import UIKit
import CoreData

enum AssessmentState {
    case Begin
    case Review
}

class AssessmentsViewController: UIViewController, UITableViewDelegate, UIActionSheetDelegate, UITableViewDataSource, DPModalMenuDelegate, DPConfirmationViewDelegate, AssessmentEditorDelegate, AssessmentViewControllerDelegate, DPTextFieldViewDelegate {

    // MARK: Init
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    // MARK: Properties
    var selectedIndex:Int = 0
    var assessments:Array<Assessment> = []
    var modalMenu:DPModalMenu!
    
    private var _selectedIndexPath:NSIndexPath!
    private var _infoView:DPInfoView!
    private var _confView:DPConfirmationView!
    private var _textFieldView:DPTextFieldView!
    
    private let _assessmentRowSize:CGFloat = 120
    private let _colorHelper = ColorHelper()
    
    let actionSheet:UIActionSheet = UIActionSheet()
    let CDA = CoreDataAccess()
    
    // MARK: IBOutlets
    @IBOutlet var tvAssessments : UITableView!
    
    // MARK: View Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tvAssessments.delegate = self
        tvAssessments.dataSource = self

        self.modalMenu = self.initModalMenu()
        
        self.recoverPartialAssessments()
        self.populateAssessments()
        
        // mop up zombie CD entities
        CDA.clearZombieEntities()
    
    }
    
    override func viewDidAppear(animated: Bool) {
        self.populateAssessments()
        tvAssessments.reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject!) {
  
        if segue.identifier == "AddAssessment" {
            var newAssessment = CDA.newObjectForEntity(CDEntity.Assessment) as Assessment
            
            CDA.save()
                
            let vcEditAssessment = segue.destinationViewController as EditAssessmentViewController
            
            vcEditAssessment.assessment = newAssessment
            vcEditAssessment.delegate = self
        }
    }
    
    // MARK: UITableViewDataSource methods
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return assessments.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cellID = "CellAssessments"
        
        var cell = tableView.dequeueReusableCellWithIdentifier(cellID) as UITableViewCell
        
        let assessment = assessments[indexPath.row] as Assessment
        let lvTitle = cell.viewWithTag(1) as UILabel
        let lvQuestionCount = cell.viewWithTag(2) as UILabel
        let lvCompletionCount = cell.viewWithTag(3) as UILabel
        
        let questionCount = assessment.questions.count
        let completionCount = CDA.participantRepository().getParticipantCountForAssessment(assessment)
        
        lvTitle.text = assessment.title
        lvQuestionCount.text = String(questionCount)
        lvCompletionCount.text = String(completionCount)
        
        return cell
    }
    
    func tableView(tableView:UITableView!, heightForRowAtIndexPath indexPath:NSIndexPath)->CGFloat
    {
        return _assessmentRowSize
    }
    
    func tableView(tableView: UITableView!, didSelectRowAtIndexPath indexPath: NSIndexPath!) {
        
        if let path = indexPath {
            selectedIndex = path.row
        }
    
        modalMenu.showInView(self.view)
  
    }
    
    func tableView(tableView: UITableView!, canEditRowAtIndexPath indexPath: NSIndexPath!) -> Bool {
        return true
    }
    
    func tableView(tableView: UITableView!, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath!) {
        
        if editingStyle == UITableViewCellEditingStyle.Delete {
            
            _selectedIndexPath = indexPath
            _confView = DPConfirmationView(
                delegate: self,
                messages: ["Are you sure you want to delete this assessment?"]
            )
            
            _confView.showInView(self.view)
                        
        }
        
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        self.tvAssessments.deselectRowAtIndexPath(indexPath, animated: true)
    }
    
    
    // MARK: init DPModalMenu
    func initModalMenu() -> DPModalMenu {
        
        let menuItemBegin = DPModalMenuItem(itemText: "Begin", iconCharacter: "b")
        let menuItemEdit = DPModalMenuItem(itemText: "Edit", iconCharacter: "e")
        let menuItemReview = DPModalMenuItem(itemText: "Review", iconCharacter: "r")
        let menuItemCancel = DPModalMenuItem(itemText: "Cancel", iconCharacter: "c")

        let menuItems = [menuItemBegin, menuItemEdit, menuItemReview, menuItemCancel]
        let modalMenu = DPModalMenu(menuItems: menuItems, delegate: self)
        
        return modalMenu
    }
    
    // MARK: DPModalMenuDelegate
    func didSelectMenuItem(itemIndex:Int) {

        let assessment = assessments[selectedIndex] as Assessment
        var validationResult:ValidationResult!
    
        // show context menu
        switch (itemIndex) {
        case 0:
            
            validationResult = checkAssessmentIsValid(
                assessment,
                state: AssessmentState.Begin
            )
            
            if !validationResult.isValid {
                self.pushErrorView(validationResult)
            }
            else {
                 self.pushBeginView()
            }
            
        case 1:
            
            let validationResult = checkAssessmentIsValidToEdit(assessment)
            
            if !validationResult.isValid {
                self.pushErrorView(validationResult)
            }
            else {
                self.pushEditView()
            }

        case 2:
            
            modalMenu.removeFromSuperView()
            
            validationResult = checkAssessmentIsValid(
                assessment,
                state: AssessmentState.Review
            )
            
            if !validationResult.isValid {
                self.pushErrorView(validationResult)
            }
            else {
                showPasswordDialogue()
            }
            
        case 3:
            let indexPath = self.tvAssessments.indexPathForSelectedRow()
            self.tvAssessments.deselectRowAtIndexPath(indexPath!, animated: true)
            
        default:
            println("default option")
        }
        
    }
    
    // MARK: CoreData methods
    func populateAssessments() {
        assessments = CDA.assessmentRepository().getAllAssessments()
    }
    
    // MARK: Validate assessment
    func checkAssessmentIsValid(assessment:Assessment, state:AssessmentState) -> ValidationResult {
        
        var errors:[String] = [];
        var isValid = true
        
        let errorNoQuestions = "This assessment has no questions configured."
        let errorNoResults = "This assessment has no results yet."
        
        if assessment.questions.count == 0 {
            isValid = false
            errors.append(errorNoQuestions)
        }
        
        // additional checks for other states
        if state == AssessmentState.Review {
           
            let resultCount = CDA.resultRepository().getResultCountForAssessment(assessment)
            
            if resultCount == 0 {
                isValid = false
                errors.append(errorNoResults)
            }
            
        }

        return ValidationResult(isValid: isValid, errorMessages: errors)
    }
    
    func checkAssessmentIsValidToEdit(assessment:Assessment) -> ValidationResult {
        
        var errors:[String] = [];
        var isValid = true
        
        let resultCount = CDA.resultRepository().getResultCountForAssessment(assessment)
        
        if resultCount > 0 {
            errors.append("This assessment already has results and is now locked for editing.")
            isValid = false
        }
        
        return ValidationResult(isValid: isValid, errorMessages: errors)
    }
    
    // MARK: Recovery
    func recoverPartialAssessments() {
        var recoveredAssessments = CDA.assessmentRepository().getAssessmentsWithBlankTitles()

        recoveredAssessments.map({$0.title = SystemAssessmentName.Recovered.rawValue})
        CDA.save()
    }
    
    // MARK: Push views
    func pushErrorView(validationResult:ValidationResult) {
        let errorMessages = validationResult.errorMessages
        
        //remove the modal menu if it's on screen
        modalMenu.removeFromSuperView()
        
        _infoView = DPInfoView(messages: errorMessages, type: InfoViewType.Error)
        _infoView.showInView(self.view)
        
    }
    
    func pushReviewView() {
        var reviewViewController = self.storyboard?.instantiateViewControllerWithIdentifier("ReviewAssessment") as ReviewAssessmentViewController
        
        reviewViewController.assessment = assessments[selectedIndex] as Assessment
        
        self.navigationController?.pushViewController(reviewViewController, animated: true)
    }
    
    func pushBeginView() {
        var orientationViewController = self.storyboard?.instantiateViewControllerWithIdentifier("BeginOrientation") as OrientationViewController
        
        orientationViewController.assessment = assessments[selectedIndex] as Assessment
        
        orientationViewController.assessmentDelegate = self
        
        self.navigationController?.pushViewController(orientationViewController, animated: true)
    }
    
    func pushEditView() {
        
        var editView = self.storyboard?.instantiateViewControllerWithIdentifier("EditAssessment") as EditAssessmentViewController
        
        editView.assessment = assessments[selectedIndex] as Assessment
        editView.delegate = self
        
        self.navigationController?.pushViewController(editView, animated: true)
        
    }
    
    func showPasswordDialogue() {
        _textFieldView = DPTextFieldView(delegate: self, messages: ["Password required"])
        _textFieldView.showInView(self.view)
    }
    
    func validatePassword(password:String) {
        
        // password is hardcored and replicates the functionality of the original app
        if password == "0000" {
            pushReviewView()
        }
        else {
            let error = ValidationResult(isValid: false, errorMessages: ["Password incorrect"])
            pushErrorView(error)
        }
    }
    
    // MARK: DPConfirmationViewDelegate
    func didPressOk() {
        
        // confirms deletion of an assessment
        CDA.deleteObject(assessments[_selectedIndexPath!.row] as NSManagedObject)
        assessments.removeAtIndex(_selectedIndexPath!.row)
        tvAssessments.deleteRowsAtIndexPaths(
            [_selectedIndexPath!],
            withRowAnimation: UITableViewRowAnimation.Fade
        )
        
        _confView.removeFromSuperView()
    }
    
    func didPressCancel() {
        tvAssessments.editing = false
        _confView.removeFromSuperView()
    }
    
    // MARK: AssessmentEditorDelegate
    func didSaveAssessment(assessment: Assessment) {
        
        println("Assessment title: \(assessment.title)")
        
        _infoView = DPInfoView(
            messages: ["Assessment saved"],
            type: InfoViewType.Info
        )
        
        _infoView.showInView(self.view)
    }
    
    // MARK: AssessmentViewControllerDelegate
    func didCompleteAssessment() {
        
        // show assessment complete confirmation message
        _infoView = DPInfoView(
            messages: ["Assessment complete, thank you."],
            type: InfoViewType.Info
        )
        
        _infoView.showInView(self.view)
    }
    
    // MARK: DPTextFieldViewDelegate
    func didPressOkOnTextFieldView(textFieldValue:String) {
        validatePassword(textFieldValue)
        _textFieldView.removeFromSuperView()
    }
    
    func didPressCancelOnTextFieldView() {
        _textFieldView.removeFromSuperView()
    }

}
