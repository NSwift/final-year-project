//
//  ResultRepository.swift
//  DynamicAssessmentFramework
//
//  Created by Daniel Parkin on 08/01/2015.
//  Copyright (c) 2015 DanielParkin. All rights reserved.
//

class ResultRepository: CoreDataRepository {
    
    override init() {
        super.init()
    }
    
    func getResultCountForAnswer(answer:Answer, ascending:Bool) -> Int {
        
        let assessment = answer.parentQuestion.parentAssessment
        let question = answer.parentQuestion
        
        let request = NSFetchRequest(entityName: "Result")
        let predicate = NSPredicate(format: "answer == %@", answer)
        request.returnsObjectsAsFaults = false
        request.predicate = predicate
        
        var results:NSArray = self.context.executeFetchRequest(request, error: nil)!
        
        return results.count
    }
    
    func getResultCountForAssessment(assessment:Assessment) -> Int {

        let request = NSFetchRequest(entityName: "Result")
        let predicate = NSPredicate(format: "assessment == %@", assessment)
        request.returnsObjectsAsFaults = false
        request.predicate = predicate
        
        var results:NSArray = self.context.executeFetchRequest(request, error: nil)!
        
        return results.count
    }
    
    func getResultsBy(#participant:Participant, assessment:Assessment) -> [Result] {
        let request = NSFetchRequest(entityName: "Result")
        let predicate = NSPredicate(format: "assessment == %@ AND participant == %@", assessment, participant)
        let sortDescriptor = NSSortDescriptor(key: "question.position", ascending: true)
        request.returnsObjectsAsFaults = false
        request.predicate = predicate
        request.sortDescriptors = [sortDescriptor]
        
        var results:NSArray = self.context.executeFetchRequest(request, error: nil)!

        return results as [Result]
    }
    
    func getResultCountFor(#participant:Participant, assessment:Assessment) -> Int {
        return getResultsBy(participant: participant, assessment: assessment).count
    }
    
    func getZombieResults() -> [Result] {
        
        let request = NSFetchRequest(entityName: "Result")
        let predicate = NSPredicate(format: "assessment == nil")
        request.returnsObjectsAsFaults = false
        request.predicate = predicate
        
        var results:NSArray = self.context.executeFetchRequest(request, error: nil)!
        
        return results as [Result]
    }
}
