//
//  DPConfirmationViewDelegate.swift
//  DynamicAssessmentFramework
//
//  Created by Daniel Parkin on 27/02/2015.
//  Copyright (c) 2015 DanielParkin. All rights reserved.
//

import UIKit

protocol DPConfirmationViewDelegate : class {
    func didPressOk()
    func didPressCancel()
}
