//
//  QuestionRecordingDelegate.swift
//  DynamicAssessmentFramework
//
//  Created by Daniel Parkin on 03/03/2015.
//  Copyright (c) 2015 DanielParkin. All rights reserved.
//

protocol QuestionRecordingDelegate : class {
    func beginRecordingQuestion(question:Question)
    func stopRecordingQuestion(question:Question)
}
