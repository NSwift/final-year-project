//
//  QuestionsTests-Bridging-Header.h
//  QuestionsTests
//
//  Created by Daniel Parkin on 19/08/2014.
//  Copyright (c) 2014 Echoyo. All rights reserved.
//

#import "CameraOperations.h"
#import "Answer.h"
#import "Question.h"
#import "Assessment.h"
#import "Result.h"
#import "FocusPoint.h"
#import "Participant.h"
#import "AnswerPresenterOption.h"
#import "CustomAnswerPresenter.h"