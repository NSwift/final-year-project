//
//  Assessment+CDFixes.h
//  DynamicAssessmentFramework
//
//  Created by Daniel Parkin on 17/10/2014.
//  Copyright (c) 2014 DanielParkin. All rights reserved.
//

#import "Participant.h"

@interface Participant (CDFixes)

- (void)addResultsObject:(Participant *)value;

@end
