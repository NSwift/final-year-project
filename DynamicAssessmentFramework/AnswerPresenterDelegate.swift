//
//  AnswerPresenterDelegate.swift
//  DynamicAssessmentFramework
//
//  Created by Daniel Parkin on 14/02/2015.
//  Copyright (c) 2015 DanielParkin. All rights reserved.
//

protocol AnswerPresenterDelegate: class {
    func didProvideAnswer(answer:Answer)
    func didCompleteWithoutAnswer()
}
