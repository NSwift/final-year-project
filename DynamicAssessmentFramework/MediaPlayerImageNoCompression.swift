//
//  MediaPlayerImageNoCompression.swift
//  DynamicAssessmentFramework
//
//  Created by Daniel Parkin on 01/10/2014.
//  Copyright (c) 2014 DanielParkin. All rights reserved.
//

import UIKit
import AssetsLibrary

class MediaPlayerImageNoCompression: NSObject, MediaPlayerProtocol {
    
    weak var mediaDelegate:MediaPlayerDelegate?
    
    private var _appState:ApplicationState!
    
    init(appState:ApplicationState, delegate:MediaPlayerDelegate)
    {
        self.mediaDelegate = delegate
        self._appState = appState
    }
    
    func playMedia(url: NSURL, onView view: UIView) {
        // Display image
        var assetLibrary: ALAssetsLibrary = ALAssetsLibrary()
        
        assetLibrary.assetForURL(url, resultBlock: {
            (asset: ALAsset!) in
            autoreleasepool {
                if asset != nil {
                    
                    var assetRep: ALAssetRepresentation = asset.defaultRepresentation()
                    var iref = assetRep.fullResolutionImage().takeUnretainedValue()
                    var originalImage =  UIImage(CGImage: iref)
                    
                    if view is UIImageView {
                        let imageView = view as UIImageView
                        
                        // Remove all subviews
                        view.subviews.map { $0.removeFromSuperview() }
                        
                        imageView.image = originalImage
                    }
                    else {
                        let viewFrame:CGRect = view.frame
                        
                        let imageView = UIImageView()
                        
                        imageView.frame = CGRectMake(0, 0, viewFrame.size.width, viewFrame.size.height)
                        
                        imageView.image = originalImage
                        
                        // Remove all subviews
                        view.subviews.map { $0.removeFromSuperview() }
                        
                        view.addSubview(imageView)
                        view.bringSubviewToFront(imageView)
                        
                        // let the delegate know the image finished displaying
                        self.imageFinishedDisplaying()
                    }
                    
                }
            }
            }, failureBlock: {
                (error: NSError!) in
                
                NSLog("Error!")
            }
        )
    }
    
    func imageFinishedDisplaying() {
        self.mediaDelegate!.didFinishPlayingMedia()
    }
}
