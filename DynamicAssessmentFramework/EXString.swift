//
//  EXString.swift
//  DynamicAssessmentFramework
//
//  Created by Daniel Parkin on 09/01/2015.
//  Copyright (c) 2015 DanielParkin. All rights reserved.
//

extension String {
    
    func contains(find: String) -> Bool{
        let lowerSelf = self.lowercaseString
        if let temp = lowerSelf.rangeOfString(find.lowercaseString){
            return true
        }
        return false
    }
}