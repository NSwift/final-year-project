//
//  AnswerPresenter.swift
//  QuestionsTests
//
//  Created by Daniel Parkin on 19/08/2014.
//  Copyright (c) 2014 Echoyo. All rights reserved.
//

import UIKit
import AssetsLibrary

class AnswerPresenterCustom:NSObject, AnswerPresentationProtocol {
    
    weak var question:Question?
    weak var selectedAnswer:Answer?
    weak var delegate:AnswerPresenterDelegate?
    var controls:[NSObject]
    var screenBounds:ScreenBounds
    var shouldAnimate:Bool = false

    private var _appState:ApplicationState!
    private var _presenter:CustomAnswerPresenter!
    private var _answerView:UIView!
    private var _imagesLoaded:Int = 0
    private var _optionCount:Int = 0
    
    private weak var _parentView:UIView!
    
    private let _fontHelper = FontHelper()
    private let _answerOverlay = AssessmentAnswerOverlay()
    private let _CDA = CoreDataAccess()
    private let _screenBounds = ScreenBounds()
    
    init(question:Question, appState:ApplicationState, delegate:AnswerPresenterDelegate) {
        self.question = question
        self.controls = [NSObject]()
        self.delegate = delegate
        self.screenBounds = ScreenBounds()
        self._appState = appState
        self.shouldAnimate = _appState == ApplicationState.Review ? false : true
        self._presenter = question.customAnswerPresenter
        self._optionCount = question.customAnswerPresenter.options.count
        
        // initialise answer view
        self._answerView = UIView(frame: self.screenBounds.bottomHalf())
        self._answerView.backgroundColor = UIColor.grayColor()
    }
    
    func displayOnView(view: UIView) {
        
        let horizontalPadding:CGFloat = 40
        
        _parentView = view
        
        // label setup
        var label:UILabel = UILabel(
            frame: CGRect(
                x: 0,
                y: 20,
                width: screenBounds.bounds().width - horizontalPadding,
                height: 100
            )
        )
        
        _screenBounds.positionViewHorizontalCentral(
            thisView: label,
            inCenterOf: _parentView
        )
        
        label.text = question?.questionText
        label.font = _fontHelper.fontPrimary(40)
        label.textColor = UIColor.whiteColor()
        label.numberOfLines = 0
        label.textAlignment = NSTextAlignment.Center
        label.adjustsFontSizeToFitWidth = true
        label.minimumScaleFactor = 0
        
        _answerView.addSubview(label)
        
        let options = _CDA.answerPresenterOptionRepository().getOptionsByAnswerPresenter(_presenter)
        
        
        for (index, option) in enumerate(options) {

            let answer:Answer = question!.answers[index] as Answer

            // setup button
            let assetURL = NSURL(string: option.assetURL)!
            
            let answerOption = option as AnswerPresenterOption
            
            var button:DPUIButtonAnswer = DPUIButtonAnswer(
                frame: CGRect(
                    x: CGFloat(answerOption.x),
                    y: CGFloat(answerOption.y),
                    width: CGFloat(answerOption.width),
                    height: CGFloat(answerOption.height)
                ),
                rounded:false,
                animates: self.shouldAnimate
            )
            
            button.backgroundColor = UIColor.clearColor()
            button.tag = index

            button.addTarget(
                self,
                action: "valueFromControl:",
                forControlEvents: UIControlEvents.TouchUpInside
            )
            
            button.clipsToBounds = true
            
            button.exclusiveTouch = true
            
            self.controls.append(button)
            
            button.answer = self.question!.answers[index] as? Answer
            
            self._answerView.addSubview(button)
            
            addImageToButton(assetURL, button: button)
        
            _answerView.addSubview(button)
         
        }
        
        if _appState == ApplicationState.Review {
            _answerOverlay.overlayAnswerOnView(
                self._answerView,
                controls: self.controls
            )
        }
        
        view.addSubview(_answerView)
        
    }
    
    private func addImageToButton(url:NSURL, button:UIButton) {
        
        var assetLibrary: ALAssetsLibrary = ALAssetsLibrary()
        
        assetLibrary.assetForURL(
            url,
            resultBlock: {(asset: ALAsset!) in
                
                if asset != nil {
                    
                    var assetRep: ALAssetRepresentation = asset.defaultRepresentation()
                    var iref = assetRep.fullResolutionImage().takeUnretainedValue()
                    var importedImage =  UIImage(CGImage: iref)

                    // resize image to correct size
                    var newSize = CGSizeMake(
                        button.frame.width,
                        button.frame.height
                    )
                    
                    var resizeRect = CGRectMake(
                        0,
                        0,
                        button.frame.width,
                        button.frame.height
                    )
                    
                    UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
                    importedImage!.drawInRect(resizeRect)
                    let resizedImage = UIGraphicsGetImageFromCurrentImageContext()
                    UIGraphicsEndImageContext()
                    
                    button.setImage(
                        resizedImage,
                        forState: UIControlState.Normal
                    )

                }
                
            }, failureBlock: {(error: NSError!) in
                println("Image load error.")
            }
        )
    }
    
    func valueFromControl(sender:AnyObject) {
        
        if sender is UIButton {
            var buttonTitle = sender.currentTitle
            
            let selectedAnswer = question?.answers.array[sender.tag] as Answer
            
            delegate?.didProvideAnswer(selectedAnswer)
        }

    }
    
}