//
//  MediaPlayerSettingsFactory.swift
//  DynamicAssessmentFramework
//
//  Created by Daniel Parkin on 02/03/2015.
//  Copyright (c) 2015 DanielParkin. All rights reserved.
//

class MediaPlayerSettingsFactory {
    
    func getSettings(answerType:String) -> MediaPlayerSettings {
        switch(answerType) {
        case "Wait for media":
            return MediaPlayerSettings(callback: true)
        default:
            return MediaPlayerSettings(callback: false)
        }
    }
    
}
