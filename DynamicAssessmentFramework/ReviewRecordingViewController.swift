//
//  ReviewRecordingViewController.swift
//  DynamicAssessmentFramework
//
//  Created by Daniel Parkin on 15/01/2015.
//  Copyright (c) 2015 DanielParkin. All rights reserved.
//

import UIKit

class ReviewRecordingViewController: UIViewController, MediaPlayerDelegate {

    var result:Result!
    var mediaPlayer:MediaPlayerVideo!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let question = result.question
        
        mediaPlayer = MediaPlayerVideo(
            appState:ApplicationState.Assessment,
            delegate: self
        )

        if let savedUrl = result.recordingUrl {
            let assetUrl:NSURL = NSURL(string: savedUrl)!
            mediaPlayer.playMedia(assetUrl, onView: view)
        }
    
    }
    
    // MARK: MediaPlayerFinishedDelegate
    func didFinishPlayingMedia() {
        println("media finished")
    }
    
}
