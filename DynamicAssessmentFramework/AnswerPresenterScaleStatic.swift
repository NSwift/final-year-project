//
//  AnswerPresenterScale.swift
//  DynamicAssessmentFramework
//
//  Created by Daniel Parkin on 21/08/2014.
//  Copyright (c) 2014 Echoyo. All rights reserved.
//

import Foundation
import UIKit

class AnswerPresenterScaleStatic:NSObject, AnswerPresentationProtocol {
    
    weak var selectedAnswer:Answer?
    weak var question:Question?
    weak var delegate:AnswerPresenterDelegate?
    var controls:[NSObject]
    var screenBounds:ScreenBounds
    
    private var _appState:ApplicationState!
    
    private let _fontHelper = FontHelper()
    private let _screenBounds = ScreenBounds()
    
    let CDA:CoreDataAccess = CoreDataAccess()
    let answerOverlay = AssessmentAnswerOverlay()
    
    
    init(question:Question, appState:ApplicationState, delegate:AnswerPresenterDelegate) {
        self.question = question
        self._appState = appState
        self.controls = [NSObject]()
        self.delegate = delegate
        self.screenBounds = ScreenBounds()
    }
    
    func displayOnView(view: UIView) {
        
        // get images for use with scale
        var scaleImages:[UIImage] = getScaleImages()
        
        // initial view setup
        var answerView = UIView(frame: screenBounds.bottomHalf())
        answerView.backgroundColor = UIColor.grayColor()
        
        let horizontalPadding:CGFloat = 40
        
        // label setup
        var label:UILabel = UILabel(
            frame: CGRect(
                x: 0,
                y: 20,
                width: screenBounds.bounds().width - horizontalPadding,
                height: 50
            )
        )
        
        _screenBounds.positionViewHorizontalCentral(
            thisView: label,
            inCenterOf: answerView
        )
        
        label.text = question?.questionText
        label.font = _fontHelper.fontPrimary(40)
        label.textColor = UIColor.whiteColor()
        label.numberOfLines = 0
        label.textAlignment = NSTextAlignment.Center
        label.adjustsFontSizeToFitWidth = true
        label.minimumScaleFactor = 0
        
        // button display parameters
        let totalAnswers:Int = question?.scaleSize as Int
        let spacing = 10
        let buttonWidth = 70
        let buttonHeight = 160
        var xPos = 10;
        
        // get scale size
        let scaleSize:Int = question?.scaleSize as Int
        
        // create containing view for buttons so they can be centred
        let buttonViewWidth = (totalAnswers * (buttonWidth + spacing))
        let bvCentrePos = (Int(screenBounds.bounds().width) / 2) - (buttonViewWidth / 2)
        
        var buttonView = UIView(frame: CGRect(x: bvCentrePos, y: 100, width: buttonViewWidth, height: buttonHeight))
        
        let answers = question!.answers
        
        // add buttons to button view
        for var i = 0; i < scaleSize ; i++ {
            
            // button
            var button:DPUIButtonAnswer = DPUIButtonAnswer(frame: CGRect(x: xPos, y: 50, width: buttonWidth, height: buttonHeight))
            
            let answer:Answer = answers[i] as Answer
            
            button.tag = i
            button.answer = answer
            button.backgroundColor = UIColor.whiteColor()
            button.setImage(scaleImages[i], forState: UIControlState.Normal)
            
            // give button tag with array index of answer
            // increment by one to give correct scale value
            button.tag = i + 1
            
            button.addTarget(self, action: "valueFromControl:", forControlEvents: UIControlEvents.TouchUpInside)
            
            button.exclusiveTouch = true
            
            // add controls to array to retain reference
            controls.append(button)
            
            buttonView.addSubview(button)
            
            // increment yPos
            xPos += (buttonWidth + spacing)
        }
        
        answerView.addSubview(label)
        answerView.addSubview(buttonView)
        
        // overlay answers if neccessary
        if _appState == ApplicationState.Review {
            self.answerOverlay.overlayAnswerOnView(
                answerView, controls:
                self.controls
            )
        }
        
        view.addSubview(answerView)
    }
    
    func valueFromControl(sender:AnyObject) {
        if sender is DPUIButtonAnswer {
            
            let selectedAnswer = sender.answer!
            
            delegate?.didProvideAnswer(selectedAnswer)
        }
    }
    
    func getScaleImages() -> [UIImage] {
        
        var scaleImages:[UIImage]
        
        // creat images views
        let image1 = UIImage(named: "SAM01.png")
        let image2 = UIImage(named: "SAM02.png")
        let image3 = UIImage(named: "SAM03.png")
        let image4 = UIImage(named: "SAM04.png")
        let image5 = UIImage(named: "SAM05.png")
        let image6 = UIImage(named: "SAM06.png")
        let image7 = UIImage(named: "SAM07.png")
        let image8 = UIImage(named: "SAM08.png")
        let image9 = UIImage(named: "SAM09.png")
        
        var scaleSize:Int = question?.scaleSize as Int
        
        // add images to image array
        switch(scaleSize) {
        case 3:
            scaleImages = [image1!, image5!, image9!]
        case 4:
            scaleImages = [image1!, image3!, image7!, image9!]
        case 5:
            scaleImages = [image1!, image3!, image5!, image7!, image9!]
        case 7:
            scaleImages = [image1!, image3!, image4!, image5!, image6!, image7!, image9!]
        case 9:
          scaleImages = [image1!, image2!, image3!, image4!, image5!, image6!, image7!, image8!, image9!]
        default:
          scaleImages = [image1!, image2!, image3!, image4!, image5!, image6!, image7!, image8!, image9!]
        }
        
        return scaleImages
    }
}