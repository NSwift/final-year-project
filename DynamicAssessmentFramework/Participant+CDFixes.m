//
//  Assessment+CDFixes.m
//  DynamicAssessmentFramework
//
//  Created by Daniel Parkin on 17/10/2014.
//  Copyright (c) 2014 DanielParkin. All rights reserved.
//

#import "Participant+CDFixes.h"

@implementation Participant (CDFixes)

- (void)addResultsObject:(Participant *)value {
    NSMutableOrderedSet* tempSet = [NSMutableOrderedSet orderedSetWithOrderedSet:self.results];
    [tempSet addObject:value];
    self.results = tempSet;
}

@end
