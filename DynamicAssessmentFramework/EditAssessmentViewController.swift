//
//  EditAssessmentViewController.swift
//  DynamicAssessmentFramework
//
//  Created by Daniel Parkin on 25/08/2014.
//  Copyright (c) 2014 Echoyo. All rights reserved.
//

import UIKit

class EditAssessmentViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, EditFocusPointDelegate, QuestionEditorDelegate {

    // MARK: Properties
    var delegate:AssessmentEditorDelegate!
    var selectedIndex:Int = 0
    var assessment:Assessment!
    var questions:[Question] = []
    var actionSheet:UIActionSheet = UIActionSheet()
    var buttonsDrawn:Bool = false
    
    private var _infoView:DPInfoView!
    
    let CDA = CoreDataAccess()
    let colorHelper = ColorHelper()
    let fontHelper = FontHelper()
    
    private let _recoveredName = "Recovered assessment"
    
    // MARK: IBOutlets
    @IBOutlet var tvQuestions : UITableView!
    @IBOutlet weak var tfAssessmentTitle: UITextField!
    @IBOutlet weak var swUseCustomFP: UISwitch!
    @IBOutlet weak var swHasStartScreen: UISwitch!
    @IBOutlet weak var btConfigureFP: UIButton!
    
    // MARK: Init
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    // MARK: IBAction methods
    @IBAction func doneTapped(sender : AnyObject) {
        
        let validationResult = checkAssessmentIsValidForSaving(assessment)
        
        if validationResult.isValid {
         
            assessment.title = tfAssessmentTitle.text
            
            // assign the correct position in the assessment to each question
            self.mapQuestionPositions()
            
            delegate.didSaveAssessment(assessment)
            
            self.navigationController?.popToRootViewControllerAnimated(true)
            
        } else {
            
            _infoView = DPInfoView(
                messages: validationResult.errorMessages,
                type: InfoViewType.Error
            )
            
            _infoView.showInView(self.view)
            
        }
        
    }
    
    @IBAction func editFocusPointTapped(sender: AnyObject) {
        
        var vcEditFocusPoint = self.storyboard?.instantiateViewControllerWithIdentifier("EditFocusPoint") as EditFocusPointViewController
        
        vcEditFocusPoint.delegate = self
        
        if let existingFocusPoint = assessment.focusPoint {
            vcEditFocusPoint.focusPoint = existingFocusPoint
        }
        
        self.navigationController?.pushViewController(vcEditFocusPoint, animated: true)
    }
    
    @IBAction func clearTapped(sender: AnyObject) {
        self.tfAssessmentTitle.text = ""
    }
    
    
    // MARK: View methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tvQuestions.delegate = self
        tvQuestions.dataSource = self
        
        tfAssessmentTitle.text = assessment.title
        
        // toggle custom focus point switch
        if let useCustomFP = assessment.usesCustomFocusPoint {
            swUseCustomFP.on = useCustomFP.boolValue
        }
        else {
            swUseCustomFP.on = false
        }
        
        // add tap event to focus point switch
        swUseCustomFP.addTarget(
            self,
            action: "focusPointSwitchToggle:",
            forControlEvents: UIControlEvents.TouchUpInside
        )
        
        // add tap event to start screen switch
        swHasStartScreen.addTarget(
            self,
            action: "hasStartScreenToggle:",
            forControlEvents: UIControlEvents.TouchUpInside
        )
        
        // toggle start screen switch
        swHasStartScreen.on = assessment.hasStartScreen.boolValue

        // show or hide the config focus point button
        self.toggleConfigFocusPoint()
        
    }
    
    override func viewDidAppear(animated: Bool) {
        self.populateQuestions()
        tvQuestions.reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()

    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject!) {

        if segue.identifier == "AddQuestion" {
            
            // hide onscreen keyboard if it's on visible
            self.view.endEditing(true)
            
            // map question positions incase they have been reordered
            self.mapQuestionPositions()
            
            let vcEditQuestion = segue.destinationViewController as EditQuestionViewController
            
            vcEditQuestion.delegate = self
            
        }

    }
    
    // MARK: UITableViewDataSourceDelegate methods
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return questions.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        // get cell
        let cellID = "CellQuestion"
        var cell = tableView.dequeueReusableCellWithIdentifier(cellID) as UITableViewCell
        
        // set cells background color
        if indexPath.row % 2 == 0 {
            cell.contentView.backgroundColor = colorHelper.colorPrimary05()
        }
        
        // get references to subviews
        let lvTitle = cell.viewWithTag(1) as UILabel
        let lvDetail = cell.viewWithTag(2) as UILabel
        
        // button height must be hard coded
        // using cell.contentView.frame.size.height causes button to magically
        // grow when added to the cell at runtime
        let buttonHeight:CGFloat = 60
        let buttonWidth:CGFloat = 60
        let buttonFontSize:CGFloat = 20
        
        // add move down button
        let btMoveDown = DPUIButtonTableCell(frame: CGRect(x: 0, y: 0, width: buttonWidth, height: buttonHeight))
        btMoveDown.titleLabel!.font! = self.fontHelper.fontGlyphicons(buttonFontSize)
        btMoveDown.setTitle("d", forState: UIControlState.Normal)
        btMoveDown.tableCellId = indexPath.row
        btMoveDown.addTarget(self, action: "moveQuestionDown:", forControlEvents: UIControlEvents.TouchUpInside)
        cell.contentView.addSubview(btMoveDown)
        
        // add move up button
        let btMoveUp = DPUIButtonTableCell(frame: CGRect(x: buttonWidth, y: 0, width: buttonWidth, height: buttonHeight))
        btMoveUp.titleLabel!.font! = self.fontHelper.fontGlyphicons(buttonFontSize)
        btMoveUp.setDefaultColor(self.colorHelper.colorPrimary3())
        btMoveUp.setTitle("u", forState: UIControlState.Normal)
        btMoveUp.tableCellId = indexPath.row
        btMoveUp.addTarget(self, action: "moveQuestionUp:", forControlEvents: UIControlEvents.TouchUpInside)
        cell.contentView.addSubview(btMoveUp)

        var question = questions[indexPath.row] as Question
        lvTitle.text = question.questionText
        lvDetail.text = question.questionType
        
        return cell
    }
    
    func tableView(tableView: UITableView!, didSelectRowAtIndexPath indexPath: NSIndexPath!) {
        
         var editView = self.storyboard?.instantiateViewControllerWithIdentifier("EditQuestion") as EditQuestionViewController
        
        if let path = indexPath {
            editView.question = questions[path.row] as Question
        }
        
        // assign the correct position in the assessment to each question
        self.mapQuestionPositions()
        
        editView.delegate = self
        
        self.navigationController?.pushViewController(editView, animated: true)
    }
    
    func tableView(tableView: UITableView!, canEditRowAtIndexPath indexPath: NSIndexPath!) -> Bool {
        return true
    }
    
    func tableView(tableView: UITableView!, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath!) {
        
        if editingStyle == UITableViewCellEditingStyle.Delete {
            
            if let table = tableView {
                CDA.deleteObject(questions[indexPath!.row] as NSManagedObject)
                questions.removeAtIndex(indexPath!.row)
                table.deleteRowsAtIndexPaths([indexPath!], withRowAnimation: UITableViewRowAnimation.Fade)
            }
            
        }
        
    }
    
    // MARK: UITableViewCell button methods
    func moveQuestionDown(sender:AnyObject)  {
        let btTableCell = sender as DPUIButtonTableCell
        let questionBeingMovedIndex = btTableCell.tableCellId
        let questionBelowOriginalIndex = questionBeingMovedIndex + 1
        let endOfList = self.questions.count - 1
        
        if questionBeingMovedIndex < endOfList {
            let questionBeingMoved = self.questions[questionBeingMovedIndex]
            
            self.questions.insert(questionBeingMoved, atIndex: questionBelowOriginalIndex + 1)
            self.questions.removeAtIndex(questionBeingMovedIndex)
            self.tvQuestions.reloadData()
        }
    }
    
    func moveQuestionUp(sender:AnyObject)  {
        let btTableCell = sender as DPUIButtonTableCell
        let questionBeingMovedIndex = btTableCell.tableCellId
        let questionAboveIndex = questionBeingMovedIndex - 1
        let beginningOfList = 0
        let endOfList = self.questions.count - 1
        
        if questionBeingMovedIndex > beginningOfList {
            
            let questionBeingMoved = self.questions[questionBeingMovedIndex]
            
            self.questions.removeAtIndex(questionBeingMovedIndex)
            self.questions.insert(questionBeingMoved, atIndex: questionAboveIndex)
            

            self.tvQuestions.reloadData()
        }
    }
    
    // MARK: CoreData methods
    func populateQuestions() {
        
        if let unwrapped = assessment {
            
            // if questions exist call to question repository for sorted list
            if let existingQuestions = unwrapped.questions.array as? [Question] {

                self.questions = self.CDA.questionRepository().getQuestionsByAssessment(
                    self.assessment,
                    ascending: true
                )
            }
            
        }
    }
    
    // MARK: Question position mapping
    func mapQuestionPositions() {
        for var i = 0; i < questions.count; i++ {
            questions[i].position = i
        }
        
        self.CDA.save()
    }
    
    // MARK: Focus point switch toggle
    func hasStartScreenToggle(sender:AnyObject) {
        if let startScreenSwitch = sender as? UISwitch {
            assessment.hasStartScreen = startScreenSwitch.on
        }
    }
    
    // MARK: Focus point switch toggle
    func focusPointSwitchToggle(sender:AnyObject) {
        if let focusPointSwitch = sender as? UISwitch {
            
            // set uses focus point to false or true
            // delete focus point if one exists
            if focusPointSwitch.on {
                assessment.usesCustomFocusPoint = true
            }
            else {
                assessment.usesCustomFocusPoint = false
                
                // if focus point exists, delete it
                if let focusPoint = assessment.focusPoint {
                    CDA.deleteObject(assessment.focusPoint)
                }
            }
            
            self.toggleConfigFocusPoint()
            
        }
    }
    
    // toggle visibility of the config focus point button
    func toggleConfigFocusPoint() {
        if(assessment.usesCustomFocusPoint.boolValue) {
            btConfigureFP.hidden = false
        }
        else {
            btConfigureFP.hidden = true
        }
    }
    
    // MARK: EditFocusPointDelegate
    func didSaveFocusPoint(focusPoint:FocusPoint) {
        
        self.assessment.focusPoint = focusPoint
    
        CDA.save()
        
        // show confirmation message
        _infoView = DPInfoView(
            messages: ["Focus point saved."],
            type: InfoViewType.Info
        )
        
        _infoView.showInView(self.view)
    }
    
    func didDiscardChanges() {
        assessment.usesCustomFocusPoint = false
        btConfigureFP.hidden = true
        swUseCustomFP.on = false
    }
    
    deinit {
        println("Edit assessment dealloc")
    }
    
    // MARK: Validate assessment
    func checkAssessmentIsValidForSaving(assessment:Assessment) -> ValidationResult {
        
        var errors:[String] = [];
        var isValid = true
    
        let assessmentName = tfAssessmentTitle.text
        let titleLimit = 50
        let titleLength = countElements(assessmentName)
        
        let nameAlreadyExists = CDA.assessmentRepository().assessmentNameExists(assessmentName)
        
        // check a focus point has been setup if switch is on
        if swUseCustomFP.on && assessment.focusPoint == nil {
            errors.append("Please configure a focus point.")
            isValid = false
        }
        
        // check if assessment name already exists
        if assessmentName != assessment.title {
            if nameAlreadyExists {
                errors.append("An assessment already exists with this name.")
                isValid = false
            }
        }

        // check the assessment has a title
        if titleLength == 0 {
            errors.append("The assessment must be given a title.")
            isValid = false
        }
        
        // check the assessment title is not too long
        if titleLength > 50 {
            errors.append("Assessment names can be no longer than \(titleLimit) characters.")
            errors.append("Current name is \(titleLength) characters, remove \(titleLength - titleLimit) characters.")
            isValid = false
        }
        
        return ValidationResult(isValid: isValid, errorMessages: errors)
    }
    
    // MARK: QuestionEditorDelegate
    func didSaveQuestion(question: Question) {
        
        let nextPosition = self.questions.count + 1
        question.position = nextPosition
        
        if let existingAssessment = assessment {
            existingAssessment.addQuestionsObject(question)
        }
        
        CDA.save()
        
        self.populateQuestions()
        tvQuestions.reloadData()
        
        // show confirmation message
        _infoView = DPInfoView(
            messages: ["Question saved."],
            type: InfoViewType.Info
        )
        
        _infoView.showInView(self.view)
        
    }
    
    func didCancelWithoutSaving() {
        
    }
}
