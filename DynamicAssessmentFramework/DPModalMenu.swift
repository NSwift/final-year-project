//
//  DPModalMenu.swift
//  DynamicAssessmentFramework
//
//  Created by Daniel Parkin on 11/01/2015.
//  Copyright (c) 2015 DanielParkin. All rights reserved.
//

import UIKit

class DPModalMenu:NSObject {
    
    var view:UIView!
    var menuItems:[DPModalMenuItem] = []
    weak var delegate:DPModalMenuDelegate?
    
    let itemHeight:CGFloat = 80
    let itemWidth:CGFloat = 300
    let alpha:CGFloat = 0.5
    let radius:CGFloat = 20
    
    let screenBounds = ScreenBounds()
    
    init(menuItems:[DPModalMenuItem], delegate:DPModalMenuDelegate) {
        super.init()
        self.menuItems = menuItems
        self.delegate = delegate
        
        // initialise menu items
        for var i = 0; i < self.menuItems.count; i++ {
            self.menuItems[i].delegate = self.delegate
            self.menuItems[i].itemIndex = i
            self.menuItems[i].height = itemHeight
            self.menuItems[i].width = itemWidth
            self.menuItems[i].parent = self
        }
        
        self.view = self.buildView()
    }
    
    func buildView() -> UIView {
        
        let itemCount = CGFloat(menuItems.count)
        let menuViewHeight = itemHeight * itemCount
        let menuViewWidth = itemWidth
        
        var menuView = UIView(
            frame: CGRect(
                x: 0,
                y: 0,
                width: menuViewWidth,
                height: menuViewHeight)
        )
        
        menuView.backgroundColor = UIColor(white: 1, alpha: 0.5)
        
        for var i = 0; i < self.menuItems.count; i++ {
            let itemView = self.menuItems[i].view()
            let itemViewY = CGFloat(i) * itemHeight
            itemView.frame.origin.y = itemViewY
            menuView.insertSubview(itemView, atIndex: 9)
        }
        
        // round the corners of the view
        menuView.layer.cornerRadius = radius
        menuView.layer.masksToBounds = true
        
        return menuView
    }
    
    func showInView(view:UIView) {
        let parentView = view
        
        screenBounds.positionViewCentral(
            thisView: self.view,
            inCenterOf: parentView
        )

        parentView.insertSubview(self.view, atIndex: 10)
    }
    
    func removeFromSuperView() {
        self.view.removeFromSuperview()
    }
    
}