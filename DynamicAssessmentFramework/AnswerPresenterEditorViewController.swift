//
//  AnswerPresenterEditorViewController.swift
//  DynamicAssessmentFramework
//
//  Created by Daniel Parkin on 17/02/2015.
//  Copyright (c) 2015 DanielParkin. All rights reserved.
//

import UIKit
import AssetsLibrary

class AnswerPresenterEditorViewController : UIViewController, MediaImporterDelegate,UIGestureRecognizerDelegate {
    
    var delegate:CustomAnswerPresenterDelegate!
    var existingPresenter:CustomAnswerPresenter!
    
    private var _images:[UIImageView] = []
    private var _imageURLMap:[UIImageView:NSURL] = [:]
    private var _gestRecs:[UIGestureRecognizer] = []
    private var _mediaImporter:MediaImporterImage!
    private var _optionView:UIView!
    private var _lastXPos:CGFloat = 0
    private var _height:CGFloat = 0
    private var _width:CGFloat = 0
    private var _infoView:DPInfoView!
    
    private let _screenBounds = ScreenBounds()
    private let _CDA = CoreDataAccess()
    
    @IBOutlet weak var tfHeight: UITextField!
    @IBOutlet weak var tfWidth: UITextField!
    
    override func viewDidLoad() {
        
        _optionView = UIView(frame: _screenBounds.bottomHalf())
        _optionView.backgroundColor = UIColor.grayColor()
        view.addSubview(_optionView)
        
        _mediaImporter = MediaImporterImage()
        _mediaImporter.delegate = self
        
        addQuestionLabel()
        
        // if an existing answer presenter has been passed in
        // reload it
        if let presenter = existingPresenter {
            self.loadPresenter(presenter)
        }
        
        // show prototype
        _infoView = DPInfoView(
            messages: ["This screen is a prototype to demonstate the creation of", "custom answer presenters."],
            type: InfoViewType.Info
        )
        
        _infoView.showInView(self.view)
        
    }
    
    // MARK: IBAction methods
    @IBAction func addTapped(sender: AnyObject) {
        self.presentViewController(
            _mediaImporter.controller(),
            animated: true,
            completion: nil
        )
    }
    @IBAction func saveTapped(sender: AnyObject) {
        savePresenter()
    }
    
    @IBAction func equaliseHeightTapped(sender: AnyObject) {
        
        var maxHeight:CGFloat!
        
        for image in _images {
            if image.frame.size.height > maxHeight {
                maxHeight = image.frame.size.height
            }
        }
        
        for image in _images {
            image.frame.size.height = maxHeight
        }
        
    }

    @IBAction func equaliseToDimensionsTapped(sender: AnyObject) {
    
        for image in _images {
            image.frame.size.height = _height
            image.frame.size.width = _width
        }
        
    }
    
    @IBAction func constrainTapped(sender: AnyObject) {
        
        // tbw = total button width
        // vw = view width
        // bc = button count
        // cw = container width
        // p = padding
        
        // cw = ((vw - tbw) / 2) + tbw
        // p = ((vw - tbw) / 2) / (bc - 1)
        
        // remove all subviews
        _optionView.subviews.map { $0.removeFromSuperview() }
        
        let viewWidth = _screenBounds.width()
        let imageCount = CGFloat(_images.count)
        
        var containerView:UIView!
        var totalImageWidth:CGFloat = 0
        var maxImageHeight:CGFloat = 0
        
        // calculate total button width
        for image in _images {
            
            // find largest image
            if image.frame.height > maxImageHeight {
                maxImageHeight = image.frame.height
            }
            
            totalImageWidth += image.frame.width
        }
        
        var containerWidth = ((viewWidth - totalImageWidth) / 2) + totalImageWidth
        var padding = ((viewWidth - totalImageWidth) / 2) / (imageCount - 1)
        
        containerView = UIView(
            frame: CGRect(
                x: 0,
                y: 0,
                width: containerWidth,
                height: maxImageHeight)
        )
        
        var xOffset:CGFloat = 0
        
        // loop through images and add to container view
        for (index, image) in enumerate(_images) {
            
            image.frame.origin.y = 0
            image.frame.origin.x = xOffset
            
            if index != _images.count {
                xOffset += padding
            }
            
            xOffset += image.frame.width
            
            containerView.addSubview(image)
        }
        

        _screenBounds.positionViewCentral(
            thisView: containerView,
            inCenterOf: _optionView
        )
        
        _optionView.addSubview(containerView)
        addQuestionLabel()
        
    }
    
    func addQuestionLabel() {
        
        var label:UILabel = UILabel(
            frame: CGRect(
                x: 0,
                y: 20,
                width: _screenBounds.width(),
                height: 50)
        )
        
        label.text = "Question text will appear here"
        label.font = UIFont.systemFontOfSize(30)
        label.textColor = UIColor.whiteColor()
        label.numberOfLines = 0
        label.textAlignment = NSTextAlignment.Center
        
        _optionView.addSubview(label)
    }
    
    // MARK: MediaImporterDelegate
    func didImportMedia(url:NSURL) {
        loadImageFromAssetURL(url)
    }
    
    private func loadPresenter(presenter:CustomAnswerPresenter) {
        
        for option in presenter.options {
            loadPresenterOption(option as AnswerPresenterOption)
        }
        
    }
    
    private func loadPresenterOption(option:AnswerPresenterOption) {
        
        let assetURL = NSURL(string: option.assetURL)
        
        var assetLibrary: ALAssetsLibrary = ALAssetsLibrary()
        
        assetLibrary.assetForURL(
            assetURL,
            resultBlock: {(asset: ALAsset!) in
                
                if asset != nil {
                    
                    var assetRep: ALAssetRepresentation = asset.defaultRepresentation()
                    var iref = assetRep.fullResolutionImage().takeUnretainedValue()
                    var importedImage =  UIImage(CGImage: iref)
                    
                    var imageView = UIImageView(image: importedImage)
                    
                    imageView.userInteractionEnabled = true
                    
                    // configure image view frame
                    imageView.frame.origin.x = CGFloat(option.x)
                    imageView.frame.origin.y = CGFloat(option.y)
                    imageView.frame.size.height = CGFloat(option.height)
                    imageView.frame.size.width = CGFloat(option.width)
                    
                    // add pan recognizer to image view
                    let panRec = UIPanGestureRecognizer(
                        target: self,
                        action: "handlePan:"
                    )
                    
                    panRec.delegate = self
                    self._gestRecs.append(panRec)
                    imageView.addGestureRecognizer(panRec)
                    
                    // add pinch recognizer to image view
                    let pinchRec = UIPinchGestureRecognizer(
                        target: self,
                        action: "handlePinch:"
                    )
                    
                    pinchRec.delegate = self
                    self._gestRecs.append(pinchRec)
                    imageView.addGestureRecognizer(pinchRec)
                    
                    // add tap recognizer
                    let tapRec = UITapGestureRecognizer(
                        target: self,
                        action: "handleTap:"
                    )
                    
                    tapRec.delegate = self
                    self._gestRecs.append(tapRec)
                    imageView.addGestureRecognizer(tapRec)
                    
                    self._images.append(imageView)
                    
                    self._optionView.addSubview(imageView)
                    
                    // map url against image view
                    // to record which asset was used
                    self._imageURLMap[imageView] = assetURL
                    
                }
                
            }, failureBlock: {(error: NSError!) in
                //println("Image load error.")
            }
        )
    }
    
    private func loadImageFromAssetURL(url:NSURL) {

        var assetLibrary: ALAssetsLibrary = ALAssetsLibrary()
        
        assetLibrary.assetForURL(
            url,
            resultBlock: {(asset: ALAsset!) in

                if asset != nil {

                    var assetRep: ALAssetRepresentation = asset.defaultRepresentation()
                    var iref = assetRep.fullResolutionImage().takeUnretainedValue()
                    var importedImage =  UIImage(CGImage: iref)
                    
                    var imageView = UIImageView(image: importedImage)
                    
                    imageView.userInteractionEnabled = true
                    
                    // configure image view frame
                    imageView.frame.origin.x = self._lastXPos
                
                    // add pan recognizer to image view
                    let panRec = UIPanGestureRecognizer(
                        target: self,
                        action: "handlePan:"
                    )
                    
                    panRec.delegate = self
                    self._gestRecs.append(panRec)
                    imageView.addGestureRecognizer(panRec)
                    
                    // add pinch recognizer to image view
                    let pinchRec = UIPinchGestureRecognizer(
                        target: self,
                        action: "handlePinch:"
                    )
                    
                    pinchRec.delegate = self
                    self._gestRecs.append(pinchRec)
                    imageView.addGestureRecognizer(pinchRec)
                    
                    // add tap recognizer
                    let tapRec = UITapGestureRecognizer(
                        target: self,
                        action: "handleTap:"
                    )
                    
                    tapRec.delegate = self
                    self._gestRecs.append(tapRec)
                    imageView.addGestureRecognizer(tapRec)
                    
                    self._images.append(imageView)
                    
                    self._optionView.addSubview(imageView)
                    
                    // set the x offset 
                    self._lastXPos = imageView.frame.origin.x + imageView.frame.width
                    
                    // map url against image view
                    // to record which asset was used
                    self._imageURLMap[imageView] = url
                    
                }
                
            }, failureBlock: {(error: NSError!) in
                println("Image load error.")
            }
        )
    }
    
    private func savePresenter() {
        
        var presenter:CustomAnswerPresenter!
        
        // choose new or existing answer presenter
        if let existing = existingPresenter {
            presenter = existingPresenter
            
            // delete all existing options to be replaced by updated ones
            for option in presenter.options {
                _CDA.deleteObject(option as NSManagedObject)
            }
        }
        else {
            presenter = _CDA.newObjectForEntity(CDEntity.CustomAnswerPresenter) as CustomAnswerPresenter
        }
        
        // add new options to the presenter
        for image in _images {
            
            //let imageBounds = image.convertRect(image.bounds, toView: self.view)
            let imageBounds = image.convertRect(image.bounds, toView: _optionView)
            
            var option = _CDA.newObjectForEntity(CDEntity.AnswerPresenterOption) as AnswerPresenterOption
            
            option.x = imageBounds.origin.x
            option.y = imageBounds.origin.y
            
            option.height = image.frame.height
            option.width = image.frame.width
            option.assetURL = _imageURLMap[image]!.absoluteString
            
            NSLog("%@", option)
            
            presenter.addOptionsObject(option)
            
        }
        
        _CDA.save()
        delegate.didReturnAnswerPresenter(presenter)
        popView()
    }
    
    func popView() {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    func handlePan(recognizer:UIPanGestureRecognizer) {
        
        self.view.bringSubviewToFront(recognizer.view!)
        var translation = recognizer.translationInView(self.view)
        
        var xPoint = recognizer.view!.center.x + translation.x
        var yPoint = recognizer.view!.center.y + translation.y
        
        recognizer.view!.center = CGPointMake(xPoint, yPoint)
        recognizer.setTranslation(CGPointZero, inView: self.view)
        
    }
    
    func handlePinch(recognizer : UIPinchGestureRecognizer) {
        
        recognizer.view!.transform = CGAffineTransformScale(
            recognizer.view!.transform,
            recognizer.scale,
            recognizer.scale
        )
        
        var scale = recognizer.scale
        
        recognizer.scale = 1
    }
    
    func handleTap(recognizer:UITapGestureRecognizer) {
        tfHeight.text = "\(recognizer.view!.frame.height)"
        tfWidth.text = "\(recognizer.view!.frame.width)"
        
        _height = recognizer.view!.frame.height
        _width = recognizer.view!.frame.width
    }


    // MARK: UIGestureRecogizerDelegate methods
    func gestureRecognizer(UIGestureRecognizer,
        shouldRecognizeSimultaneouslyWithGestureRecognizer:UIGestureRecognizer) -> Bool {
            return true
    }

}
