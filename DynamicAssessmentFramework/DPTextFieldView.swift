//
//  DPErrorView.swift
//  DynamicAssessmentFramework
//
//  Created by Daniel Parkin on 23/02/2015.
//  Copyright (c) 2015 DanielParkin. All rights reserved.
//
import UIKit

class DPTextFieldView : NSObject {
    
    private var _view:UIView!
    private var _messages:[String]!
    private var _textField:UITextField!
    private var _baseButton:UIButton!
    private weak var _delegate:DPTextFieldViewDelegate!
    
    private let _viewRadius:CGFloat = 20
    private let _viewWidth:CGFloat = 600
    private let _viewMinHeight:CGFloat = 300
    private let _viewPaddingBottom:CGFloat = 20
    private let _viewPaddingTop:CGFloat = 20
    private let _viewPaddingLeft:CGFloat = 20
    private let _viewPaddingRight:CGFloat = 20
    private let _titleLabelHeight:CGFloat = 50
    private let _textFieldHeight:CGFloat = 30
    private let _messageLabelHeight:CGFloat = 30
    private let _messageLabelFontSize:CGFloat = 20
    private let _okButtonHeight:CGFloat = 50
    
    private let _screenBounds = ScreenBounds()
    private let _colorHelper = ColorHelper()
    private let _fontHelper = FontHelper()
    
    init(delegate:DPTextFieldViewDelegate, messages:[String]) {
        super.init()
        self._delegate = delegate
        self._messages = messages
        self.initView()
    }
    
    private func initView() {
        
        // create base button view
        _view = buildBaseButton()
        
        // build error container
        let errorContainer = buildErrorContainer()
        
        // create labels
        self.addErrorLabelSubViews(errorContainer)
        
        _screenBounds.positionViewCentral(
            thisView: errorContainer,
            inCenterOf: _view
        )
        
        // add the completed view
        _view.addSubview(errorContainer)
    }
    
    private func calculateContainerHeight() -> CGFloat {
        
        let totalLabelHeight = (_titleLabelHeight + _okButtonHeight + _textFieldHeight) + (_messageLabelHeight * CGFloat(_messages.count))
        
        let padding = _viewPaddingTop + _viewPaddingBottom
        var totalHeight = totalLabelHeight + padding
        
        //if totalHeight < _viewMinHeight { totalHeight = _viewMinHeight }
        
        return totalHeight
    }
    
    private func calculateContainerWidth() -> CGFloat {
        var padding = _viewPaddingLeft + _viewPaddingRight
        return padding + _viewWidth
    }
    
    private func totalHorizontalPadding() -> CGFloat {
        return _viewPaddingLeft + _viewPaddingRight
    }
    
    private func buildErrorContainer() -> UIView {
        
        var containerHeight = calculateContainerHeight()
        var containerWidth = calculateContainerWidth()
        
        var errorContainer = UIView(
            frame: CGRect(
                x: 0,
                y: 0,
                width: containerWidth,
                height: containerHeight)
        )
        
        errorContainer.backgroundColor = _colorHelper.colorPrimary4()
        
        errorContainer.layer.cornerRadius = _viewRadius
        errorContainer.layer.masksToBounds = true
        
        return errorContainer
    }
    
    private func addErrorLabelSubViews(parentView:UIView) {
        
        var yOffset:CGFloat = 0
        
        // add title label
        //
        let titleLabel = UILabel(
            frame: CGRect(
                x: 0,
                y: 0,
                width: parentView.frame.width,
                height: _titleLabelHeight)
        )
        
        titleLabel.font = _fontHelper.fontPrimary(_messageLabelFontSize)
        titleLabel.backgroundColor = _colorHelper.colorPrimary3()
        titleLabel.textColor = UIColor.whiteColor()
        titleLabel.textAlignment = NSTextAlignment.Center
        
        // add icon to title label
        //
        let iconLabel = UILabel(
            frame: CGRect(
                x: 20,
                y: 0,
                width: 20,
                height: 20)
        )
        
        _screenBounds.positionViewVerticalCentral(
            thisView: iconLabel,
            inCenterOf: titleLabel
        )
        
        iconLabel.font = _fontHelper.fontGlyphicons(_messageLabelFontSize)
        iconLabel.textColor = UIColor.whiteColor()
        
        // configure title
        titleLabel.text = "Information Required"
        iconLabel.text = "z" // ? icon
        
        // add the icon to the title label, then add title label to parent
        titleLabel.addSubview(iconLabel)
        
        parentView.addSubview(titleLabel)
        
        yOffset += (_titleLabelHeight + _viewPaddingTop)
        
        
        // add messages
        //
        for message in _messages {
            
            var messageLabel = createLabel(message,
                frame: CGRect(
                    x: 0,
                    y: yOffset,
                    width: _viewWidth,
                    height: _messageLabelHeight
                )
            )
            
            yOffset += _messageLabelHeight
            
            _screenBounds.positionViewHorizontalCentral(
                thisView: messageLabel,
                inCenterOf: parentView
            )
            
            parentView.addSubview(messageLabel)
        }
        
        // add text field
        //
        _textField = UITextField(
            frame: CGRect(
                x: 0,
                y: yOffset,
                width: _viewWidth,
                height: _textFieldHeight
            ))
        
        _textField.backgroundColor = UIColor.whiteColor()
        _textField.layer.borderColor = UIColor.blackColor().CGColor
        
        yOffset += _textFieldHeight
        
        _screenBounds.positionViewHorizontalCentral(
            thisView: _textField,
            inCenterOf: parentView
        )
        
        _textField.becomeFirstResponder()
        
        parentView.addSubview(_textField)
        
        
        // calculate button width
        //
        let buttonWidth = (_viewWidth + totalHorizontalPadding()) / 2
        
        // add cancel button
        //
        var cancelButton = UIButton(
            frame: CGRect(
                x: 0,
                y: yOffset + _viewPaddingBottom,
                width: buttonWidth,
                height: _okButtonHeight)
        )
        
        cancelButton.titleLabel!.textAlignment = NSTextAlignment.Center
        cancelButton.backgroundColor = _colorHelper.colorPrimary3()
        cancelButton.addTarget(self,
            action: "didPressCancel:",
            forControlEvents: UIControlEvents.TouchUpInside
        )
        
        cancelButton.setTitle("Cancel", forState: UIControlState.Normal)
        
        // add cancel button
        //
        var okButton = UIButton(
            frame: CGRect(
                x: buttonWidth,
                y: yOffset + _viewPaddingBottom,
                width: buttonWidth,
                height: _okButtonHeight)
        )
        
        okButton.titleLabel!.textAlignment = NSTextAlignment.Center
        okButton.backgroundColor = _colorHelper.colorPrimary2()
        okButton.addTarget(self,
            action: "didPressOk:",
            forControlEvents: UIControlEvents.TouchUpInside
        )
        
        okButton.setTitle("OK", forState: UIControlState.Normal)
        
        parentView.addSubview(cancelButton)
        parentView.addSubview(okButton)
        
    }
    
    func createLabel(text:String, frame:CGRect) -> UILabel {
        var standardLabel = UILabel(frame: frame)
        standardLabel.font = _fontHelper.fontPrimary(_messageLabelFontSize)
        standardLabel.textColor = UIColor.whiteColor()
        standardLabel.text = text
        
        return standardLabel
    }
    
    func buildBaseButton() -> UIButton {
        
        // large button view used to hide menu
        let screenHeight = _screenBounds.height()
        let screenWidth = _screenBounds.width()
        
        let buttonView = UIButton(
            frame: CGRect(
                x: 0,
                y: 0,
                width: screenWidth,
                height: screenHeight)
        )
        
        buttonView.backgroundColor = UIColor.clearColor()
        
        // add tap outside menu action
        /*
        buttonView.addTarget(self,
        action: "didTapOutsideView:",
        forControlEvents: UIControlEvents.TouchUpInside
        )
        */
        
        return buttonView
    }
    
    func showInView(view:UIView) {
        
        let parentView = view
        
        _screenBounds.positionViewCentral(
            thisView: _view,
            inCenterOf: parentView
        )
        
        parentView.insertSubview(_view, atIndex: 50)
    }
    
    func removeFromSuperView() {
        self._view.removeFromSuperview()
    }
    
    func didTapOutsideView(sender:AnyObject) {
        self.removeFromSuperView()
    }
    
    func didPressCancel(sender:AnyObject) {
        _delegate.didPressCancelOnTextFieldView()
    }
    
    func didPressOk(sender:AnyObject) {
        _delegate.didPressOkOnTextFieldView(_textField.text)
    }
}