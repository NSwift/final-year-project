//
//  MediaFilenameBundle.swift
//  DynamicAssessmentFramework
//
//  Created by Daniel Parkin on 15/02/2015.
//  Copyright (c) 2015 DanielParkin. All rights reserved.
//

import Foundation

class MediaFilenameResponse : NSObject {
    
    var filename:String
    var assetURL:NSURL
    var status:MediaFilenameGeneratorStatus
    
    init(assetURL:NSURL, filename:String, status:MediaFilenameGeneratorStatus) {
        self.filename = filename
        self.assetURL = assetURL
        self.status = status
    }
    
}