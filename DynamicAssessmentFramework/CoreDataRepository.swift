//
//  CoreDataRepository.swift
//  DynamicAssessmentFramework
//
//  Created by Daniel Parkin on 08/01/2015.
//  Copyright (c) 2015 DanielParkin. All rights reserved.
//

import UIKit
import CoreData

class CoreDataRepository: CoreDataBase {

    func entitiesFromObjectIDs(objectIDs:[NSManagedObjectID]) -> [NSManagedObject] {
        
        var entities:[NSManagedObject] = []
        
        for objId in objectIDs {
            let entity = context.existingObjectWithID(objId, error: nil) as NSManagedObject!
            entities.append(entity)
        }
        
        return entities
        
    }
    
}
