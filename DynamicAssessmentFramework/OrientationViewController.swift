//
//  OrientationViewController.swift
//  DynamicAssessmentFramework
//
//  Created by Daniel Parkin on 09/01/2015.
//  Copyright (c) 2015 DanielParkin. All rights reserved.
//

import UIKit
import QuartzCore

class OrientationViewController: UIViewController, UITextFieldDelegate, ParticipantSelectDelegate, DPConfirmationViewDelegate {

    var assessment:Assessment!
    var camera:CameraOperations!
    var previewView:UIView!
    var buttonBeginFrame:CGRect!
    var dateOfBirth:String = ""
    var selectedParticipant:Participant?
    var pendingParticipant:Participant!
    
    weak var assessmentDelegate:AssessmentViewControllerDelegate!

    private var _infoView:DPInfoView!
    private var _confView:DPConfirmationView!
    
    let CDA = CoreDataAccess()
    
    let screenBounds = ScreenBounds()
    let previewY:CGFloat = 50
    let previewWidth:CGFloat = 300
    let previewHeight:CGFloat = 300
    let previewRadius:CGFloat = 50
    
    let buttonBeginY = 430
    let buttonBeginX = 255
    let buttonBeginH = 210
    let buttonBeginW = 260
    
    @IBOutlet weak var dpDateOfBirth: UIDatePicker!
    @IBOutlet weak var btBegin: UIButton!
    @IBOutlet weak var tfParticipantId: UITextField!
    
    @IBAction func dateSelected(sender: UIDatePicker) {
        //self.setDatOfBirth(sender.date)
    }
    
    @IBAction func selectParticipantTapped(sender: AnyObject) {
        var participantViewController = self.storyboard?.instantiateViewControllerWithIdentifier("ParticipantViewController") as ParticipantSelectViewController
        
        participantViewController.delegate = self
        
        self.navigationController?.pushViewController(participantViewController, animated: true)
    }

    @IBAction func beginTapped(sender: AnyObject) {
        
        
        let validationResult = checkAssessmentIsValidToBegin()
        
        if !validationResult.isValid {

            let errorMessages = validationResult.errorMessages
            
            _infoView = DPInfoView(
                messages: errorMessages,
                type: InfoViewType.Error
            )
            
            _infoView.showInView(self.view)
            
        } else {
            
            var foundExistingParticipant:Bool = false
            
            // participant object that will be validated
            var participantToValidate:Participant!
            
            // get selected participant or create new
            if let existingParticipant = self.selectedParticipant {
                
                let idInField = tfParticipantId.text
                
                // is it still the selected participants id in the id field?
                if idInField == existingParticipant.id {
                    participantToValidate = existingParticipant
                    foundExistingParticipant = true
                }
            }
                
            if participantToValidate == nil {
                
                let enteredId = tfParticipantId.text
                
                // no participant has been selected, but does entered id exist
                let participantExists:Bool = CDA.participantRepository().participantAlreadyExists(enteredId)
                
                if participantExists {
                    participantToValidate = CDA.participantRepository().getParticipantById(id: enteredId)
                    foundExistingParticipant = true
                }
                else {
                    // create participant
                    participantToValidate = self.CDA.newObjectForEntity(CDEntity.Participant) as Participant
                    
                    participantToValidate.id = tfParticipantId.text
                    participantToValidate.dateOfBirth = self.getDatOfBirth(dpDateOfBirth.date)
                    
                    self.CDA.save()
                }
                
            }
            
            if foundExistingParticipant {
                
                pendingParticipant = participantToValidate
                
                let infoMessage = [
                    "Use existing participant?",
                    "ID: \(participantToValidate.id)",
                    "DOB: \(participantToValidate.dateOfBirth)"
                ]
                
                _confView = DPConfirmationView(delegate:self, messages: infoMessage)
                
                _confView.showInView(self.view)
                
            }
            else {
                validateParticipantAndBegin(participantToValidate)
            }
        
        }
        
    }
    
    
    func validateParticipantAndBegin(participant:Participant) {
        
        // create assessment view controller
        var assessmentViewController = self.storyboard?.instantiateViewControllerWithIdentifier("BeginAssessment") as AssessmentViewController
        
        // set the assessment view controller delegate
        assessmentViewController.delegate = assessmentDelegate
        
        // validate whether participant has already taken part in assessment
        let validationResult = validateParticipant(participant)
        
        if !validationResult.isValid {
            let errorMessages = validationResult.errorMessages
            
            _infoView = DPInfoView(
                messages: errorMessages,
                type: InfoViewType.Error
            )
            
            _infoView.showInView(self.view)
        }
        else {
            
            assessmentViewController.participant = participant
            assessmentViewController.assessment = self.assessment
            
            self.navigationController?.pushViewController(assessmentViewController, animated: true)
        }
        
    }
    
    // MARK: View methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // set participant text field delegate
        tfParticipantId.delegate = self
        
        buttonBeginFrame = CGRect(
            x: 430,
            y: 10,
            width: self.buttonBeginW,
            height: self.buttonBeginH
        )

        // animate begin button
        UIView.animateWithDuration(1.0,
            delay: 0.0,
            options: UIViewAnimationOptions.CurveEaseIn,
            animations: {
                self.btBegin.frame = self.buttonBeginFrame
            }, completion: nil)
        
        // configure the date picker
        dpDateOfBirth.datePickerMode = UIDatePickerMode.Date
        //let currentDate = NSDate()
        let startDate = NSDate(dateString: "01/01/1950")
        dpDateOfBirth.date = startDate
        
        // instantiate the preview layer
        self.previewView = UIView(
            frame: CGRect(
                x: 0,
                y: self.previewY,
                width: self.previewWidth,
                height: self.previewHeight)
        )
        
        // modify preview view
        self.previewView.layer.cornerRadius = previewRadius
        self.previewView.layer.masksToBounds = true

        
        // centre the preview layer
        self.screenBounds.positionViewHorizontalCentral(
            thisView: self.previewView,
            inCenterOf: self.view
        )
        
        // add the preview layer to the screen
        self.camera = CameraOperations(rootLayer: previewView.layer)
        self.view.addSubview(self.previewView)
    
    }
    
    func getDatOfBirth(date:NSDate) -> String {
        let dateFormatter = NSDateFormatter()
        var theDateFormat = NSDateFormatterStyle.ShortStyle
        
        dateFormatter.dateStyle = theDateFormat
        
        return dateFormatter.stringFromDate(date)
    }
    
    // MARK: UITextFieldDelegate
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        return true
    }
    
    // MARK: ParticipantSelectDelegate
    func didSelectParticipant(participant: Participant) {
        self.selectedParticipant = participant
        let startDate = NSDate(dateString: participant.dateOfBirth)
        dpDateOfBirth.date = startDate
        tfParticipantId.text = participant.id
    }
    
    // MARK: Validation
    func checkAssessmentIsValidToBegin() -> ValidationResult {
        
        var errors:[String] = [];
        var isValid = true
        
        let enteredId = tfParticipantId.text
        
        // participant id is empty
        if countElements(enteredId) == 0 {
            errors.append("Please enter a participant ID.")
            isValid = false
        }
        
        return ValidationResult(isValid: isValid, errorMessages: errors)
    }
    
    // MARK: Validate participant
    func validateParticipant(participant:Participant) -> ValidationResult {
        
        var errors:[String] = [];
        var isValid = true
        
        let resultCount = CDA.resultRepository().getResultCountFor(
            participant: participant,
            assessment: assessment
        )
        
        // check if participant has already taken part in assessment
        if resultCount > 0 {
            errors.append("\(participant.id) has already taken part in this assessment.")
            isValid = false
        }
        
        return ValidationResult(isValid: isValid, errorMessages: errors)
        
    }
    
    func doDiscard() {
        
        // show confirmation message
        let infoMessage = ["Are you sure you want to discard your changes?"]
        _confView = DPConfirmationView(delegate:self, messages: infoMessage)
        
        _confView.showInView(self.view)
    }
    
    // MARK: DPConfirmationViewDelegate
    func didPressOk() {
        _confView.removeFromSuperView()
        validateParticipantAndBegin(pendingParticipant)
    }
    
    func didPressCancel() {
        pendingParticipant = nil
        _confView.removeFromSuperView()
    }
    
    deinit {
        println("Edit focus point deinit")
    }
    
}
