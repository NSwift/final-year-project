//
//  AssessmentScene.swift
//  DynamicAssessmentFramework
//
//  Created by Daniel Parkin on 20/08/2014.
//  Copyright (c) 2014 Echoyo. All rights reserved.
//

import Foundation
import UIKit

class AssessmentScene:NSObject, ActResponseDelegate {
    
    var acts:[AssessmentActProtocol]?
    var currentActIndex:Int
    var maxActIndex:Int
    var currentActView:UIView?
    var rootView:UIView
    var recordScene:Bool
    weak var delegate:SceneResponseDelegate?
    weak var question:Question?
    
    init(rootView:UIView, question:Question, delegate:SceneResponseDelegate, recordScene:Bool) {
        self.acts = []
        self.currentActIndex = 0
        self.maxActIndex = 0
        self.currentActView = nil
        self.rootView = rootView
        self.recordScene = recordScene
        self.delegate = delegate
        self.question = question
    }
    
    func setRootView(rootView:UIView) {
        self.rootView = rootView
    }
    
    func setActs(acts:[AssessmentActProtocol]) {
        if acts.count > 0  {
            maxActIndex = acts.count - 1
        }
        self.acts = acts
    }
    
    func clearActs() {
        self.acts = []
    }
    
    func displayNextAct() {

        // check if this is the last view
        if currentActIndex <= maxActIndex {
            
            // remove previous act
            rootView.subviews.map { $0.removeFromSuperview() }

            // set the current view
            acts![currentActIndex].beginDisplayOnView(rootView)
            
            // increment the index count
            currentActIndex++
            
        }
        else {
            // scene is finished
            var sceneResponse = SceneResponse(
                status: SceneResponseStatus.CompleteGetNextScene,
                bundle: nil)
        }
    }
    
    // MARK: ActResponseDelegate
    func actDidRespond(response: ActResponse) {
        
        var sceneResponse:SceneResponse!
        
        /*
            Handle ActResponseStatus.MediaWasReplayed
        */
        
        if response.status == ActResponseStatus.CompleteGetNextAct {
            
            /*
                Reset current timer
            */
            self.displayNextAct()
            
        }
        else if response.status == ActResponseStatus.CompletedWithBundle {
            
            if let answer = response.bundle!.answer {
                
                /*
                    Update scene response to include duration and media replays
                */
                
                sceneResponse = SceneResponse(
                    status: SceneResponseStatus.CompletedWithBundle,
                    bundle: response.bundle)
                
                delegate?.sceneDidRespond(sceneResponse)
            }
            
        }
        else if response.status == ActResponseStatus.CompletedWithoutAnswer {
            
            sceneResponse = SceneResponse(
                status: SceneResponseStatus.CompleteGetNextScene
            )
            
            delegate?.sceneDidRespond(sceneResponse)
        }

    }
    
    deinit {
        println("Assessment scene dealloced")
    }
}