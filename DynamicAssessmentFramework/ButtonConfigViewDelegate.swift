//
//  ButtonConfigViewDelegate.swift
//  DynamicAssessmentFramework
//
//  Created by Daniel Parkin on 28/02/2015.
//  Copyright (c) 2015 DanielParkin. All rights reserved.
//

protocol ButtonConfigViewDelegate : class {
    func buttonViewDidProvideAnswer(answer:Answer)
}