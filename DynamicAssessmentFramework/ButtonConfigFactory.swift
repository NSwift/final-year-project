//
//  ButtonConfigFactory.swift
//  DynamicAssessmentFramework
//
//  Created by Daniel Parkin on 28/02/2015.
//  Copyright (c) 2015 DanielParkin. All rights reserved.
//

class ButtonConfigFactory {
    
    func getButtonConfig(answers:[Answer], shouldAnimate:Bool, appState:ApplicationState) -> ButtonConfigViewProtocol {
        
        let answerCount = answers.count
        
        if answerCount <= 4 {
            return ButtonConfigOneRow(
                answers: answers,
                shouldAnimate: shouldAnimate,
                appState: appState
            )
        }
        else {
            return ButtonConfigTwoRow(
                answers: answers,
                shouldAnimate: shouldAnimate,
                appState: appState
            )
        }
        
    }
    
}
