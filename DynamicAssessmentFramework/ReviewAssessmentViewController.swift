//
//  ReviewAssessmentViewController.swift
//  DynamicAssessmentFramework
//
//  Created by Daniel Parkin on 10/01/2015.
//  Copyright (c) 2015 DanielParkin. All rights reserved.
//

import UIKit

class ReviewAssessmentViewController: UIViewController, UIPageViewControllerDataSource, DPSideMenuDelegate, UITableViewDelegate, UITableViewDataSource, ParticipantSelectDelegate {

    private var assessments:[Assessment]!
    private var participants:[Participant]!
    private var sideMenu:DPSideMenu!
    private var tvAssessments:UITableView!
    private var pageViewController:UIPageViewController?
    private var questions:[ActQuestion] =  []
    private var itemControllers:[ReviewAssessmentItemViewController] = []
    
    private var _resultsExporter:ResultsExporterCSV!
    private var _resultsDelivery:ResultsDeliveryEmail!
    
    var assessment:Assessment!
    
    let menuWidth:CGFloat = 300
    let menuItemHeight:CGFloat = 80
    let tableCellHeight:CGFloat = 50
    
    let CDA = CoreDataAccess()
    let fontHelper = FontHelper()
    let colorHelper = ColorHelper()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // get all assessments for tableview
        self.assessments = CDA.assessmentRepository().getAllAssessmentsWithResults()
    
        // get questions to populate page item controllers
        self.getQuestions()
        
        // get all participants for current assessment
        self.getParticipants()
        
        // instantiate page controller
        self.initItemControllers()
        self.initPageViewController()
        self.configurePageControl()
        
        // instantiate tableview
        self.initTableView()
        
        // instantiate side menu
        sideMenu = self.initSideMenu()
        
        // adding bar button item
        var barButtonItem = UIBarButtonItem(
            title: "m",
            style: UIBarButtonItemStyle.Plain,
            target: self,
            action: "menuTapped"
        )
    
        barButtonItem.setTitleTextAttributes([NSFontAttributeName: fontHelper.fontGlyphicons(25)], forState: UIControlState.Normal)
        
        self.navigationItem.rightBarButtonItem = barButtonItem
    
    }
    
    // MARK: Get questions
    func getQuestions() {
        // instantiate item builder and get questions
        let itemBuilder = AssessmentReviewItemBuilder(assessment: self.assessment)
        self.questions = itemBuilder.buildReviewItems()
    }
    
    // MARK: Get participants
    func getParticipants() {
        self.participants = CDA.participantRepository().getParticipantsByAssessment(self.assessment)
    }
    
    private func initPageViewController() {
        
        let pageController = self.storyboard!.instantiateViewControllerWithIdentifier("AssessmentReviewPageController") as UIPageViewController
        pageController.dataSource = self
        
        if questions.count > 0 {
            pageController.setViewControllers(
                self.getFirstController(),
                direction: UIPageViewControllerNavigationDirection.Forward,
                animated: false,
                completion: nil)
        }
        
        pageViewController = pageController
        addChildViewController(pageViewController!)
        self.view.addSubview(pageViewController!.view)
        pageViewController!.didMoveToParentViewController(self)
    }
    
    private func configurePageControl() {
        let appearance = UIPageControl.appearance()
        appearance.pageIndicatorTintColor = UIColor.grayColor()
        appearance.currentPageIndicatorTintColor = UIColor.whiteColor()
        appearance.backgroundColor = UIColor.darkGrayColor()
    }
    
    func getFirstController() -> NSArray {
        let firstController = getItemController(0)!
        let startingViewController: NSArray = [firstController]
        return startingViewController
    }
    
    // MARK: - UIPageViewControllerDataSource
    func pageViewController(pageViewController: UIPageViewController, viewControllerBeforeViewController viewController: UIViewController) -> UIViewController? {
        
        let itemController = viewController as ReviewAssessmentItemViewController
        
        if itemController.itemIndex > 0 {
            return getItemController(itemController.itemIndex - 1)
        }
        
        return nil
    }
    
    func pageViewController(pageViewController: UIPageViewController, viewControllerAfterViewController viewController: UIViewController) -> UIViewController? {
        
        let itemController = viewController as ReviewAssessmentItemViewController
        
        if itemController.itemIndex + 1 < questions.count {
            return getItemController(itemController.itemIndex + 1)
        }
        
        return nil
    }
    
    private func getItemController(itemIndex: Int) -> ReviewAssessmentItemViewController? {
        
        if itemIndex < questions.count {
            return self.itemControllers[itemIndex]
        }
        
        return nil
    }
    
    func initItemControllers() {
        
        self.itemControllers = []
        
        for var i = 0; i < self.questions.count; i++ {
            let pageItemController = self.storyboard!.instantiateViewControllerWithIdentifier("ReviewAssessmentItemController") as ReviewAssessmentItemViewController
            pageItemController.itemIndex = i
            pageItemController.question = self.questions[i]
            self.itemControllers.append(pageItemController)
        }
        
    }
    
    // MARK: - Page Indicator
    func presentationCountForPageViewController(pageViewController: UIPageViewController) -> Int {
        return questions.count
    }
    
    func presentationIndexForPageViewController(pageViewController: UIPageViewController) -> Int {
        return 0
    }
    
    deinit {
        println("Review view controller dealloc")
    }
    
    // MARK: DPSideMenu
    func initSideMenu() -> DPSideMenu {
        
        let menuItemIndividual = DPSideMenuItem(itemText: "Individual", iconCharacter: "i")
        let menuItemExport = DPSideMenuItem(itemText: "Export", iconCharacter: "x")
        let menuItemHide = DPSideMenuItem(itemText: "Hide", iconCharacter: "h")
        
        let menuItems = [menuItemIndividual, menuItemExport, menuItemHide]
        
        let sideMenu = DPSideMenu(
            menuItems: menuItems,
            menuWidth: self.menuWidth,
            itemHeight:self.menuItemHeight,
            delegate: self,
            tableView: tvAssessments,
            tableTitle: "Select Assessment")
        
        return sideMenu
    }
    
    // MARK: DPSideMenu tapped
    func menuTapped() {
        self.sideMenu.showInViewWithNavbar(
            showIn: self.view,
            withNavBar: self.navigationController!.navigationBar
        )
    }
    
    // MARK: DPSideMenuDelegate
    func didSelectMenuItem(itemIndex:Int) {
        
        switch (itemIndex) {
        case 0:

            var participantViewController = self.storyboard?.instantiateViewControllerWithIdentifier("ParticipantViewController") as ParticipantSelectViewController
            
            participantViewController.participants = self.participants
            participantViewController.delegate = self
            
            self.navigationController?.pushViewController(participantViewController, animated: true)
            
        case 1:
            
            _resultsExporter = ResultsExporterCSV(assessment: self.assessment)
            
            let resultsBundle = _resultsExporter.export()
    
            _resultsDelivery = ResultsDeliveryEmail(resultsBundle: resultsBundle)
            _resultsDelivery.show(self)
            
        case 2:

            self.menuTapped()
            
        default:
            println("default option")
        }
    }
    
    // MARK: Initialise assessments table view
    func initTableView() {
        
        self.tvAssessments = UITableView(
            frame: CGRect(
                x: 0,
                y: 0,
                width: self.menuWidth,
                height: 200)
        )
        
        self.tvAssessments.backgroundColor = colorHelper.colorPrimary05()
        
        self.tvAssessments.delegate = self
        self.tvAssessments.dataSource = self
        self.view.addSubview(self.tvAssessments)
    }
    
    // MARK: UITableViewDelegate
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.assessments.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cellIdentifier = "assessmentCell"
        
        // variable type is inferred
        var cell = tableView.dequeueReusableCellWithIdentifier(cellIdentifier) as? DPCellBasic
        
        if cell == nil {
        
            // instantiate custom cell
            cell = DPCellBasic(
                style: UITableViewCellStyle.Value1,
                reuseIdentifier: cellIdentifier,
                height: self.tableCellHeight,
                width: self.menuWidth
            )
        }
        
        cell!.lvTitle.text = self.assessments[indexPath.row].title
        
        return cell!
    }
    
    func tableView(tableView: UITableView!, didSelectRowAtIndexPath indexPath: NSIndexPath!) {

        // deselect currently highlighted row
        self.tvAssessments.deselectRowAtIndexPath(indexPath!, animated: true)
        
        // update assment with one select from table view
        self.assessment = self.assessments[indexPath.row]
        
        // use selected assessment to refresh questions and page item controls
        self.getQuestions()
        self.getParticipants()
        self.initItemControllers()
        
        self.pageViewController!.setViewControllers(
            self.getFirstController(),
            direction: UIPageViewControllerNavigationDirection.Forward,
            animated: false,
            completion: nil)
    }
    
    func tableView(tableView:UITableView!, heightForRowAtIndexPath indexPath:NSIndexPath)->CGFloat
    {
        return self.tableCellHeight
    }
    
    
    // MARK: ParticipantSelectDelegate
    func didSelectParticipant(participant:Participant) {
        
        var individualView = self.storyboard?.instantiateViewControllerWithIdentifier(
            "ReviewIndividualViewController") as ReviewIndividualViewController
        
        individualView.assessment = self.assessment
        individualView.participant = participant
        
        self.navigationController?.pushViewController(
            individualView,
            animated: true
        )
        
    }
    
}
