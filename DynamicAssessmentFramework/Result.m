//
//  Result.m
//  DynamicAssessmentFramework
//
//  Created by Daniel Parkin on 14/02/2015.
//  Copyright (c) 2015 DanielParkin. All rights reserved.
//

#import "Result.h"
#import "Answer.h"
#import "Assessment.h"
#import "Participant.h"
#import "Question.h"


@implementation Result

@dynamic recordingUrl;
@dynamic mediaReplays;
@dynamic responseTime;
@dynamic answer;
@dynamic assessment;
@dynamic participant;
@dynamic question;

@end
