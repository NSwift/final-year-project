//
//  UIColorHelper.swift
//  DynamicAssessmentFramework
//
//  Created by Daniel Parkin on 27/09/2014.
//  Copyright (c) 2014 DanielParkin. All rights reserved.
//

import UIKit

class ColorHelper: NSObject {
        
    let availableColors = [
        "White": UIColor.whiteColor(),
        "Dark Gray": UIColor.darkGrayColor(),
        "Light Gray": UIColor.lightGrayColor(),
        "Gray": UIColor.grayColor(),
        "Red": UIColor.redColor(),
        "Green": UIColor.greenColor(),
        "Blue": UIColor.blueColor(),
        "Cyan": UIColor.cyanColor(),
        "Yellow": UIColor.yellowColor(),
        "Magenta": UIColor.magentaColor(),
        "Orange": UIColor.orangeColor(),
        "Purple": UIColor.purpleColor(),
        "Brown": UIColor.brownColor(),
        "Black": UIColor.blackColor()
    ]
    

    func availableColorNames() -> [String] {
        return availableColors.keys.array
    }
    
    // attemps to get color by name, returns
    // white by default if not found
    func colorNamed(name:String) -> UIColor {
        var selectedColor = UIColor.whiteColor()
        if let color = availableColors[name] {
            selectedColor = color
        }
        return selectedColor
    }
    
    // return app theme colours
    func colorPrimary5() -> UIColor {
        return self.UIColorFromHex(0x4CAF50)
    }
    
    func colorPrimary4() -> UIColor {
        return self.UIColorFromHex(0x66BB6A)
    }
    
    func colorPrimary3() -> UIColor {
        return self.UIColorFromHex(0x81C784)
    }
    
    func colorPrimary2() -> UIColor {
        return self.UIColorFromHex(0xA5D6A7)
    }
    
    func colorPrimary1() -> UIColor {
        return self.UIColorFromHex(0xC8E6C9)
    }
    
    func colorPrimary05() -> UIColor {
        return self.UIColorFromHex(0xE8F5E9)
    }
    
    // generate UIColor from hex value
    func UIColorFromHex(rgbValue:UInt32, alpha:Double=1.0) -> UIColor {
        let red = CGFloat((rgbValue & 0xFF0000) >> 16)/256.0
        let green = CGFloat((rgbValue & 0xFF00) >> 8)/256.0
        let blue = CGFloat(rgbValue & 0xFF)/256.0
        
        return UIColor(red:red, green:green, blue:blue, alpha:CGFloat(alpha))
    }

   
}
