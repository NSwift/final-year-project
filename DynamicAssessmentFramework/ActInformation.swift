//
//  ActInformation.swift
//  DynamicAssessmentFramework
//
//  Created by Daniel Parkin on 20/08/2014.
//  Copyright (c) 2014 Echoyo. All rights reserved.
//

import Foundation
import UIKit

class ActInformation:NSObject, AssessmentActProtocol {
    
    var message:String
    weak var delegate:ActResponseDelegate?
    var controls:[UIControl]
    var screenBounds:ScreenBounds
    var fontHelper = FontHelper()
    
    init(message:String, delegate:ActResponseDelegate) {
        self.message = message
        self.delegate = delegate
        self.screenBounds = ScreenBounds()
        self.controls = []
    }
    
    func beginDisplayOnView(view: UIView) {
        let actView = UIView(frame: screenBounds.bounds())
        actView.backgroundColor = UIColor.darkGrayColor()
        
        // label
        var label = UILabel(frame: CGRect(x: 0, y: 100, width: 700, height: 700))
        label.backgroundColor = UIColor.darkGrayColor()
        label.text = self.message
        label.font = fontHelper.fontPrimary(50)
        label.textAlignment = NSTextAlignment.Center
        label.numberOfLines = 0
        label.textColor = UIColor.whiteColor()
        
        // button
        var button:DPUIButton = DPUIButton(
            frame: CGRect(x: 50, y: 820, width: 200, height: 100),
            rounded: true,
            animates: true
        )
        
        button.backgroundColor = UIColor.whiteColor()
        button.setTitleColor(UIColor.blackColor(), forState: UIControlState.Normal)
        button.setTitle("Continue", forState: UIControlState.Normal)
        button.addTarget(self, action: "userPressedContinue:", forControlEvents: UIControlEvents.TouchUpInside)
        button.titleLabel!.font = fontHelper.fontPrimary(30)

        screenBounds.positionViewHorizontalCentral(
            thisView: label,
            inCenterOf: actView
        )
        
        screenBounds.positionViewHorizontalCentral(
            thisView: button,
            inCenterOf: actView
        )
        
        // add controls to array to retain reference
        controls.append(button)
        
        actView.addSubview(label)
        actView.addSubview(button)
        
        view.addSubview(actView)
    }
    
    func userPressedContinue(sender:AnyObject) {
        let status = ActResponseStatus.CompleteGetNextAct
        let response = ActResponse(status: status, bundle: nil)
        
        delegate?.actDidRespond(response)
    }
    
}