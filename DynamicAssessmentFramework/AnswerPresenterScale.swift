//
//  AnswerPresenterScale.swift
//  DynamicAssessmentFramework
//
//  Created by Daniel Parkin on 21/08/2014.
//  Copyright (c) 2014 Echoyo. All rights reserved.
//

import Foundation
import UIKit

class AnswerPresenterScale:NSObject, AnswerPresentationProtocol {
    
    weak var selectedAnswer:Answer?
    weak var question:Question?
    var controls:[NSObject]
    weak var delegate:AnswerPresenterDelegate?
    var screenBounds:ScreenBounds
    
    init(question:Question, appState:ApplicationState, delegate:AnswerPresenterDelegate) {
        self.question = question
        self.selectedAnswer = Answer()
        self.controls = [NSObject]()
        self.delegate = delegate
        self.screenBounds = ScreenBounds()
    }
    
    func displayOnView(view: UIView) {
        
        // get images for use with scale
        var scaleImages:[UIImage] = getScaleImages()
        
        // initial view setup
        var answerView = UIView(frame: screenBounds.bottomHalf())
        //var answerView = UIView(frame: CGRect(x: 0, y: 250, width: 320, height: 480))
        
        
        answerView.backgroundColor = UIColor.grayColor()
        
        // button display parameters
        let totalAnswers = question?.answers.count
        let spacing = 20
        let buttonWidth = 80
        let buttonHeight = 300
        var xPos = 10;
        
        // create containing view for buttons so they can be centred
        let buttonViewWidth = (totalAnswers! * (buttonWidth + spacing))
        let bvCentrePos = (Int(screenBounds.bounds().width) / 2) - (buttonViewWidth / 2)
        
        var buttonView = UIView(frame: CGRect(x: bvCentrePos, y: 50, width: buttonViewWidth, height: buttonHeight))
        
        // add buttons to button view
        for var i = 0; i < totalAnswers; i++ {
            
            // button
            var button:UIButton = UIButton(frame: CGRect(x: xPos, y: 50, width: buttonWidth, height: buttonHeight))
            
            button.backgroundColor = UIColor.blueColor()
            button.setImage(scaleImages[i], forState: UIControlState.Normal)
            
            // give button tag with array index of answer
            button.tag = i
            
            button.addTarget(self, action: "valueFromControl:", forControlEvents: UIControlEvents.TouchUpInside)
            
            button.exclusiveTouch = true
            
            // add controls to array to retain reference
            controls.append(button)
            
            buttonView.addSubview(button)
            
            // increment yPos
            xPos += (buttonWidth + spacing)
        }
        
        answerView.addSubview(buttonView)
        
        view.addSubview(answerView)

    }

    /*
    func valueFromControl(sender:AnyObject) {
        if sender is UIButton {
            
            // try sending information to delegate
            //let selectedAnswer = question.answers.allObjects[sender.tag] as Answer
            
            let selectedAnswer = question?.answers.array[sender.tag] as Answer
            
            // create non managed answer
            //var nonManagedAnswer = NMAnswer(value: selectedAnswer.value)
            
            let status = ActResponseStatus.CompletedWithAnswer
            let response = ActResponse(status: status, answer: selectedAnswer)
            
            delegate?.actDidRespond(response)
        }
    }
    */
    
    func valueFromControl(sender:AnyObject) {
        if sender is UIButton {
            
            let selectedAnswer = question?.answers.array[sender.tag] as Answer
            
            delegate?.didProvideAnswer(selectedAnswer)
        }
    }

    func getScaleImages() -> [UIImage] {
        
        // load full length scale
        var loadedImages = [UIImage]()
        
        // creat images views
        var image1 = UIImage(named: "SAM01.png")
        var image2 = UIImage(named: "SAM02.png")
        var image3 = UIImage(named: "SAM03.png")
        var image4 = UIImage(named: "SAM04.png")
        var image5 = UIImage(named: "SAM05.png")
        var image6 = UIImage(named: "SAM06.png")
        var image7 = UIImage(named: "SAM07.png")
        var image8 = UIImage(named: "SAM08.png")
        var image9 = UIImage(named: "SAM09.png")
        
        // add images to image array
        //loadedImages = [image1, image2, image3, image4, image5, image6, image7, image8, image9]
        
        // calculate step count
        let answerCount = question?.answers.count
        let loadedImageCount = loadedImages.count
        var stepCount = loadedImageCount - answerCount!
        
        //println("step count: \(stepCount)")
        
        if stepCount == 0 { stepCount = 1 }
        
        var scaleImages = [UIImage]()
        
        for var i = 0; i < loadedImageCount; i = i + stepCount {
            //println(i)
            scaleImages.append(loadedImages[i])
        }
        
        return scaleImages
    }
}