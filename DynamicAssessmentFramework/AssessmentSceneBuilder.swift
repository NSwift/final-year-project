//
//  AssessmentSceneBuilder.swift
//  DynamicAssessmentFramework
//
//  Created by Daniel Parkin on 08/01/2015.
//  Copyright (c) 2015 DanielParkin. All rights reserved.
//

import UIKit

class AssessmentSceneBuilder: NSObject {
   
    var assessment:Assessment
    var rootView:UIView
    weak var delegate:SceneResponseDelegate?
    weak var camDelegate:QuestionRecordingDelegate!
    
    let CDA = CoreDataAccess()
    
    init(assessment:Assessment, rootView:UIView, delegate:SceneResponseDelegate, camDelegate:QuestionRecordingDelegate) {
        self.assessment = assessment
        self.rootView = rootView
        self.delegate = delegate
        self.camDelegate = camDelegate
    }
    
    func buildScenes() -> [AssessmentScene] {
        
        // create collection to hold generated scenes
        var scenes:[AssessmentScene] = []
        
        // get questions
        var questions:[Question] = self.CDA.questionRepository().getQuestionsByAssessment(self.assessment, ascending: true)
        
        // iterate over each question, build acts for each scene
        for (index, question) in enumerate(questions) {
            
            let currentQuestion = question as Question
            
            var newScene = AssessmentScene(
                rootView: self.rootView,
                question:currentQuestion,
                delegate:self.delegate!,
                recordScene: true)
            
            var acts:[AssessmentActProtocol] = []
            
            // add start screen or not
            if index == 0 {
                if assessment.hasStartScreen.boolValue {
                    
                    var actStartScreen = ActStartScreen(delegate: newScene)
                    acts.append(actStartScreen)
                    
                }
            }
            
            // add focus point if neccessary
            if let focusPointValue = currentQuestion.hasFocusPoint {
                
                let hasSeque = focusPointValue.boolValue
                
                if hasSeque {
                    
                    // check if assessment uses custom focus point
                    if let useCustomFP = assessment.usesCustomFocusPoint {
                        
                        let useCustom = useCustomFP.boolValue
                        
                        var actFocusPoint:AssessmentActProtocol!
                        
                        if(useCustom) {
                            actFocusPoint = ActFocusPointCustom(
                                focusPoint:assessment.focusPoint,
                                seconds: 5,
                                delegate: newScene
                            )
                        }
                        else {
                            actFocusPoint = ActSegue(
                                seconds: 5,
                                delegate: newScene
                            )
                        }
                        
                        acts.append(actFocusPoint)
                    
                    }
                
                }
            }
            
            // add intro text
            if let introText = currentQuestion.introText {
                if introText != "" {
                    let actInfo = ActInformation(message: currentQuestion.introText, delegate: newScene)
                    acts.append(actInfo)
                }
            }
            
            // add countdown if neccessary
            if let countValue = currentQuestion.countdown {
                
                let countdown = currentQuestion.countdown.toInt()
                
                if countdown > 0 {
                    
                    let actCountdown = ActCountdown(
                        seconds: countdown!,
                        delegate: newScene
                    )
                    
                    acts.append(actCountdown)
                }
            }
            
            // add the question
            let actQuestion = ActQuestion(
                question: currentQuestion,
                delegate: newScene,
                appState: ApplicationState.Assessment
            )
            
            // should the question record?
            // if question set to record, set the delegate
            if let shouldRecord = currentQuestion.shouldRecord {
                if shouldRecord.boolValue { actQuestion.cameraDelegate = camDelegate }
            }
            
            acts.append(actQuestion)
            
            newScene.setActs(acts)
            
            scenes.append(newScene)
        }
        
        return scenes
        
    }
    
}
