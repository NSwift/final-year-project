//
//  ProtocolAnswerPresentation.swift
//  QuestionsTests
//
//  Created by Daniel Parkin on 19/08/2014.
//  Copyright (c) 2014 Echoyo. All rights reserved.
//

import Foundation
import UIKit

protocol AnswerPresentationProtocol {
    weak var selectedAnswer:Answer? { get set }
    weak var question:Question? { get set }
    var controls:[NSObject] { get set }
    func valueFromControl(sender:AnyObject)
    func displayOnView(view:UIView)
}