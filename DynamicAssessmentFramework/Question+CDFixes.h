//
//  Question+CDFixes.h
//  DynamicAssessmentFramework
//
//  Created by Daniel Parkin on 17/10/2014.
//  Copyright (c) 2014 DanielParkin. All rights reserved.
//

#import "Question.h"

@interface Question (CDFixes)

- (void)addAnswersObject:(Answer *)value;

@end
