//
//  DPCellBasic.swift
//  DynamicAssessmentFramework
//
//  Created by Daniel Parkin on 14/01/2015.
//  Copyright (c) 2015 DanielParkin. All rights reserved.
//

import UIKit

class DPCellBasic : UITableViewCell {
    
    var lvTitle:UILabel!
    
    let colorHelper = ColorHelper()
    let fontHelper = FontHelper()
    
    init(style: UITableViewCellStyle, reuseIdentifier: String!, height:CGFloat, width:CGFloat) {
        super.init(style: UITableViewCellStyle.Value1, reuseIdentifier: reuseIdentifier)
        
        self.lvTitle = UILabel(
            frame: CGRect(
                x: 20,
                y: 0,
                width: width,
                height: height)
        )
        
        // configure cell background color
        self.backgroundColor = colorHelper.colorPrimary4()
        
        self.lvTitle.backgroundColor = colorHelper.colorPrimary4()
        self.lvTitle.textColor = UIColor.whiteColor()
        self.lvTitle.font = fontHelper.fontPrimary(20)
        self.lvTitle.text = "Null"
        self.addSubview(self.lvTitle)
        
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder);
    }
    
}
