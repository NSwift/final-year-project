//
//  EditQuestionViewController.swift
//  DynamicAssessmentFramework
//
//  Created by Daniel Parkin on 25/08/2014.
//  Copyright (c) 2014 Echoyo. All rights reserved.
//

import UIKit

enum QuestionConfirmationRequest {
    case ClearAllAnswers
    case QuitWithoutSaving
}

class EditQuestionViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UINavigationControllerDelegate, UIPickerViewDataSource, UIPickerViewDelegate, MediaImporterDelegate, UITextFieldDelegate, MediaPlayerDelegate, MediaFilenameGeneratorDelegate, CustomAnswerPresenterDelegate, DPConfirmationViewDelegate {

    // MARK: IBOutlets
    @IBOutlet weak var tvAnswers : UITableView!
    @IBOutlet weak var tfAddAnswer : UITextField!
    @IBOutlet weak var tfCountdown : UITextField!
    @IBOutlet weak var tfIntroductionText : UITextField!
    @IBOutlet weak var tfQuestionText : UITextField!
    @IBOutlet weak var tfPauseTime: UITextField!
    @IBOutlet weak var swHasFocusPoint: UISwitch!
    @IBOutlet weak var swRecordVideo: UISwitch!
    @IBOutlet weak var pvScaleSize: UIPickerView!
    @IBOutlet weak var pvMediaType: UIPickerView!
    @IBOutlet weak var pvQuestionType: UIPickerView!
    @IBOutlet weak var pvAnswerBehaviour: UIPickerView!
    @IBOutlet weak var uvMediaPreview: UIView!
    @IBOutlet weak var btAddAnswer: UIButton!
    @IBOutlet weak var btEditCustomAnswer: UIButton!
    @IBOutlet weak var lvScaleSize: UILabel!
    @IBOutlet weak var lvPauseTime: UILabel!
    @IBOutlet weak var lvCustomAnswer: UILabel!
    @IBOutlet weak var vwCustomAnswer: UIView!
    @IBOutlet weak var stCountdown: UIStepper!
    @IBOutlet weak var stPauseTime: UIStepper!
    
    // MARK: Properties
    weak var delegate:QuestionEditorDelegate!
    var question:Question! = nil
    var importedMedia:NSData! = nil
    
    
    var answers:[String] = []
    var mediaTypeToSave:String = ""
    var mediaAssetURLToSave:String = ""
    var correctAnswer:String = ""
    var isInitialAppearance:Bool = true
    
    
    var CDA = CoreDataAccess()
    var imagePicker = UIImagePickerController()
    var questionTypeData = ["Buttons", "Slider", "Scale", "Custom", "Not Required"]
    var mediaTypeData = ["Audio", "Image", "Video"]
    var answerBehaviorData = ["Display immediately", "Wait for media", "Pause for interval"]
    var scaleSizes = ["3","4","5","7","9"]
    var mediaImporter:MediaImporterProtocol?
    var mediaPlayer:MediaPlayerProtocol?
    var correctAnswerIndexPath:NSIndexPath!
    var filenameGenerator:MediaFilenameGeneratorProtocol!
    
    let mediaImportFact = MediaImporterFactory()
    let mediaPlayerFact = MediaPlayerFactory()
    let mediaFilenameFact = MediaFilenameGeneratorFactory()
    
    let colorHelper = ColorHelper()
    let fontHelper = FontHelper()
    let screenBounds = ScreenBounds()
    
    // update properties to use private access
    private var _spinner:DPSpinner!
    private var _infoView:DPInfoView!
    private var _confView:DPConfirmationView!
    private var _lastConfReq:QuestionConfirmationRequest!
    
    // MARK: IBAction methods
    @IBAction func pauseTimeChanged(sender: AnyObject) {
        /*
        */
    }
    
    @IBAction func countdownStepperChanged(sender: UIStepper) {
        tfCountdown.text = "\(Int(sender.value))"
    }

    @IBAction func pauseTimeStepperChanged(sender: UIStepper) {
        tfPauseTime.text = "\(Int(sender.value))"
    }
    
    @IBAction func clearAllTapped(sender: AnyObject) {
        
        _lastConfReq = QuestionConfirmationRequest.ClearAllAnswers
        
        _confView = DPConfirmationView(
            delegate: self,
            messages: ["Are you sure you want to delete all answers?"]
        )
        
        _confView.showInView(self.view)
    }
    
    @IBAction func addMediaTapped(sender : AnyObject) {
        var mediaType = mediaTypeData[pvMediaType.selectedRowInComponent(0)]
        self.mediaImporter = mediaImportFact.getImporter(mediaType)
        self.mediaImporter!.delegate = self
        self.presentViewController(self.mediaImporter!.controller(), animated: true, completion: nil)
    }
    
    
    // MARK: Save Functionality
    @IBAction func saveTapped(sender : AnyObject) {
        
        // get question type
        var questionType = questionTypeData[pvQuestionType.selectedRowInComponent(0)]

        // get media type
        var mediaType = mediaTypeData[pvMediaType.selectedRowInComponent(0)]
        
        // get pause time
        let pauseTime = tfPauseTime.text.toInt()
        
        // get answer display behaviour
        var answerDisplayBehavior = answerBehaviorData[pvAnswerBehaviour.selectedRowInComponent(0)]
        
        question.questionType = questionType
        question.countdown = tfCountdown.text
        question.introText = tfIntroductionText.text
        question.questionText = tfQuestionText.text
        question.mediaType = mediaTypeToSave
        question.assetURL = mediaAssetURLToSave
        question.hasFocusPoint = swHasFocusPoint.on
        question.shouldRecord = swRecordVideo.on
        question.pauseTime = pauseTime
        question.answerDisplayBehavior = answerDisplayBehavior
        
        self.removeAllAnswersFromQuestionObject()

        for answer in answers {
            let currentAnswer = CDA.createObjectFromEntity(CDEntity.Answer) as Answer
            currentAnswer.value = answer
            
            question.addAnswersObject(currentAnswer)
            
            if answer == correctAnswer {
                question.correctAnswer = currentAnswer
            }

        }
        
        
        // check question is valid for saving
        let validationResult = checkQuestionIsValidForSaving(question)
        
        if !validationResult.isValid {
            
            let errorMessages = validationResult.errorMessages
            
            _infoView = DPInfoView(
                messages: errorMessages,
                type: InfoViewType.Error
            )
            
            _infoView.showInView(self.view)
            
        }
        else {
            delegate.didSaveQuestion(question)
            self.popViewController()
        }
        

    }
    
    @IBAction func addAnswerTapped(sender : AnyObject) {
        
        let answer = tfAddAnswer.text
        let validationResult = checkAnswerIsValidForAdding(answer)
        
        if !validationResult.isValid {
            let errorMessages = validationResult.errorMessages
            
            _infoView = DPInfoView(
                messages: errorMessages,
                type: InfoViewType.Error
            )
            
            _infoView.showInView(self.view)
        }
        else {

            answers.append(tfAddAnswer.text)
            
            tfAddAnswer.text = ""

            tvAnswers.reloadData()
            
            self.view.endEditing(true)
            self.highlightCorrectAnswerRow()
        }
        
    }
    
    @IBAction func cancelTapped(sender : AnyObject) {
        
        _lastConfReq = QuestionConfirmationRequest.QuitWithoutSaving
        
        _confView = DPConfirmationView(
            delegate: self,
            messages: ["Are you sure you want quit? Your changes will be lost."]
        )
        
        _confView.showInView(self.view)
    }
    
    @IBAction func clearQuestionTapped(sender: AnyObject) {
        tfQuestionText.text = ""
    }
  
    @IBAction func clearIntroTextTapped(sender: UIButton) {
        tfIntroductionText.text = ""
    }
    
    
    // MARK: View methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // asign delegates
        // picker views
        pvQuestionType.delegate = self
        pvQuestionType.dataSource = self
        pvMediaType.delegate = self
        pvMediaType.dataSource = self
        pvScaleSize.delegate = self
        pvScaleSize.dataSource = self
        pvAnswerBehaviour.delegate = self
        pvAnswerBehaviour.dataSource = self
        
        // table views
        tvAnswers.delegate = self
        tvAnswers.dataSource = self
    
        
        // textfield views
        tvAnswers.delegate = self
        tfAddAnswer.delegate = self
        tfCountdown.delegate = self
        tfIntroductionText.delegate = self
        tfQuestionText.delegate = self
        tfPauseTime.delegate = self
        
        initViewControls()
    }
    
    
    /*
     *  Initialise media player in view will appear,
     *  allows media player to properly colour the background
     *  of the media preview view. If done in viewDidLoad
     *  background is overwritten with default colour from IB.
     */
    override func viewWillAppear(animated: Bool) {
     
        if isInitialAppearance {
            isInitialAppearance = false
            
            if let existing = question {
                
                // load media
                if let existingAssetURL = existing.assetURL {
                    
                    mediaAssetURLToSave = existingAssetURL
                    
                    let assetURL = NSURL(string: existingAssetURL)
                    
                    self.mediaPlayer = mediaPlayerFact.getPlayer(
                        ApplicationState.Preview,
                        type: existing.mediaType,
                        delegate: self
                    )
                    
                    self.mediaPlayer!.playMedia(assetURL!, onView: uvMediaPreview)
                }
            }
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()

    }

    func popViewController() {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    func getScaleSize() -> Int {
        return scaleSizes[pvScaleSize.selectedRowInComponent(0)].toInt()!
    }
    
    func initViewControls() {
        
        if let existing = question {
            // question already exists
        }
        else {
            // create a new question from core data
            question = CDA.newObjectForEntity(CDEntity.Question) as Question
            
        }
        
        // configure steppers
        stCountdown.maximumValue = 600
        stCountdown.minimumValue = 0
        stCountdown.wraps = true
        stCountdown.autorepeat = true
        
        stPauseTime.maximumValue = 600
        stPauseTime.minimumValue = 0
        stPauseTime.wraps = true
        stPauseTime.autorepeat = true
        
        if let existing = question {
            
            tfCountdown.text = existing.countdown
            tfIntroductionText.text = existing.introText

            // add existing answer strings to answer array
            let existingAnswers = question.answers.array as [Answer]
            existingAnswers.map({ self.answers.append($0.value)})
            
            // set focus point requirement
            if let hasFocusPoint = existing.hasFocusPoint {
                swHasFocusPoint.on = existing.hasFocusPoint.boolValue
            }
            else {
                swHasFocusPoint.on = true
            }
            
            // should record video
            if let shouldRecord = existing.shouldRecord {
                swRecordVideo.on = existing.shouldRecord.boolValue
            }
            
            // set correct answer
            if let savedCorrectAnswer = existing.correctAnswer {
                correctAnswer = savedCorrectAnswer.value
            }
            
            // set question type
            if let questionType = existing.questionType {
                if let i = find(questionTypeData, existing.questionType) {
                    pvQuestionType.selectRow(i, inComponent: 0, animated: true)
                }
            }
            else {
                pvQuestionType.selectRow(0, inComponent: 0, animated: true)
            }
            
            // set scale size
            if let scaleSize = existing.scaleSize {
                if let i = find(scaleSizes, existing.scaleSize.stringValue) {
                    pvScaleSize.selectRow(i, inComponent: 0, animated: true)
                }
            }
            else {
                pvScaleSize.selectRow(0, inComponent: 0, animated: true)
            }
            
            // set question text
            if let questionText = existing.questionText {
                tfQuestionText.text = existing.questionText
            }
            else {
                tfQuestionText.text = existing.questionText
            }

            // set media type
            if let mediaType = existing.mediaType {
                
                mediaTypeToSave = mediaType
                
                if let i = find(mediaTypeData, existing.mediaType) {
                    pvMediaType.selectRow(i, inComponent: 0, animated: true)
                }
            }
            else {
                pvMediaType.selectRow(0, inComponent: 0, animated: true)
            }
            
            // set pause time
            if let pauseTime = existing.pauseTime {
                tfPauseTime.text = pauseTime.stringValue
            }
            else {
                existing.pauseTime = 0
                tfPauseTime.text = "0"
            }
            
            if let countdown = existing.countdown {
                tfCountdown.text = countdown
            }
            else {
                tfCountdown.text = "0"
            }
            
            // set answer behavior
            if let answerDisplayBehavior = existing.answerDisplayBehavior {
                if let i = find(answerBehaviorData, existing.answerDisplayBehavior) {
                    pvAnswerBehaviour.selectRow(i, inComponent: 0, animated: true)
                }
            }
            else {
                pvAnswerBehaviour.selectRow(0, inComponent: 0, animated: true)
            }
            
            // manually call did select row on question type to hide controls
            let selectedRowQuestion = pvQuestionType.selectedRowInComponent(0)
            self.pickerView(pvQuestionType, didSelectRow: selectedRowQuestion, inComponent: 0)
            
            // manually call did select row on answer display behaviour to hide controls
            let selectedRowAnswerBehaviour = pvAnswerBehaviour.selectedRowInComponent(0)
            self.pickerView(pvAnswerBehaviour, didSelectRow: selectedRowAnswerBehaviour, inComponent: 0)

        }
        
        
        // round the preview view
        uvMediaPreview.backgroundColor = colorHelper.colorPrimary2()
        uvMediaPreview.layer.cornerRadius = 20
        uvMediaPreview.layer.masksToBounds = true
        
        // add not available label?
        if question.assetURL == nil {
            var noMediaLabel = UILabel(
                frame: CGRect(
                    x: 0,
                    y: 0,
                    width: 100,
                    height: 50)
            )
            
            noMediaLabel.font = fontHelper.fontPrimary(15)
            noMediaLabel.textColor = UIColor.whiteColor()
            noMediaLabel.text = "Not available"
            noMediaLabel.numberOfLines = 3
            noMediaLabel.adjustsFontSizeToFitWidth = true
            noMediaLabel.lineBreakMode = NSLineBreakMode.ByWordWrapping
            noMediaLabel.textAlignment = NSTextAlignment.Center
            
            screenBounds.positionViewCentral(
                thisView: noMediaLabel,
                inCenterOf: uvMediaPreview
            )
            
            uvMediaPreview.addSubview(noMediaLabel)
        }
        
    }
    
    // MARK: Image picker delegate methods
    func imagePickerControllerDidCancel(picker: UIImagePickerController!) {
        imagePicker.dismissViewControllerAnimated(true, completion: nil)
    }
    
    // MARK: Table view methods
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return answers.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cellID = "CellAnswer"
        
        var cell = tableView.dequeueReusableCellWithIdentifier(cellID) as UITableViewCell
        
        var answer = answers[indexPath.row]
        cell.textLabel.text = answer
        
        return cell
    }
    
    func tableView(tableView: UITableView!, canEditRowAtIndexPath indexPath: NSIndexPath!) -> Bool {
        return true
    }
    
    func tableView(tableView: UITableView!, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath!) {
        
        if editingStyle == UITableViewCellEditingStyle.Delete {
            
            if let table = tableView {
            
                // empty correct answer if answer being deleted 
                // is already assigned as the correct answer
                let selectedAnswer = answers[indexPath!.row]
                
                if let correctAnswer = question.correctAnswer {
                    if correctAnswer.value == selectedAnswer {
                        question.correctAnswer = nil
                    }
                }
                
                // delete the answer from core data
                answers.removeAtIndex(indexPath!.row)
                table.deleteRowsAtIndexPaths([indexPath!], withRowAnimation: UITableViewRowAnimation.Fade)
                
                self.highlightCorrectAnswerRow()
                
            }
            
        }
        
    }
    
    func tableView(tableView: UITableView, didEndEditingRowAtIndexPath indexPath: NSIndexPath) {
        
        self.highlightCorrectAnswerRow()
        
    }
    
    func tableView(tableView: UITableView!, didSelectRowAtIndexPath indexPath: NSIndexPath!) {
        
        var cell = tvAnswers.cellForRowAtIndexPath(indexPath)
        cell!.contentView.backgroundColor = colorHelper.colorPrimary3()
        
        self.correctAnswerIndexPath = indexPath
        
        correctAnswer = self.answers[indexPath.row]
        
    }
    
    // test for table view finishing loading
    //
    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        
        var lastIndexPath:NSIndexPath = tvAnswers.indexPathsForVisibleRows()!.last as NSIndexPath
        
        if (indexPath.row == lastIndexPath.row) {
            self.highlightCorrectAnswerRow()
        }
        
    }
    
    func highlightCorrectAnswerRow() {
        
        // test if a correct answer still exits
        // if so highlight it
        if let correctAnswer = question.correctAnswer {
            
            let correctAnswerVal = correctAnswer.value
            
            var answerIndexPath:NSIndexPath!
            
            for(var i = 0; i < answers.count; i++) {
                
                if answers[i] == correctAnswerVal {
                    answerIndexPath = NSIndexPath(forItem: i, inSection: 0)
                }
                
            }
            
            tvAnswers.selectRowAtIndexPath(answerIndexPath, animated: false, scrollPosition: UITableViewScrollPosition.None)
            
        }
        
    }
    
    // MARK: Picker view methods
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        var componentCount:Int = 1
    
        return componentCount
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        var rowCount:Int = 0
        
        // pvQuestionType
        if pickerView.isEqual(pvQuestionType) {
            rowCount = questionTypeData.count
        }
        
        // pvMediaType
        if pickerView.isEqual(pvMediaType) {
            rowCount = mediaTypeData.count
        }
        
        // pvScaleSize
        if pickerView.isEqual(pvScaleSize) {
            rowCount = scaleSizes.count
        }
        
        // pvAnswerBehaviour
        if pickerView.isEqual(pvAnswerBehaviour) {
            rowCount = answerBehaviorData.count
        }
        
        return rowCount
    }
    
    func pickerView(pickerView: UIPickerView!, titleForRow row: Int, forComponent component: Int) -> String! {
        var stringForRow:String = ""
        
        // pvQuestionType
        if pickerView.isEqual(pvQuestionType) {
            stringForRow = questionTypeData[row]
        }
        
        // pvMediaType
        if pickerView.isEqual(pvMediaType) {
            stringForRow = mediaTypeData[row]
        }
        
        // pvScaleSize
        if pickerView.isEqual(pvScaleSize) {
            stringForRow = scaleSizes[row]
        }
        
        // pvAnswerBehavior
        if pickerView.isEqual(pvAnswerBehaviour) {
            stringForRow = answerBehaviorData[row]
        }
        
        return stringForRow
    }
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        // hide form controls based on question type
        if pickerView.isEqual(pvQuestionType) {
            let questionType = questionTypeData[row]
            
            if questionType != "Custom" {
                hideCustomAnswerControls(true)
            }
            else {
                hideCustomAnswerControls(false)
            }
            
            switch(questionType) {
                case "Scale":
                    pvScaleSize.hidden = false
                    lvScaleSize.hidden = false
                    generateScaleAnswers()
                
                default:
                    pvScaleSize.hidden = true
                    lvScaleSize.hidden = true
            }
            
        }
        
        if pickerView.isEqual(pvScaleSize) {
            generateScaleAnswers()
        }
        
        // hide form controls based on answer display behaviour type
        if pickerView.isEqual(pvAnswerBehaviour) {
            let answerDisplayBehavior = answerBehaviorData[row]
            
            switch(answerDisplayBehavior) {
                case "Pause for interval":
                    lvPauseTime.hidden = false
                    tfPauseTime.hidden = false
                    stPauseTime.hidden = false
                default:
                    lvPauseTime.hidden = true
                    tfPauseTime.hidden = true
                    stPauseTime.hidden = true
            }
        }
        
    }
    
    // MARK: UITextFieldDelegate
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        
        // sanity check text fields on hide keyboard
        if textField.isEqual(tfPauseTime) {
            if(self.tfPauseTime.text == "") {
                tfPauseTime.text = "0"
            }
        }
        
        textField.resignFirstResponder()
        return true
    }
    
    // MARK: Core data methods
    func populateAnswers() {
        if let unwrapped = question {
            let answerObjs = unwrapped.answers.array as [Answer]
            answerObjs.map({self.answers.append($0.value)})
        }
    }
    
    // MARK: MediaImporterDelegate methods
    func didImportMedia(url: NSURL) {
        
        // show the spinner with loading message
        self.showSpinner("Finishing import")
        
        mediaTypeToSave = mediaTypeData[pvMediaType.selectedRowInComponent(0)]
        mediaAssetURLToSave = url.absoluteString!

        self.getFilenameForMedia(mediaTypeToSave, assetURL: mediaAssetURLToSave)
        
        self.mediaPlayer = mediaPlayerFact.getPlayer(
            ApplicationState.Preview,
            type: mediaTypeToSave,
            delegate: self
        )
        
        self.mediaPlayer!.playMedia(url, onView: uvMediaPreview)
        
    }
    
    // MARK: MediaPlayerFinishedDelegate
    func didFinishPlayingMedia() {
        // no implementation
    }
    
    func generateScaleAnswers() {
        
        let questionScaleSize = question.scaleSize.integerValue
        let pickerScaleSize = self.getScaleSize()
        
        if pickerScaleSize != questionScaleSize {
            
            // remove all existing answers
            self.removeAllAnswers()
            
            // add scale answers
            for var i = 1; i < pickerScaleSize + 1; i++ {
                /*
                var answer = CDA.newObjectForEntity(CDEntity.Answer) as Answer
                answer.value = String(i)
                question.addAnswersObject(answer)
                */
                answers.append(String(i))
            }
            
            question.scaleSize = pickerScaleSize
        
            tvAnswers.reloadData()
        }

    }
    
    // remove all existing answers
    func removeAllAnswers() {
        self.answers = []
    }
    
    func removeAllAnswersFromQuestionObject() {
        for answer in question.answers.array {
            CDA.deleteObject(answer as NSManagedObject)
        }
    }
    
    // MARK: Get filename
    func getFilenameForMedia(mediaType:String, assetURL:String) {
        
        self.filenameGenerator = MediaFilenameGeneratorFactory().getGenerator(
            type: mediaType,
            delegate: self
        )
        
        let assetURL = NSURL(string: assetURL)
        filenameGenerator.getFilename(assetURL!)
    }
    
    // MARK: MediaFilenameGeneratorDelegate
    func didGenerateFilenameForAssetURL(response: MediaFilenameResponse) {
        if(response.status == MediaFilenameGeneratorStatus.DidGenerateFilename) {
            question.filename = response.filename
            self.removeSpinner()
        }
    }
    
    // MARK: Spinner
    func showSpinner(message:String) {
        self.view.userInteractionEnabled = false
        self.navigationItem.leftBarButtonItem!.enabled = false
        self.navigationItem.rightBarButtonItem!.enabled = false
        _spinner = DPSpinner(message: message)
        _spinner.showInView(self.view)
    }
    
    func removeSpinner() {
        self.view.userInteractionEnabled = true
        self.navigationItem.leftBarButtonItem!.enabled = true
        self.navigationItem.rightBarButtonItem!.enabled = true
        _spinner.remove()
    }
    
    // MARK: Toggle custom answer controls
    func hideCustomAnswerControls(isHidden:Bool) {
        btEditCustomAnswer.hidden = isHidden
        lvCustomAnswer.hidden = isHidden
    }
    
    // MARK: CustomAnswerPresenterDelegate
    func didReturnAnswerPresenter(presenter: CustomAnswerPresenter) {
        question.customAnswerPresenter = presenter
    }
    
    // MARK: PrepareForSegue
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject!) {
        if (segue.identifier == "EditAnswerPresenter") {
            
            let answerPresenterEditorVC = segue.destinationViewController as AnswerPresenterEditorViewController
            
            if let existingPresenter = question.customAnswerPresenter {
                answerPresenterEditorVC.existingPresenter = existingPresenter
            }
            
            answerPresenterEditorVC.delegate = self
            
        }
    }
    
    // MARK: Validate question
    func checkQuestionIsValidForSaving(question:Question) -> ValidationResult {
        
        var errors:[String] = [];
        var isValid = true
        
        let questionTextLimit = 150
        let questionText = tfQuestionText.text
        let questionTextCount = countElements(questionText)
        let answerCount = answers.count
        let questionType = question.questionType
        let assetURL = question.assetURL
        let scaleSize = question.scaleSize
        let introTextLimit = 200
        let introTextCount = countElements(tfIntroductionText.text)
        
        // make sure the introduction text isn't longer than 200 characters
        if introTextCount > introTextLimit {
            errors.append("Introduction can not be longer than \(introTextLimit) characters long.")
            isValid = false
        }
        
        // make sure question text count is less than the limit
        if (questionTextCount > questionTextLimit) {
            errors.append("Question text can't be longer than 150 characters.")
            errors.append("Current question is \(questionTextCount) characters long, remove \(questionTextCount - questionTextLimit).")
            isValid = false
        }
        
        // question is set to require answers but non have been added
        if (questionType != "Not Required" && answerCount == 0) {
            errors.append("Answers are required for this question but non have been added.")
            isValid = false
        }
        
        // check that media has been imported
        if (assetURL == nil || assetURL == "") {
            errors.append("No media has been imported for this question.")
            isValid = false
        }
        
        // if using scale, have the correct amount of answers been added
        if (questionType == "Scale" && scaleSize != answerCount) {
            
            errors.append("Scale size \(scaleSize) selected, but \(answerCount) answers added.")
            errors.append("Please add the correct amount of answers for scale size.")
            isValid = false
        }
        
        // no more than 8 answers to be added for buttons
        if questionType == "Buttons" && (answerCount > 8) {
            errors.append("No more than 8 answers can be added when using buttons.")
            isValid = false
        }

        // no more than 5 answers to be added for sliders
        if questionType == "Slider" && (answerCount > 5) {
            errors.append("No more than 8 answers can be added when using a slider.")
            isValid = false
        }
        
        // if custom is selected as answer type make sure a custom answer
        // presenter actually exists and if it does, that the correct amount
        // of answers are configured for it
        if questionType == "Custom" {
            if let existingPresenter = question.customAnswerPresenter {
                let presenterOptionCount = existingPresenter.options.count
                
                if answerCount != presenterOptionCount {
                    errors.append("A custom answer type with \(presenterOptionCount) has been configured.")
                    errors.append("However \(answerCount) answers have been setup. Supply only \(presenterOptionCount) answers.")
                    isValid = false
                }
            }
            else {
                errors.append("Custom answer type is selected but has not been configured.")
                isValid = false
            }
        }

        
        
        return ValidationResult(isValid: isValid, errorMessages: errors)
    }
    
    // MARK: Validate answer
    func checkAnswerIsValidForAdding(answer:String) -> ValidationResult {
        
        var errors:[String] = [];
        var isValid = true
        
        let buttonAnswerLimit = 8
        let sliderAnswerLimit = 5
        let answerCharacterLimit = 40

        let answerCharacterCount = countElements(answer)
        let answerCount = answers.count
        let questionType = questionTypeData[pvQuestionType.selectedRowInComponent(0)]
        
        // make sure answer isn't empty
        if answerCharacterCount == 0 {
            errors.append("You can not add an empty answer.")
            isValid = false
        }
        
        // make sure answer character count isn't massively long
        if answerCharacterCount > answerCharacterLimit {
            errors.append("Answers can not be longer than \(answerCharacterLimit) characters long.")
            isValid = false
        }
        
        // no more than 8 answers to be added for buttons
        if questionType == "Buttons" && answerCount >= buttonAnswerLimit {
            errors.append("No more than \(buttonAnswerLimit) answers can be added when using buttons.")
            isValid = false
        }
        
        // no more than 10 answers when using a slider
        if questionType == "Slider" && answerCount >= sliderAnswerLimit {
            errors.append("No more than \(sliderAnswerLimit) answers can be added when using a slider.")
            isValid = false
        }
        
        return ValidationResult(isValid: isValid, errorMessages: errors)
    }
    
    // MARK: DPConfirmationViewDelegate
    func didPressOk() {
        
        // check a confirmation request exists
        if let lastConfReq = _lastConfReq {
            
            switch(lastConfReq) {
            case .ClearAllAnswers:
                self.clearAllAnswers()
                
            case .QuitWithoutSaving:
                self.popViewController()
            }
            
        }
        
        removeConfirmationView()
    }
    
    func didPressCancel() {
        removeConfirmationView()
    }
    
    func removeConfirmationView() {
        _confView.removeFromSuperView()
    }
    
    // MARK: Functions requiring confirmation
    func clearAllAnswers() {
        self.removeAllAnswers()
        tvAnswers.reloadData()
    }
    
    deinit {
        println("Edit question dealloced")
    }
    
}
