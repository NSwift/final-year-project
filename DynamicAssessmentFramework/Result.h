//
//  Result.h
//  DynamicAssessmentFramework
//
//  Created by Daniel Parkin on 14/02/2015.
//  Copyright (c) 2015 DanielParkin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Answer, Assessment, Participant, Question;

@interface Result : NSManagedObject

@property (nonatomic, retain) NSString * recordingUrl;
@property (nonatomic, retain) NSNumber * mediaReplays;
@property (nonatomic, retain) NSNumber * responseTime;
@property (nonatomic, retain) Answer *answer;
@property (nonatomic, retain) Assessment *assessment;
@property (nonatomic, retain) Participant *participant;
@property (nonatomic, retain) Question *question;

@end
