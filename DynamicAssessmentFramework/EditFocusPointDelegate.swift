//
//  EditFocusPointDelegate.swift
//  DynamicAssessmentFramework
//
//  Created by Daniel Parkin on 17/02/2015.
//  Copyright (c) 2015 DanielParkin. All rights reserved.
//

protocol EditFocusPointDelegate : class {
    func didSaveFocusPoint(focusPoint:FocusPoint)
    func didDiscardChanges()
}
