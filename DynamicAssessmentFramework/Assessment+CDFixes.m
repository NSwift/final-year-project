//
//  Assessment+CDFixes.m
//  DynamicAssessmentFramework
//
//  Created by Daniel Parkin on 17/10/2014.
//  Copyright (c) 2014 DanielParkin. All rights reserved.
//

#import "Assessment+CDFixes.h"

@implementation Assessment (CDFixes)

- (void)addQuestionsObject:(Question *)value {
    NSMutableOrderedSet* tempSet = [NSMutableOrderedSet orderedSetWithOrderedSet:self.questions];
    [tempSet addObject:value];
    self.questions = tempSet;
}

@end
