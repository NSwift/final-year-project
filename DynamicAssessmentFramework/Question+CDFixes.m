//
//  Question+CDFixes.m
//  DynamicAssessmentFramework
//
//  Created by Daniel Parkin on 17/10/2014.
//  Copyright (c) 2014 DanielParkin. All rights reserved.
//

#import "Question+CDFixes.h"

@implementation Question (CDFixes)

- (void)addAnswersObject:(Answer *)value {
    NSMutableOrderedSet* tempSet = [NSMutableOrderedSet orderedSetWithOrderedSet:self.answers];
    [tempSet addObject:value];
    self.answers = tempSet;
}

@end
