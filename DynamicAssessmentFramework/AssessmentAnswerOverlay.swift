//
//  AssessmentAnswerOverlay.swift
//  DynamicAssessmentFramework
//
//  Created by Daniel Parkin on 10/01/2015.
//  Copyright (c) 2015 DanielParkin. All rights reserved.
//

import UIKit

class AssessmentAnswerOverlay {
    
    let screenBounds = ScreenBounds()
    let CDA = CoreDataAccess()
    
    func overlayAnswerOnView(view:UIView, controls:[NSObject]) {
        
        var xOffset:CGFloat = 100
        
        for control in controls {

            if control is AnswerProvidingControlProtocol {
                
                let rootView:UIView = view
                let answerProvider = control as AnswerProvidingControlProtocol
                let apView = answerProvider.getView()
                let apFrame = answerProvider.getFrame()
                let answer = answerProvider.provideAnswer()
                
                let count = CDA.resultRepository().getResultCountForAnswer(
                    answer,
                    ascending: true
                )
                
                let testLabel = UILabel(
                    frame: CGRect(
                        x: 0,
                        y: 0,
                        width: 50,
                        height: 50)
                )
                
                testLabel.text = String(count)
                testLabel.font = UIFont.systemFontOfSize(30)
                testLabel.textColor = UIColor.whiteColor()
                testLabel.numberOfLines = 0
                testLabel.textAlignment = NSTextAlignment.Center
                
                // color answer provider background
                //answerProvider.getView().backgroundColor = UIColor.orangeColor()
            
                // position the test label
                // get the actual window coordinates of the answer provider control
                // not relative to it's parent view
                let apCoords = apView.convertRect(apView.bounds, toView: rootView)
                
                // calculate Y coordinate
                let apYCoord:CGFloat = apCoords.origin.y
                let apHeight:CGFloat = apCoords.size.height
                let y:CGFloat = (apYCoord + apHeight)
                testLabel.frame.origin.y = y
                
                // calculate X coordinate
                let apXCoord = apCoords.origin.x
                let apWidth = apCoords.size.width
                let testLabelWidth = testLabel.frame.width
                let x = (apXCoord + (apWidth / 2)) - testLabelWidth / 2
                testLabel.frame.origin.x = x

                rootView.addSubview(testLabel)
            }
        }
        
    }
    
}
