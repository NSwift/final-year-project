//
//  AnswerPresenter.swift
//  QuestionsTests
//
//  Created by Daniel Parkin on 19/08/2014.
//  Copyright (c) 2014 Echoyo. All rights reserved.
//

import Foundation
import UIKit

class AnswerPresenterBlank:NSObject, AnswerPresentationProtocol {
    
    weak var selectedAnswer:Answer?
    weak var question:Question?
    weak var delegate:AnswerPresenterDelegate?
    var controls:[NSObject] = []
    
    private var _appState:ApplicationState!

    init(appState:ApplicationState, delegate:AnswerPresenterDelegate) {
        self._appState = appState
        self.delegate = delegate
    }
    
    func displayOnView(view:UIView) {
        let status = ActResponseStatus.CompletedWithoutAnswer
        let response = ActResponse(status: status)
        
        if _appState != ApplicationState.Review {
            delegate?.didCompleteWithoutAnswer()    
        }
    }
    
    func valueFromControl(sender:AnyObject) {
        // no implementation
    }
    
}