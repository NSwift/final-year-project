//
//  AnswerPresenterFactory.swift
//  QuestionsTests
//
//  Created by Daniel Parkin on 19/08/2014.
//  Copyright (c) 2014 Echoyo. All rights reserved.
//

class MediaFilenameGeneratorFactory {
    
    func getGenerator(#type:String, delegate:MediaFilenameGeneratorDelegate) -> MediaFilenameGeneratorProtocol {
        
        switch(type) {
        case "Image", "Video":
            return MediaFilenameGeneratorImage(delegate: delegate)
        case "Audio":
            return MediaFilenameGeneratorAudio(delegate: delegate)
        default:
            return MediaFilenameGeneratorImage(delegate: delegate)
        }
        
    }
    
}