//
//  SystemAssessmentName.swift
//  DynamicAssessmentFramework
//
//  Created by Daniel Parkin on 16/02/2015.
//  Copyright (c) 2015 DanielParkin. All rights reserved.
//

enum SystemAssessmentName:String {
    case Recovered = "Recovered Assessment"
}