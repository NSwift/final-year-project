//
//  MediaImporterImage.swift
//  DynamicAssessmentFramework
//
//  Created by Daniel Parkin on 27/09/2014.
//  Copyright (c) 2014 DanielParkin. All rights reserved.
//

import UIKit
import AssetsLibrary

class MediaImporterImage: NSObject, UIImagePickerControllerDelegate, UINavigationControllerDelegate, MediaImporterProtocol {
    
    // MARK: Properties
    var mediaURL:NSURL = NSURL()
    weak var delegate:MediaImporterDelegate?
    let imagePicker = UIImagePickerController()
    
    // MARK: Init
    override init() {
        super.init()
        self.imagePicker.delegate = self
        self.imagePicker.sourceType = UIImagePickerControllerSourceType.PhotoLibrary
        self.imagePicker.allowsEditing = true
    }
    
    // MARK: Media importer protocol methods
    func controller() -> UIViewController {
        return imagePicker
    }
    
    // MARK: Image picker delegate methods
    func imagePickerController(picker: UIImagePickerController!, didFinishPickingMediaWithInfo info: NSDictionary!) {
        
        let chosenImage = info[UIImagePickerControllerOriginalImage] as UIImage

        let mediaURL = info[UIImagePickerControllerReferenceURL] as NSURL
        
        self.getFilename(mediaURL)
        
        delegate!.didImportMedia(mediaURL)
        
        imagePicker.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(picker: UIImagePickerController!) {
        imagePicker.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func getFilename(url: NSURL) {
        // Display image
        var assetLibrary: ALAssetsLibrary = ALAssetsLibrary()
        
        assetLibrary.assetForURL(url, resultBlock: {
            (asset: ALAsset!) in
            autoreleasepool {
                
                if asset != nil {
                    
                    var assetRep: ALAssetRepresentation = asset.defaultRepresentation()
                    var filename = assetRep.filename()

                }
            }
            }, failureBlock: {
                (error: NSError!) in
                
                println("ALAsset error.")
            }
        )
    }
   
}
