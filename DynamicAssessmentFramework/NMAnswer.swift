//
//  Answer.swift
//  QuestionsTests
//
//  Created by Daniel Parkin on 19/08/2014.
//  Copyright (c) 2014 Echoyo. All rights reserved.
//


import Foundation

class NMAnswer:NSObject {
    var value:String?
    
    init(value:String?) {
        self.value = value
    }
}
