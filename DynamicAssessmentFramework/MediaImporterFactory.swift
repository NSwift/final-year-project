//
//  AnswerPresenterFactory.swift
//  QuestionsTests
//
//  Created by Daniel Parkin on 19/08/2014.
//  Copyright (c) 2014 Echoyo. All rights reserved.
//

import Foundation

class MediaImporterFactory {
    
    func getImporter(type:String) -> MediaImporterProtocol {
        
        switch(type) {
        case "Audio":
            return MediaImporterAudio()
        case "Image":
            return MediaImporterImage()
        case "Video":
            return MediaImporterVideo()
        default:
            return MediaImporterAudio()
        }
        
    }
    
}