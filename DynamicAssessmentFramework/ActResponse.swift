//
//  ActResponse.swift
//  DynamicAssessmentFramework
//
//  Created by Daniel Parkin on 20/08/2014.
//  Copyright (c) 2014 Echoyo. All rights reserved.
//

import Foundation

class ActResponse:NSObject {
    var status:ActResponseStatus
    var bundle:AnswerBundle?
    
    init(status:ActResponseStatus) {
        self.status = status
    }
    
    init(status:ActResponseStatus, bundle:AnswerBundle?) {
        self.status = status
        self.bundle = bundle
    }
}