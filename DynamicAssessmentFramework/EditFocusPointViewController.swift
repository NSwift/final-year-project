//
//  FocusPointViewController.swift
//  DynamicAssessmentFramework
//
//  Created by Daniel Parkin on 26/09/2014.
//  Copyright (c) 2014 DanielParkin. All rights reserved.
//

import UIKit
import AssetsLibrary

class EditFocusPointViewController: UIViewController, UIPickerViewDataSource, UIPickerViewDelegate, MediaImporterDelegate, MediaPlayerDelegate, UIGestureRecognizerDelegate, DPConfirmationViewDelegate {

    // MARK: Properties
    var assessment:Assessment!
    var focusPoint:FocusPoint!
    weak var delegate:EditFocusPointDelegate!
    
    private var _confView:DPConfirmationView!
    private var _infoView:DPInfoView!
    private var _mediaPlayer:MediaPlayerImageNoCompression!
    private var _xPoint:CGFloat = 0
    private var _yPoint:CGFloat = 0
    private var _width:CGFloat = 0
    private var _height:CGFloat = 0
    private var _scale:CGFloat = 0
    private var _rotation:CGFloat = 0
    private var _assetURL:NSURL!
    private var _isInitialLayout:Bool = true
    private var _viewsLayedOut:Int = 0
    
    private let _fontHelper = FontHelper()
    private let _colorHelper = ColorHelper()
    private let _screenBounds = ScreenBounds()
    private let _mediaImporter = MediaImporterImage()
    private let _CDA = CoreDataAccess()

    // MARK: Init
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self._mediaImporter.delegate = self
        self._mediaPlayer = MediaPlayerImageNoCompression(appState: ApplicationState.Review, delegate: self)
    }
    
    // MARK: IBOutlets
    @IBOutlet weak var lbRotationValue: UILabel!
    @IBOutlet weak var lbScaleValue: UILabel!
    @IBOutlet weak var lbTouchCoords: UILabel!
    @IBOutlet weak var pvColorSelector: UIPickerView!
    @IBOutlet weak var ivFocusPoint: UIImageView!

    @IBOutlet weak var uvWhiteView: UIView!
    @IBOutlet weak var lbAlignment: UILabel!
    @IBOutlet weak var lbImage: UILabel!
    @IBOutlet weak var lbBackgroundColor: UILabel!
    
    @IBOutlet weak var lbIconAlignment: UILabel!
    @IBOutlet weak var lbIconImage: UILabel!
    @IBOutlet weak var lbIconBackgroundColor: UILabel!

    
    @IBOutlet weak var uvMediaPreview: UIView!
    
    @IBAction func handlePan(recognizer:UIPanGestureRecognizer) {
        
        self.view.bringSubviewToFront(recognizer.view!)
        var translation = recognizer.translationInView(self.view)
        
        _xPoint = recognizer.view!.center.x + translation.x
        _yPoint = recognizer.view!.center.y + translation.y
        
        recognizer.view!.center = CGPointMake(_xPoint, _yPoint)
        recognizer.setTranslation(CGPointZero, inView: self.view)
        
    }
    
    @IBAction func handlePinch(recognizer : UIPinchGestureRecognizer) {
        
        recognizer.view!.transform = CGAffineTransformScale(
            recognizer.view!.transform,
            recognizer.scale,
            recognizer.scale
        )
        
        _scale = recognizer.scale
        self.updateHeightWidth()
    
        recognizer.scale = 1
    }
    
    @IBAction func handleRotate(recognizer : UIRotationGestureRecognizer) {
        
        /*
        recognizer.view!.transform = CGAffineTransformRotate(
            recognizer.view!.transform,
            recognizer.rotation
        )
        
        if recognizer.rotation != 0 {
            _rotation = recognizer.rotation
        }
        
        recognizer.rotation = 0
        */
    }
    
    @IBAction func importImageTapped(sender: AnyObject) {
        self.presentViewController(
            _mediaImporter.controller(),
            animated: true,
            completion: nil
        )
    }
    
    @IBAction func resetScaleTapped(sender: AnyObject) {
    }
    
    @IBAction func resetRotationTapped(sender: AnyObject) {
    }
    
    @IBAction func discardIconTapped(sender: AnyObject) {
        self.doDiscard()
    }
    
    @IBAction func discardTapped(sender: AnyObject) {
        self.doDiscard()
    }
    
    @IBAction func saveIconTapped(sender: AnyObject) {
        self.save()
    }
    
    @IBAction func saveTapped(sender: AnyObject) {
        self.save()
    }
    
    @IBAction func alignTopCentreTapped(sender: AnyObject) {
        //ivFocusPoint.center = _screenBounds.viewTopCentre(ivFocusPoint)
        uvMediaPreview.center = _screenBounds.viewTopCentre(uvMediaPreview)
        self.updateXYPoints()
    }
    
    @IBAction func alignMiddleCentreTapped(sender: AnyObject) {
        //ivFocusPoint.center = _screenBounds.viewCentre(ivFocusPoint)
        uvMediaPreview.center = _screenBounds.viewCentre(uvMediaPreview)
        self.updateXYPoints()
    }
    
    @IBAction func alignBottomCentreTapped(sender: AnyObject) {
        //ivFocusPoint.center = _screenBounds.viewBottomCentre(ivFocusPoint)
        uvMediaPreview.center = _screenBounds.viewBottomCentre(uvMediaPreview)
        self.updateXYPoints()
    }
    
    // MARK: View methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        
        pvColorSelector.dataSource = self
        pvColorSelector.delegate = self
        ivFocusPoint.backgroundColor = UIColor.clearColor()
    
        self.configureView()
    }
    
    override func viewDidAppear(animated: Bool) {
        
    }
    
    override func viewWillAppear(animated: Bool) {

    }

    override func viewDidLayoutSubviews() {
  
        let viewFrame = CGRect(
            x: CGFloat(_xPoint),
            y: CGFloat(_yPoint),
            width: CGFloat(_width),
            height: CGFloat(_height)
        )

        self.uvMediaPreview.frame = viewFrame
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func configureView() {
        
        // round the edges of uvWhiteView
        // round the corners of the view
        uvWhiteView.layer.cornerRadius = 20
        uvWhiteView.layer.masksToBounds = true
        
        // if a focus point was passed in
        if let focusPoint = self.focusPoint {
            
            _xPoint = CGFloat(focusPoint.x)
            _yPoint = CGFloat(focusPoint.y)
            _height = CGFloat(focusPoint.height)
            _width = CGFloat(focusPoint.width)
            _scale = CGFloat(focusPoint.scale)
            _rotation = CGFloat(focusPoint.rotation)
            
            self.setViewBackgroundColor(focusPoint.backgroundColor)
            uvWhiteView.backgroundColor =  _colorHelper.colorNamed(focusPoint.backgroundColor)
            
            let colorIndex = find(_colorHelper.availableColorNames(), focusPoint.backgroundColor)
            
            pvColorSelector.selectRow(colorIndex!, inComponent: 0, animated: false)
            
            let viewFrame = CGRect(
                x: CGFloat(focusPoint.x),
                y: CGFloat(focusPoint.y),
                width: CGFloat(focusPoint.width),
                height: CGFloat(focusPoint.height)
            )
            
            // if the background colour is black show the white background view
            if focusPoint.backgroundColor == "Black" {
                uvWhiteView.hidden = false
                uvWhiteView.backgroundColor = UIColor.whiteColor()
            }
            
            //self.ivFocusPoint.frame = viewFrame
            self.uvMediaPreview.translatesAutoresizingMaskIntoConstraints()
            self.uvMediaPreview.frame = viewFrame
            
            _assetURL = NSURL(string: focusPoint.assetURL)
            self.loadMedia(_assetURL)
            
            // do rotation
            self.ivFocusPoint.transform = CGAffineTransformRotate(
                self.ivFocusPoint.transform,
                CGFloat(focusPoint.rotation)
            )
            
        }
        else {
            
            let colorIndex = find(_colorHelper.availableColorNames(), "White")
            
            pvColorSelector.selectRow(colorIndex!, inComponent: 0, animated: false)
            
            
            // centre the image view in the centre of the screen
            _screenBounds.positionViewCentral(
                thisView: self.ivFocusPoint,
                inCenterOf: self.view
            )
            
            self.updateXYPoints()
            self.updateHeightWidth()
        }
        
    }
    
    // MARK: UIPickerView methods
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return _colorHelper.availableColors.count
    }
    
    func pickerView(pickerView: UIPickerView!, titleForRow row: Int, forComponent component: Int) -> String! {
        return _colorHelper.availableColorNames()[row]
    }
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        let colorName = _colorHelper.availableColorNames()[row]
        self.setViewBackgroundColor(colorName)
        
        if colorName == "Black" {
            uvWhiteView.backgroundColor = UIColor.whiteColor()
            lbAlignment.textColor = UIColor.whiteColor()
            lbImage.textColor = UIColor.whiteColor()
            lbBackgroundColor.textColor = UIColor.whiteColor()
            lbIconAlignment.textColor = UIColor.whiteColor()
            lbIconImage.textColor = UIColor.whiteColor()
            lbBackgroundColor.textColor = UIColor.whiteColor()
            lbIconBackgroundColor.textColor = UIColor.whiteColor()
        }
        else {
            uvWhiteView.backgroundColor = _colorHelper.colorNamed(colorName)
            lbAlignment.textColor = UIColor.blackColor()
            lbImage.textColor = UIColor.blackColor()
            lbBackgroundColor.textColor = UIColor.blackColor()
            lbIconAlignment.textColor = UIColor.blackColor()
            lbIconImage.textColor = UIColor.blackColor()
            lbBackgroundColor.textColor = UIColor.blackColor()
            lbIconBackgroundColor.textColor = UIColor.blackColor()
        }
    }
    
    func pickerView(pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusingView view: UIView!) -> UIView {
        
        let pickerLabel = UILabel()
        let colorName = _colorHelper.availableColorNames()[row]

        pickerLabel.frame.size.height = CGFloat(50)
        pickerLabel.font = _fontHelper.fontPrimary(20)
        
        pickerLabel.text = colorName
        pickerLabel.textAlignment = NSTextAlignment.Center

        return pickerLabel

    }

    
    // MARK: MediaImporterDelegate methods
    func didImportMedia(url: NSURL) {
        _assetURL = url
        self.loadMedia(_assetURL)
    }
    
    func loadMedia(url:NSURL) {
        //self.ivFocusPoint.image = nil
        _mediaPlayer.playMedia(url, onView: uvMediaPreview)
        ivFocusPoint.setNeedsDisplay()
    }
    
    // MARK: MediaPlayerProtocol
    func didFinishPlayingMedia() {
        // no implementation
    }
    
    // MARK: UIGestureRecogizerDelegate methods
    func gestureRecognizer(UIGestureRecognizer,
        shouldRecognizeSimultaneouslyWithGestureRecognizer:UIGestureRecognizer) -> Bool {
            return true
    }
    
    func updateXYPoints() {
        //_xPoint = ivFocusPoint.frame.origin.x
        //_yPoint = ivFocusPoint.frame.origin.y
        
        _xPoint = uvMediaPreview.frame.origin.x
        _yPoint = uvMediaPreview.frame.origin.y
    }
    
    func updateHeightWidth() {
        //_height = ivFocusPoint.frame.height
        //_width = ivFocusPoint.frame.width
        
        _height = uvMediaPreview.frame.height
        _width = uvMediaPreview.frame.width
    }
    
    func setViewBackgroundColor(color:String) {
        view.backgroundColor = _colorHelper.colorNamed(color)
    }
    
    func popView() {
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    func save() {
        
        let colorRow = pvColorSelector.selectedRowInComponent(0)
        let backgroundColor = _colorHelper.availableColorNames()[colorRow]
        
        var fp:FocusPoint!
        
        if let focusPoint = self.focusPoint {
            // edit existing
            fp = self.focusPoint
        }
        else {
            // create new
            fp = _CDA.newObjectForEntity(CDEntity.FocusPoint) as FocusPoint
        }

        // validate focus point for saving
        var validationResult = self.focusPointIsValidForSaving(fp)
        
        if validationResult.isValid {
            
            // configure the focus point object before passing back
            fp.backgroundColor = backgroundColor
            fp.height = _height
            fp.width = _width
            fp.scale = _scale
            fp.rotation = _rotation
            fp.x = _xPoint
            fp.y = _yPoint
            fp.assetURL = _assetURL.absoluteString
            
            delegate.didSaveFocusPoint(fp)
            self.popView()
        }
        else {
            pushErrorView(validationResult)
        }
        
    }
    
    // MARK: Push views
    func pushErrorView(validationResult:ValidationResult) {
        let errorMessages = validationResult.errorMessages
        _infoView = DPInfoView(messages: errorMessages, type:InfoViewType.Error)
        _infoView.showInView(self.view)
    }
    
    func focusPointIsValidForSaving(focusPoint:FocusPoint) -> ValidationResult {
        
        var errors:[String] = [];
        var isValid = true
        
        let errorAsset = "You must configure an image for the focus point."
        
        if let assetUrl = _assetURL {
            
            if assetUrl.absoluteString == "" {
                isValid = false
                errors.append(errorAsset)
            }
            
        }
        else {
            isValid = false
            errors.append(errorAsset)
        }
    
        return ValidationResult(isValid: isValid, errorMessages: errors)
    }
    
    func doDiscard() {
        
        // show confirmation message
        let infoMessage = ["Are you sure you want to discard your changes?"]
        _confView = DPConfirmationView(delegate:self, messages: infoMessage)
        
        _confView.showInView(self.view)
    }
    
    // DPConfirmationViewDelegate
    func didPressOk() {
        delegate.didDiscardChanges()
        self.popView()
    }
    
    func didPressCancel() {
        _confView.removeFromSuperView()
    }

    deinit {
        println("Edit focus point deinit")
    }
}
