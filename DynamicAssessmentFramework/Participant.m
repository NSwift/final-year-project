//
//  Participant.m
//  DynamicAssessmentFramework
//
//  Created by Daniel Parkin on 09/01/2015.
//  Copyright (c) 2015 DanielParkin. All rights reserved.
//

#import "Participant.h"
#import "Result.h"


@implementation Participant

@dynamic id;
@dynamic dateOfBirth;
@dynamic results;

@end
