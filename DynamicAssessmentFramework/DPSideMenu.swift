//
//  DPSideMenu.swift
//  DynamicAssessmentFramework
//
//  Created by Daniel Parkin on 12/01/2015.
//  Copyright (c) 2015 DanielParkin. All rights reserved.
//


import UIKit

class DPSideMenu : NSObject {
    
    var view:UIView!
    var tvMenuTable:UITableView! = nil
    var menuItems:[DPSideMenuItem] = []
    var isExpanded:Bool = false
    var yPosAfterLastItem:CGFloat = 0
    var tableTitle:String?
    weak var delegate:DPSideMenuDelegate?
    
    var menuHeight:CGFloat = 0
    var menuXPos:CGFloat = 0
    
    let tableTitleHeight:CGFloat = 50
    let itemHeight:CGFloat = 0
    let menuWidth:CGFloat = 0
    let alpha:CGFloat = 0.5
    let radius:CGFloat = 20
    
    let colorHelper = ColorHelper()
    let fontHelper = FontHelper()
    let screenBounds = ScreenBounds()
    
    init(menuItems:[DPSideMenuItem],menuWidth:CGFloat,itemHeight:CGFloat, delegate:DPSideMenuDelegate, tableView:UITableView? = nil, tableTitle:String? = nil) {
            
        super.init()
        
        self.delegate = delegate
        self.menuWidth = menuWidth
        self.itemHeight = itemHeight
        self.menuItems = menuItems
        self.initCalculatedValues()
        
        // initialise menu items
        for var i = 0; i < self.menuItems.count; i++ {
            self.menuItems[i].delegate = self.delegate
            self.menuItems[i].itemIndex = i
            self.menuItems[i].height = self.itemHeight
            self.menuItems[i].width = self.menuWidth
            self.menuItems[i].parent = self
        }
        
        self.view = self.buildView()
        
        // unwrap tableview title if present
        if let unwrappedTitle = tableTitle {
            self.tableTitle = unwrappedTitle
        }
        
        // unwrap tableview if present
        if let unwrappedTableView = tableView {
            self.tvMenuTable = tableView
            self.addTableView()
        }
        
    }
    
    func initCalculatedValues() {
        self.menuHeight = screenBounds.height()
        self.menuXPos = screenBounds.width() - menuWidth
    }
    
    func buildView() -> UIView {
        
        // large button view used to hide menu
        let screenHeight = screenBounds.height()
        let screenWidth = screenBounds.width()
        
        let buttonView = UIButton(
            frame: CGRect(
                x: 0,
                y: 0,
                width: screenWidth,
                height: screenHeight)
        )
        
        // add tap outside menu action
        buttonView.addTarget(self,
            action: "didTapOutsideView:",
            forControlEvents: UIControlEvents.TouchUpInside
        )
        
        // need to minus height of nav bar?
        var menuView = UIView(
            frame: CGRect(
                x: self.menuXPos,
                y: 20,
                width: menuWidth,
                height: menuHeight)
        )
        
        menuView.backgroundColor = colorHelper.colorPrimary4()
        
        for var i = 0; i < self.menuItems.count; i++ {
            let itemView = self.menuItems[i].view()
            let itemViewY = CGFloat(i) * itemHeight
            itemView.frame.origin.y = itemViewY
            
            // calculate bottom of each item on each iteration
            let itemY = itemView.frame.origin.y
            self.yPosAfterLastItem = itemY + itemHeight
            
            menuView.insertSubview(itemView, atIndex: 9)
        }
        
        buttonView.addSubview(menuView)
        
        return buttonView
    }
    
    func showInView(view:UIView) {
        let parentView = view
        parentView.insertSubview(self.view, atIndex: 10)
    }
    
    func showInViewWithNavbar(#showIn:UIView, withNavBar:UINavigationBar) {
        let parentView = showIn
        let navBarView = withNavBar
        let statusBarHeight = UIApplication.sharedApplication().statusBarFrame.height
        let offset = 0
        
        if(!isExpanded) {
            //self.view.frame.origin.y = (navBarView.frame.height + statusBarHeight)
            //self.view.frame.origin.y = navBarView.frame.height
            self.view.frame.origin.y = 0 - statusBarHeight
            parentView.insertSubview(self.view, atIndex: 10)
            isExpanded = true
        }
        else {
            self.removeFromSuperView()
        }

    }
    
    func didTapOutsideView(sender:AnyObject) {
        self.removeFromSuperView()
    }
    
    func removeFromSuperView() {
        self.view.removeFromSuperview()
        isExpanded = false
    }
    
    func addTableView() {
        
        let titleYOffset:CGFloat = 20
        yPosAfterLastItem = yPosAfterLastItem + titleYOffset
        
        if let unwrappedTitle = self.tableTitle {

            // instantiate the table title if supplied
            var tableTitle = UIButton(
                frame: CGRect(
                    x: self.menuXPos,
                    y: yPosAfterLastItem,
                    width: self.menuWidth,
                    height: self.tableTitleHeight)
            )
            
            tableTitle.titleLabel!.font = fontHelper.fontPrimary(20)
            tableTitle.backgroundColor = colorHelper.colorPrimary5()
            tableTitle.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
            tableTitle.setTitle(unwrappedTitle, forState: UIControlState.Normal)
            tableTitle.contentHorizontalAlignment = UIControlContentHorizontalAlignment.Left
            tableTitle.titleEdgeInsets = UIEdgeInsetsMake(0, 20, 0, 0)
            
            yPosAfterLastItem = yPosAfterLastItem + self.tableTitleHeight
            
            self.view.addSubview(tableTitle)
        }
        
        // configure tableview's frame for adding to view
        var menuHeight:CGFloat = self.view.frame.height
        var tvHeight:CGFloat = self.view.frame.height - yPosAfterLastItem
        
        self.tvMenuTable!.frame = CGRect(
            x: self.menuXPos,
            y: yPosAfterLastItem,
            width: self.menuWidth,
            height: tvHeight
        )
        
        self.view.addSubview(self.tvMenuTable!)
    }
    
    
}