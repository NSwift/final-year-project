//
//  ApplicationState
//  DynamicAssessmentFramework
//
//  Created by Daniel Parkin on 01/03/2015.
//  Copyright (c) 2015 DanielParkin. All rights reserved.
//

enum ApplicationState {
    case Assessment
    case Review
    case Preview
}
