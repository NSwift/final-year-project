//
//  AssessmentReviewItemBuilder.swift
//  DynamicAssessmentFramework
//
//  Created by Daniel Parkin on 10/01/2015.
//  Copyright (c) 2015 DanielParkin. All rights reserved.
//

import UIKit

class AssessmentReviewItemBuilder : ActResponseDelegate {

    weak var assessment:Assessment!
    
    let CDA = CoreDataAccess()
    
    init(assessment:Assessment) {
        self.assessment = assessment
    }

    func buildReviewItems() -> [ActQuestion] {
        
        // create collection to hold generated scenes
        var acts:[ActQuestion] = []
        
        // get questions
        //var questions:[Question] = self.CDA.questionRepository().getQuestionsByAssessment(self.assessment, ascending: true)
        
        var questions:[Question] = self.CDA.questionRepository().getQuestionsRequiringAnswersByAssessment(self.assessment, ascending: true)
        
        // iterate over each question, build acts for each scene
        for question in questions {
            
            let currentQuestion = question as Question

            // add the question
            let actQuestion = ActQuestion(
                question: currentQuestion,
                delegate: self,
                appState: ApplicationState.Review
            )
            
            acts.append(actQuestion)
            
        }
        
        return acts
        
    }
    
    func actDidRespond(response: ActResponse) {
        
    }

    
}
