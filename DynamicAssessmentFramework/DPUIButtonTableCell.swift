//
//  DPUIButton.swift
//  DynamicAssessmentFramework
//
//  Created by Daniel Parkin on 05/01/2015.
//  Copyright (c) 2015 DanielParkin. All rights reserved.
//

import UIKit

class DPUIButtonTableCell: UIButton {
    
    var tableCellId:Int = 0
    var defaultColor:UIColor!
    let colorHelper = ColorHelper()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.defaultColor = self.colorHelper.colorPrimary2()
        self.backgroundColor = self.defaultColor
        
        // button fade in
        self.alpha = 0
        UIView.animateWithDuration(1.0,
            delay: 0.0,
            options: UIViewAnimationOptions.CurveEaseIn,
            animations: {
                self.alpha = 1.0
            }, completion: nil)
        
    }
    
    override var highlighted: Bool {
        didSet {
            
            if (highlighted) {
                self.backgroundColor = self.colorHelper.colorPrimary5()
            }
            else {
                self.backgroundColor = self.defaultColor
            }
        }
    }
    
    func setDefaultColor(color:UIColor) {
        self.defaultColor = color
        self.backgroundColor = self.defaultColor
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
}
