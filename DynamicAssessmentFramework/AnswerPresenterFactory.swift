//
//  AnswerPresenterFactory.swift
//  QuestionsTests
//
//  Created by Daniel Parkin on 19/08/2014.
//  Copyright (c) 2014 Echoyo. All rights reserved.
//

import Foundation

class AnswerPresenterFactory {
    
    func getPresenter(question:Question, appState:ApplicationState, delegate:AnswerPresenterDelegate) -> AnswerPresentationProtocol {

        switch(String(question.questionType)) {
        case "Buttons":
            return AnswerPresenterButtons(question:question, appState:appState, delegate: delegate)
        case "Slider":
            return AnswerPresenterSlider(question:question, appState:appState, delegate: delegate)
        case "Scale":
            return AnswerPresenterScaleStatic(question:question, appState:appState, delegate: delegate)
        case "Custom":
            return AnswerPresenterCustom(question: question, appState: appState, delegate: delegate)
        case "Not Required":
            return AnswerPresenterBlank(appState: appState, delegate: delegate)
        default:
            return AnswerPresenterButtons(question:question, appState: appState, delegate: delegate)

        }
        
    }
    
}