//
//  OrientationViewController.swift
//  DynamicAssessmentFramework
//
//  Created by Daniel Parkin on 09/01/2015.
//  Copyright (c) 2015 DanielParkin. All rights reserved.
//

import UIKit

class ParticipantSelectViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate  {
    
    weak var delegate:ParticipantSelectDelegate?
    var bufferedParticipants:[Participant] = []
    var participants:[Participant] = []
    
    let CDA = CoreDataAccess()
    
    @IBOutlet weak var tvParticipants: UITableView!
    @IBOutlet weak var tfSearch: UITextField!
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
    }
    
    /*
    @IBAction func cancelTapped(sender: AnyObject) {
        self.popViewController()
    }
    */
    
    // MARK: View methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // if participants haven't been initialised by another view
        // controller, load all participants
        if self.participants.count == 0 {
            self.participants = CDA.participantRepository().getAllParticipants()
        }
        
        self.refreshParticipants()
        
        self.tfSearch.delegate = self
        self.tfSearch.addTarget(self, action: "refreshParticipants", forControlEvents: UIControlEvents.EditingChanged)
    }
    
    // MARK: UITableViewDataSource methods
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return bufferedParticipants.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cellID = "ParticipantCell"
        
        var cell = tableView.dequeueReusableCellWithIdentifier(cellID) as UITableViewCell
        
        var participant = bufferedParticipants[indexPath.row] as Participant
        cell.textLabel.text = participant.id
        
        return cell
    }
    
    func tableView(tableView: UITableView!, didSelectRowAtIndexPath indexPath: NSIndexPath!) {
        
        // call back to delegate with selected user
        if let path = indexPath {
            var participant = bufferedParticipants[indexPath.row] as Participant
            self.popViewController()
            delegate!.didSelectParticipant(participant)
        }
    
    }
    
    // MARK: UITextFieldDelegate
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        return true
        
    }
    
    func refreshParticipants() {
        if tfSearch.text == "" {
            self.bufferedParticipants = self.participants
        } else {
            let filtered = self.participants.filter() { $0.id.contains(self.tfSearch.text) }
            self.bufferedParticipants = filtered
        }
        self.tvParticipants.reloadData()
    }
    
    func popViewController() {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    deinit {
        println("Participant select dealloc")
    }
    
}