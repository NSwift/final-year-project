//
//  Assessment+CDFixes.h
//  DynamicAssessmentFramework
//
//  Created by Daniel Parkin on 17/10/2014.
//  Copyright (c) 2014 DanielParkin. All rights reserved.
//

#import "Assessment.h"

@interface Assessment (CDFixes)

- (void)addQuestionsObject:(Question *)value;

@end
