//
//  AssessmentDirector.swift
//  DynamicAssessmentFramework
//
//  Created by Daniel Parkin on 27/08/2014.
//  Copyright (c) 2014 DanielParkin. All rights reserved.
//


import UIKit

class AssessmentDirector: NSObject, SceneResponseDelegate, CameraResponseDelegate, QuestionRecordingDelegate {
   
    weak var delegate:AssessmentResponseDelegate?
    weak var assessment:Assessment?
    var activeCameras:[String: CameraOperations] = [:]
    var activeCamera:CameraOperations?
    var cameraCount:Int32 = 0
    var allVideoStopped:Bool = false
    var currentSceneIndex:Int = 0
    var maxSceneIndex:Int = 0
    var outputFileCount:Int = 0
    var answerBundles:[Question: AnswerBundle] = [:]
    var camQuestionMap:[String:Question] = [:]
    var videoURLS:[Question: NSURL] = [:]
    var scenes:[AssessmentScene] = []
    var rootView:UIView!
    var sceneBuilder:AssessmentSceneBuilder!
    
    private let _CDA = CoreDataAccess()
    
    // constructor
    init(assessment:Assessment, rootView:UIView, delegate:AssessmentResponseDelegate) {
        super.init()
        self.assessment = assessment
        self.rootView = rootView
        self.delegate = delegate
        self.sceneBuilder = AssessmentSceneBuilder(assessment: self.assessment!, rootView: self.rootView, delegate: self, camDelegate: self)
        self.scenes = self.sceneBuilder.buildScenes()
        self.maxSceneIndex = self.scenes.count
    }
    
    // MARK: SceneResponseDelegate
    func sceneDidRespond(response: SceneResponse) {
        
        switch(response.status) {
            
        case SceneResponseStatus.CompletedWithBundle:
            
            let lastIndex:Int = currentSceneIndex - 1
            let currentScene:AssessmentScene = self.scenes[lastIndex]
            let currentQuestion = currentScene.question!
            
            answerBundles[currentQuestion] = response.bundle

            //activeCamera!.stopRecording()
            self.getNextScene()
            
        case SceneResponseStatus.CompleteGetNextScene:
            
            //activeCamera!.stopRecording()
            self.getNextScene()
            
        default:
            println("Scene error occured")
        }
        
    }
    
    // generics?
    func getNextScene() {
        
        // check if this is the last scene
        if currentSceneIndex < maxSceneIndex {
            
            // remove current scenes acts
            var currentScene = scenes[currentSceneIndex]
 
            currentScene.displayNextAct()

            self.removeActsFromLastScene()
            
            currentSceneIndex++
            
        }
        else {
            
            self.removeActsFromLastScene()
            
            NSLog("No more scenes to play, assessment finished.")
            
            // wait for all remaining video urls to return
            // check in background thread
            let priority = DISPATCH_QUEUE_PRIORITY_DEFAULT
            dispatch_async(dispatch_get_global_queue(priority, 0)) {
                
                let questions = self._CDA.questionRepository().getQuestionsByAssessment(self.assessment!, ascending: true)
                let questionsRequiringRecording = questions.filter() {$0.shouldRecord == true}
                let recordingCount = questionsRequiringRecording.count
            
                // wait for all video recording urls to be returned
                while(recordingCount != self.videoURLS.count) {
                    // loop until equal
                }
                
                // when all file urls have been received call back to main thread
                dispatch_async(dispatch_get_main_queue()) {
                    
                    // invoke method on main thread
                    self.assessmentComplete()
                }
            }
        
        }
        
    }
    
    func assessmentComplete()
    {
        // clear remaining views from the assessment and prepare
        // to return to the assessment view controller
        rootView.subviews.map { $0.removeFromSuperview() }
        
        if let lastActiveCam = activeCamera {
            activeCamera!.setDelegate(nil)
            activeCamera = nil
        }
        
        // remove delegate reference
        for (id, camera) in activeCameras {
            camera.setDelegate(nil)
        }
        
        // clear all cameras
        self.activeCameras = [:]
        
        // send response that assessment has finished
        var response = AssessmentResponse(
            status: AssessmentResponseStatus.CompletedWithAnswers,
            answerBundles: self.answerBundles,
            videoURLS: self.videoURLS)
        
        delegate?.assessmentDidRespond(response)
    }
    

    func createNewCamera() -> CameraOperations {
        
        var newCam = CameraOperations(delegate: self)
        
        // increment outputfile count
        outputFileCount++
        
        // incremement the camera count
        self.cameraCount++
        
        newCam.id = self.cameraCount
        newCam.fileNameID = Int32(outputFileCount)
        newCam.didFinishRecording = false
        
        return newCam
    }
    
    func removeDelegate() {
        self.delegate = nil
    }
    
    func removeActsFromLastScene() {
        if (currentSceneIndex > 0) {
            let previousIndex = currentSceneIndex - 1
            self.scenes[previousIndex].clearActs()
        }
    }
    
    func cameraDidFinishRecording(url: NSURL) {
        
        // get the id of the camera that is calling back from the returned url
        let urlString:String = url.absoluteString!
        let urlLength:Int = countElements(urlString)
        let idIndex = urlLength - 5
        
        // regex test
        var pattern = "(?<=cam&)(.*)(?=&)"
        var error: NSError? = nil
        
        var regex = NSRegularExpression(
            pattern: pattern,
            options: NSRegularExpressionOptions.DotMatchesLineSeparators,
            error: &error
        )
        
        var results:[NSTextCheckingResult] = regex?.matchesInString(
            urlString,
            options: nil,
            range: NSMakeRange(0, countElements(urlString))
            ) as [NSTextCheckingResult]
        
        let nsStringUrl = urlString as NSString
        var cameraId = nsStringUrl.substringWithRange(results[0].range)

        //let cameraId = String(urlString[advance(urlString.startIndex, idIndex)]) // get character at index 4
        //println("Camera ID: \(cameraId)")
        
        // get the reference to the question that maps to the camera calling back
        let question = camQuestionMap[cameraId]
        
        // add the NSURL object into the video urls dictionary against the correct question
        videoURLS[question!] = url
        
        // mark the camera as having called back ready for pruning
        self.activeCameras.removeValueForKey(cameraId)
        
        println("---------------------------------------------")
        println("The video URL count is \(self.videoURLS.count)")
        println("---------------------------------------------")
        
        println("Did complete with URL: \(url.absoluteString)")
    }
    
    // MARK: QuestionRecordingDelegate
    func beginRecordingQuestion(question: Question) {
        
        // create a new camera and add it to the camera dictionary
        let newCam = self.createNewCamera()
        let newCamId = String(newCam.id)
        
        // set the new camera as the active camera
        self.activeCamera = newCam
        
        // add the new camera to the active cameras collection
        self.activeCameras[newCamId] = newCam
        
        // map the new cameras id to the question being recorded
        self.camQuestionMap[newCamId] = question
        
        // set the new camera recording
        newCam.startRecording()
        
    }
    
    func stopRecordingQuestion(question: Question) {
        activeCamera!.stopRecording()
    }
    
    deinit {
        println("Assessment director dealloc")
    }

}
