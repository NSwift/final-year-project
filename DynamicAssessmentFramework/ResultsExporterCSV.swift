//
//  ResultsExporterCSV.swift
//  DynamicAssessmentFramework
//
//  Created by Daniel Parkin on 15/02/2015.
//  Copyright (c) 2015 DanielParkin. All rights reserved.
//

import Foundation

class ResultsExporterCSV  {
    
    // constants
    private let _CDA = CoreDataAccess()
    
    // private members
    private var _questions:[Question] = []
    private var _filenames:[Question:String] = [:]
    private var _assessment:Assessment!
    private var _filenameGenerators:[MediaFilenameGeneratorProtocol] = []
    private var _numberFormatter:NSNumberFormatter!
    
    init(assessment:Assessment) {
        self._assessment = assessment
        self._questions = _CDA.questionRepository().getQuestionsRequiringAnswersByAssessment(_assessment, ascending: true)
    
        _numberFormatter = NSNumberFormatter()
        _numberFormatter.numberStyle = NSNumberFormatterStyle.DecimalStyle
        _numberFormatter.maximumFractionDigits = 2
        _numberFormatter.roundingMode = NSNumberFormatterRoundingMode.RoundDown
    
    }
    
    func export() -> ResultsExportBundle {
        
        var outputFile = ""
        var headerRow = "Participant ID, DOB"
        var newLine = "\n"
        
        // build file header
        for (index, question) in enumerate(_questions) {
            let questionID = index + 1
            
            // filename
            // type - buttons / scale / slider / custom
            // response time
            // rate - if scale
            // answer 
            // correct answer if applicable
            
            let questionPrefix = "," + "Q\(questionID)"
            
            headerRow = headerRow + questionPrefix + " Filename"
            headerRow = headerRow + questionPrefix + " Answer Type"
            headerRow = headerRow + questionPrefix + " Media Type"
            headerRow = headerRow + questionPrefix + " Response Time"
            
            if question.mediaType != "Image" {
                headerRow = headerRow + questionPrefix + " Media Replays"
            }
            
            if question.questionType == "Scale" {
                headerRow = headerRow + questionPrefix + " Scale Size"
            }
            
            if question.correctAnswer != nil {
               headerRow = headerRow + questionPrefix + " Correct Answer"
            }
            
            // answer or score
            if question.questionType == "Scale" {
                headerRow = headerRow + questionPrefix + " Score"
            }
            else {
                headerRow = headerRow + questionPrefix + " Answer"
            }
            
            
        }
        
        // add the header row
        outputFile = outputFile + headerRow + newLine
        
        // begin building participant output
        let participants = _CDA.participantRepository().getParticipantsByAssessment(_assessment)
        
        // loop through each participant
        for participant in participants {
            
            // begin string with participants id
            var participantRow = participant.id + "," + participant.dateOfBirth
            
            // get results
            let results = _CDA.resultRepository().getResultsBy(participant: participant, assessment: _assessment)
            
            for result in results {
                
                participantRow = participantRow + "," + result.question.filename
                participantRow = participantRow + "," + result.question.questionType
                participantRow = participantRow + "," + result.question.mediaType
                
                participantRow = participantRow + "," + _numberFormatter.stringFromNumber(result.responseTime)!
                
                if result.question.mediaType != "Image" {
                    participantRow = participantRow + "," + "\(result.mediaReplays)"
                }
                
                if result.question.questionType == "Scale" {
                    participantRow = participantRow + "," + "\(result.question.scaleSize)"
                }
                
                if result.question.correctAnswer != nil {
                    participantRow = participantRow + "," + result.question.correctAnswer.value
                }
                
                participantRow = participantRow + "," + result.answer.value
            }
            
            participantRow = participantRow + newLine
            outputFile = outputFile + participantRow
            
        }
        
        let data:NSData = outputFile.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: false)!
        
        let filename = _assessment.title
        
        let resultsBundle = ResultsExportBundle(
            data: data,
            filename: filename,
            fileType: FileExportType.CSV
        )
        
        return resultsBundle
    }
    
}