//
//  ResultDeliveryEmail.swift
//  DynamicAssessmentFramework
//
//  Created by Daniel Parkin on 16/02/2015.
//  Copyright (c) 2015 DanielParkin. All rights reserved.
//

import MessageUI

class ResultsDeliveryEmail : NSObject, MFMailComposeViewControllerDelegate {
    
    private var _resultsBundle:ResultsExportBundle
    
    init(resultsBundle:ResultsExportBundle) {
        self._resultsBundle = resultsBundle
    }
    
    func show(parentViewController:UIViewController) {
        
        let mailComposeViewController = configuredMailComposeViewController()
        
        if MFMailComposeViewController.canSendMail() {
            
            parentViewController.presentViewController(
                mailComposeViewController,
                animated: true,
                completion: nil
            )
            
        } else {
            self.showSendMailErrorAlert()
        }
    }
    
    func configuredMailComposeViewController() -> MFMailComposeViewController {
        
        let mailComposer = MFMailComposeViewController()
        
        mailComposer.mailComposeDelegate = self
        
        //mailComposer.setToRecipients(["10412965@stu.mmu.ac.uk"])
        mailComposer.setSubject("Results")
        //mailComposer.setMessageBody("Test email", isHTML: false)
        
        mailComposer.addAttachmentData(
            _resultsBundle.data,
            mimeType: _resultsBundle.mimeType,
            fileName: _resultsBundle.filename
        )
        
        return mailComposer
    }
    
    func showSendMailErrorAlert() {
        
        let sendMailErrorAlert = UIAlertView(
            title: "Unable to send",
            message: "Unable to send your message. Please your configuration and try again.",
            delegate: self,
            cancelButtonTitle: "OK"
        )
        
        sendMailErrorAlert.show()
    }
    
    // MARK: MFMailComposeViewControllerDelegate
    func mailComposeController(controller: MFMailComposeViewController!, didFinishWithResult result: MFMailComposeResult, error: NSError!) {
        
        controller.dismissViewControllerAnimated(true, completion: nil)
        
    }
    
}