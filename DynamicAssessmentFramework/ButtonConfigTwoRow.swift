//
//  ButtonConfigOneRow.swift
//  DynamicAssessmentFramework
//
//  Created by Daniel Parkin on 28/02/2015.
//  Copyright (c) 2015 DanielParkin. All rights reserved.
//
import UIKit

class ButtonConfigTwoRow : NSObject, ButtonConfigViewProtocol {
    
    weak var delegate:ButtonConfigViewDelegate!
    private var _answers:[Answer]!
    private var _shouldAnimate:Bool!
    private var _appState:ApplicationState!
    private var _controls:[UIButton] = []
    
    private let _fontHelper = FontHelper()
    
    init(answers:[Answer], shouldAnimate:Bool, appState:ApplicationState) {
        self._answers = answers
        self._shouldAnimate = shouldAnimate
        self._appState = appState
    }
    
    func displayOnView(view: UIView) {
        
        // create NSArray in order to use indexOfObject for tag
        var array = NSArray()
        
        // loop through answers and create buttons
        // increment ypos on each iteration
        let screenBounds = ScreenBounds()
        let totalAnswers = _answers.count
        let horizontalSpacing:CGFloat = 20
        let verticalSpacing:CGFloat = 50
        let buttonWidth:CGFloat = 150
        let buttonHeight:CGFloat = 100
        var xPos:CGFloat = 10;
        
        let firstRowYOffset:CGFloat = 100
        let secondRowYOffset = firstRowYOffset + buttonHeight + verticalSpacing
        
        let firstRowAnswerTotal:CGFloat = 4
        let secondRowAnswerTotal = CGFloat(_answers.count) - firstRowAnswerTotal
        
        // create containing view for buttons so they can be centred
        let row1ButtonViewWidth = (firstRowAnswerTotal * (buttonWidth + horizontalSpacing))
        
        var row1ButtonView = UIView(
            frame: CGRect(
                x: 0,
                y: firstRowYOffset,
                width: row1ButtonViewWidth,
                height: buttonHeight
            )
        )
        
        //row1ButtonView.backgroundColor = UIColor.yellowColor()
        
        let row2ButtonViewWidth = (secondRowAnswerTotal * (buttonWidth + horizontalSpacing))
        
        var row2ButtonView = UIView(
            frame: CGRect(
                x: 0,
                y: secondRowYOffset,
                width: row2ButtonViewWidth,
                height: buttonHeight
            )
        )
        
        //row2ButtonView.backgroundColor = UIColor.orangeColor()
        
        screenBounds.positionViewHorizontalCentral(
            thisView: row1ButtonView,
            inCenterOf: view
        )
        
        screenBounds.positionViewHorizontalCentral(
            thisView: row2ButtonView,
            inCenterOf: view
        )
        
        // populate first button view
        //
        for(var i=0; i < 4; i++) {
            
            let button = createButton(
                xPos,
                width: buttonWidth,
                height: buttonHeight,
                answer: _answers[i]
            )
            
            // add controls to array to retain reference
            _controls.append(button)
            
            row1ButtonView.addSubview(button)
            
            // increment yPos
            xPos += (buttonWidth + horizontalSpacing)
        }
        
        // reset xPos
        xPos = 10
        
        // populate second button view
        let answerCount = _answers.count
        
        for(var i=4; i < answerCount; i++) {
            
            let button = createButton(
                xPos,
                width: buttonWidth,
                height: buttonHeight,
                answer: _answers[i]
            )
            
            // add controls to array to retain reference
            _controls.append(button)
            
            row2ButtonView.addSubview(button)
            
            // increment yPos
            xPos += (buttonWidth + horizontalSpacing)
        }
        
        // overlay answers if neccessary
        if _appState == ApplicationState.Review {
            let answerOverlay = AssessmentAnswerOverlay()
            answerOverlay.overlayAnswerOnView(row1ButtonView, controls: _controls)
        }
        
        view.addSubview(row1ButtonView)
        view.addSubview(row2ButtonView)
        
    }
    
    func createButton(xPos:CGFloat, width:CGFloat, height:CGFloat, answer:Answer) -> UIButton {
        
        // button
        var button:DPUIButtonAnswer = DPUIButtonAnswer(
            frame: CGRect(
                x: xPos,
                y: 0,
                width: width,
                height: height),
            rounded:true,
            animates: _shouldAnimate
        )
        
        // assign the answer to the button
        button.answer = answer
        
        button.backgroundColor = UIColor.whiteColor()
        
        button.setTitleColor(UIColor.blackColor(), forState: UIControlState.Normal)
        
        button.setTitle(answer.value, forState: UIControlState.Normal)
        button.addTarget(self, action: "provideAnswer:", forControlEvents: UIControlEvents.TouchUpInside)
        
        button.clipsToBounds = true
        
        button.exclusiveTouch = true
        
        /*
        button.titleLabel!.minimumScaleFactor = 0.01
        button.titleLabel!.adjustsFontSizeToFitWidth = true
        button.titleLabel!.font = UIFont.systemFontOfSize(60)
        */
        
        button.titleLabel!.font = _fontHelper.fontPrimary(20)
        button.titleLabel!.numberOfLines = 3
        button.titleLabel!.adjustsFontSizeToFitWidth = true
        button.titleLabel!.lineBreakMode = NSLineBreakMode.ByWordWrapping
        button.titleLabel!.textAlignment = NSTextAlignment.Center
        
        return button
    }
    
    func provideAnswer(sender:AnyObject) {
        if sender is DPUIButtonAnswer {
            
            let answerButton = sender as DPUIButtonAnswer
            let selectedAnswer = answerButton.answer!
            
            delegate.buttonViewDidProvideAnswer(selectedAnswer)
        }
    }
    
}
