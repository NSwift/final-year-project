//
//  AnswerPresenter.swift
//  QuestionsTests
//
//  Created by Daniel Parkin on 19/08/2014.
//  Copyright (c) 2014 Echoyo. All rights reserved.
//

import Foundation
import UIKit

class AnswerPresenterButtons:NSObject, AnswerPresentationProtocol, ButtonConfigViewDelegate {
    
    weak var question:Question?
    weak var selectedAnswer:Answer?
    weak var delegate:AnswerPresenterDelegate?
    var controls:[NSObject]
    var screenBounds:ScreenBounds
    var shouldAnimate:Bool = false

    private var _appState:ApplicationState!
    private var _buttonConfigView:ButtonConfigViewProtocol!
    
    private let _screenBounds = ScreenBounds()
    private let _fontHelper = FontHelper()
    
    let answerOverlay = AssessmentAnswerOverlay()
    
    init(question:Question, appState:ApplicationState, delegate:AnswerPresenterDelegate) {
        self.question = question
        self.controls = [NSObject]()
        self.delegate = delegate
        self.screenBounds = ScreenBounds()
        self._appState = appState
        self.shouldAnimate = _appState == ApplicationState.Review ? false : true
    }
    
    func displayOnView(view: UIView) {
        
        var answerView:UIView = UIView(frame: screenBounds.bottomHalf())
        
        answerView.backgroundColor = UIColor.grayColor()

        let horizontalPadding:CGFloat = 40
        
        // label setup
        var label:UILabel = UILabel(
            frame: CGRect(
                x: 0,
                y: 20,
                width: screenBounds.bounds().width - horizontalPadding,
                height: 60
            )
        )
        
        _screenBounds.positionViewHorizontalCentral(
            thisView: label,
            inCenterOf: answerView
        )
        
        label.text = question?.questionText
        label.font = _fontHelper.fontPrimary(40)
        label.textColor = UIColor.whiteColor()
        label.numberOfLines = 0
        label.textAlignment = NSTextAlignment.Center
        label.adjustsFontSizeToFitWidth = true
        label.minimumScaleFactor = 0
   
        let answers = question!.answers.array as [Answer]
        let buttonConfigFact = ButtonConfigFactory()
        
        _buttonConfigView = buttonConfigFact.getButtonConfig(
            answers,
            shouldAnimate: shouldAnimate,
            appState: _appState
        )
        
        _buttonConfigView.delegate = self
        _buttonConfigView.displayOnView(answerView)
        
        answerView.addSubview(label)
        
        view.addSubview(answerView)
    }
    
    func buttonViewDidProvideAnswer(answer: Answer) {
        delegate?.didProvideAnswer(answer)
    }
    
    // no implementation
    func valueFromControl(sender:AnyObject) {
    }
    
}