//
//  AnswerPresenter.swift
//  QuestionsTests
//
//  Created by Daniel Parkin on 19/08/2014.
//  Copyright (c) 2014 Echoyo. All rights reserved.
//

import Foundation
import UIKit

class AnswerPresenterSlider:NSObject, AnswerPresentationProtocol {
    
    weak var delegate:AnswerPresenterDelegate?
    weak var selectedAnswer:Answer?
    weak var question:Question?
    var screenBounds:ScreenBounds
    var selectedIndex:Float
    var controls:[NSObject]
    
    private var _appState:ApplicationState
    
    private let _screenBounds = ScreenBounds()
    private let _fontHelper = FontHelper()
    
    let colorHelper = ColorHelper()
    let answerOverlay = AssessmentAnswerOverlay()
    
    init(question:Question, appState:ApplicationState, delegate:AnswerPresenterDelegate) {
        self.question = question
        self.selectedIndex = 0
        self.controls = [NSObject]()
        self.delegate = delegate
        self._appState = appState
        self.screenBounds = ScreenBounds()
    }

    func displayOnView(view: UIView) {
        
        // initial view setup
        var answerView:UIView = UIView(frame: screenBounds.bottomHalf())
        //var answerView = UIView(frame: CGRect(x: 0, y: 250, width: 320, height: 480))
        
        answerView.backgroundColor = UIColor.grayColor()
        
        let horizontalPadding:CGFloat = 40
        
        // label setup
        var label:UILabel = UILabel(
            frame: CGRect(
                x: 0,
                y: 20,
                width: screenBounds.bounds().width - horizontalPadding,
                height: 50
            )
        )
        
        _screenBounds.positionViewHorizontalCentral(
            thisView: label,
            inCenterOf: answerView
        )
        
        label.text = question?.questionText
        label.font = _fontHelper.fontPrimary(40)
        label.textColor = UIColor.whiteColor()
        label.numberOfLines = 0
        label.textAlignment = NSTextAlignment.Center
        label.adjustsFontSizeToFitWidth = true
        label.minimumScaleFactor = 0
        
        // button setup
        var buttonWidth:CGFloat = 200
        var buttonHeight:CGFloat = 100
        
        var buttonPos = screenBounds.centreFromDimensions(width: buttonWidth, height: buttonHeight)
        
        var button:DPUIButton = DPUIButton(
            frame: CGRect(
                x: buttonPos.x,
                y: 350,
                width: buttonWidth,
                height: buttonHeight),
            rounded: true,
            animates: true
        )
        
        button.backgroundColor = UIColor.whiteColor()
        button.setTitleColor(UIColor.blackColor(), forState: UIControlState.Normal)
        
        button.setTitle("Continue", forState: UIControlState.Normal)
        button.titleLabel!.font = _fontHelper.fontPrimary(30)
        button.addTarget(self, action: "valueFromControl:", forControlEvents: UIControlEvents.TouchUpInside)
        
        // setup slider
        // get count of answers in array
        var arrayCount:Int = 0
        
        if let existingQuestion = question {
            arrayCount = existingQuestion.answers.count
        }
        
        
        var sliderWidth:CGFloat = 400
        var sliderHeight:CGFloat = 100
        
        var sliderPos = screenBounds.centreFromDimensions(width: sliderWidth, height: sliderHeight)
        
        var slider = UISlider(frame: CGRect(x: sliderPos.x, y: 100, width: sliderWidth, height: sliderHeight))
        slider.tintColor = self.colorHelper.colorPrimary4()
        
        // set slider tag
        slider.tag = 2
        
        // set slider min and max values
        slider.minimumValue = 0
        slider.maximumValue = Float(arrayCount - 1)
        slider.value = selectedIndex
        
        // add sub labels to slider
        
        // get y position of slider
        var sublabelY = slider.frame.origin.y + 80
        var sliderX = slider.frame.origin.x
        var sectionWidth = sliderWidth / CGFloat(arrayCount)
        
        for var i = 0; i < arrayCount; i++ {
            
            // get answer from current iteration to get value
            //let answer:Answer = question.answers.array as Answer
            let answers = question?.answers.array as [Answer]
            let answer = answers[i]
            
            var sublabelX = sliderX + (sectionWidth * CGFloat(i))
            var sublabel:DPUILabelAnswer = DPUILabelAnswer(frame: CGRect(x: sublabelX, y: sublabelY, width: 100, height: 100))
            
            sublabel.answer = answer
            sublabel.text = answer.value
            sublabel.font = _fontHelper.fontPrimary(16)
            sublabel.textColor = UIColor.whiteColor()
            sublabel.numberOfLines = 0
            sublabel.textAlignment = NSTextAlignment.Left
            
            sublabel.transform = CGAffineTransformMakeRotation (2/2);
            
            // append to controls to retain
            controls.append(sublabel)
            
            answerView.addSubview(sublabel)
            
        }
        
        // add slider target
        slider.addTarget(self, action: "sliderDidChange:", forControlEvents: UIControlEvents.ValueChanged)
        
        // add label to show changing answer
        var answerLabel = UILabel(frame: CGRect(x: 0, y: 250, width: screenBounds.bounds().width, height: 100))
    
        let initialAnswer = question?.answers.array[Int(selectedIndex)] as Answer
        
        answerLabel.text = initialAnswer.value
        answerLabel.font = _fontHelper.fontPrimary(30)
        answerLabel.textColor = UIColor.whiteColor()
        answerLabel.numberOfLines = 0
        answerLabel.textAlignment = NSTextAlignment.Center
        answerLabel.tag = 3
        
        // add controls to array to retain
        controls.append(label)
        controls.append(button)
        controls.append(slider)
        controls.append(answerLabel)
        
        // add controls to view
        answerView.addSubview(label)
        answerView.addSubview(slider)
        
        // if we're in review mode we don't want the continue 
        // button and answer label
        if _appState != ApplicationState.Review {
            answerView.addSubview(button)
            answerView.addSubview(answerLabel)
        }
        
        // overlay answers if neccessary
        if _appState == ApplicationState.Review {
            self.answerOverlay.overlayAnswerOnView(answerView, controls: self.controls)
        }

        view.addSubview(answerView)
        
    }

    func valueFromControl(sender:AnyObject) {
        if sender is UIButton {

            let selectedAnswer = question?.answers.array[Int(selectedIndex)] as Answer

            delegate?.didProvideAnswer(selectedAnswer)
            
        }
    }
    
    func sliderDidChange(sender:AnyObject) {
        if sender is UISlider {
            let slider = sender as UISlider
            var index = Int(slider.value)
            selectedIndex = slider.value
            //println("Index: \(index)")
            for control in controls {
                if control is UILabel {
                    let label = control as UILabel
                    if label.tag == 3 {
                        //let selectedAnswer = question.answers.allObjects[index] as Answer
                        
                        let selectedAnswer = question?.answers.array[index] as Answer
                        label.text = selectedAnswer.value
                    }
                }
            }
        }
    }
    
}