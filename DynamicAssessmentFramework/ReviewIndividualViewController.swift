//
//  ReviewIndividualViewController.swift
//  DynamicAssessmentFramework
//
//  Created by Daniel Parkin on 15/01/2015.
//  Copyright (c) 2015 DanielParkin. All rights reserved.
//

import UIKit

class ReviewIndividualViewController : UIViewController, UITableViewDelegate, UITableViewDataSource, DPSideMenuDelegate, MediaPlayerDelegate {
    
    var participant:Participant!
    var assessment:Assessment!
    
    private var tvParticipants:UITableView!
    private var sideMenu:DPSideMenu!
    private var results:[Result] = []
    private var participants:[Participant] = []

    private var _mediaPlayers:[MediaPlayerProtocol] = []
    private var _infoView:DPInfoView!
    
    let menuWidth:CGFloat = 300
    let menuItemHeight:CGFloat = 80
    let participantCellHeight:CGFloat = 50
    let resultRowHeight:CGFloat = 200
    
    let CDA = CoreDataAccess()
    let fontHelper = FontHelper()
    let colorHelper = ColorHelper()
    let mediaPlayerFact = MediaPlayerFactory()
    
    @IBOutlet weak var tvIndividualResults: UITableView!
    @IBOutlet weak var lvParticipantID: UILabel!
    @IBOutlet weak var lvParticipantDOB: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // setup individual results table view
        self.tvIndividualResults.delegate = self
        self.tvIndividualResults.dataSource = self
        
        // refresh results table view
        self.refreshIndividualResults()
        
        // setup participants table view
        self.initParticipantsTableView()
        
        // init side menu
        self.sideMenu = self.initSideMenu()
        
        // adding bar button item
        var barButtonItem = UIBarButtonItem(
            title: "m",
            style: UIBarButtonItemStyle.Plain,
            target: self,
            action: "menuTapped"
        )
        
        barButtonItem.setTitleTextAttributes(
            [NSFontAttributeName: fontHelper.fontGlyphicons(25)],
            forState: UIControlState.Normal
        )
        
        self.navigationItem.rightBarButtonItem = barButtonItem
        
    }
    
    // MARK: Set participant details
    func setParticipantDetails() {
        self.lvParticipantID.text = self.participant.id
        self.lvParticipantDOB.text = self.participant.dateOfBirth
    }
    
    // MARK: Refresh individual results
    func refreshIndividualResults() {
        
        // get results for individual
        self.results = CDA.resultRepository().getResultsBy(
            participant: self.participant,
            assessment: self.assessment
        )
        
        self.tvIndividualResults.reloadData()
        self.setParticipantDetails()
    }
    
    // MARK: Init participants table view
    func initParticipantsTableView() {
        
        self.tvParticipants = UITableView(
            frame: CGRect(
                x: 0,
                y: 0,
                width: self.menuWidth,
                height: 200)
        )
        
        // get the participants for current assessment
        self.participants = CDA.participantRepository().getParticipantsByAssessment(self.assessment)
        
        self.tvParticipants.backgroundColor = colorHelper.colorPrimary05()
        
        self.tvParticipants.delegate = self
        self.tvParticipants.dataSource = self
        self.view.addSubview(self.tvParticipants)
    }
    
    // MARK: UITableViewDelegate
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        var rowCount = 0
        
        if tableView.isEqual(self.tvIndividualResults) {
            rowCount = self.results.count
        }
        
        if tableView.isEqual(self.tvParticipants) {
            rowCount = self.participants.count
        }
        
        return rowCount
        
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        var returnCell:UITableViewCell = UITableViewCell()
        
        if tableView.isEqual(self.tvIndividualResults) {
            let cell = tvIndividualResults.dequeueReusableCellWithIdentifier("IndividualResultCell") as UITableViewCell
            
            let result:Result = self.results[indexPath.row]
            
            let lvQuestion = cell.viewWithTag(1) as UILabel
            let lvAnswer = cell.viewWithTag(2) as UILabel
            let lvCorrectAnswer = cell.viewWithTag(9) as UILabel
            let lvResponseTime = cell.viewWithTag(3) as UILabel
            let lvMediaReplays = cell.viewWithTag(4) as UILabel
            let lvFilename = cell.viewWithTag(5) as UILabel
            let lvRecIcon = cell.viewWithTag(6) as UILabel
            let lvViewRecording = cell.viewWithTag(7) as UILabel
            let uvMediaPreview = cell.viewWithTag(8) as UIView!
            
            
            // generate correct - correct answer string
            var correctAnswerString = ""
            
            if let correctAnswer = result.question!.correctAnswer {
                correctAnswerString = String("\(correctAnswer.value)")
            }
            else {
                correctAnswerString = "N/A"
            }
            
            // round preview corners
            uvMediaPreview.layer.cornerRadius = 20
            uvMediaPreview.layer.masksToBounds = true
            
            if let recordingUrl = result.recordingUrl {
                //
            }
            else {
                lvRecIcon.hidden = true
                lvViewRecording.hidden = true
            }
            
            let assetURL = NSURL(string: result.question.assetURL)
            
            var mediaPlayer = mediaPlayerFact.getPlayer(
                ApplicationState.Preview,
                type: result.question!.mediaType,
                delegate: self
            )
            
            // add newly instantiated media player to the media player collection to keep retain count
            _mediaPlayers.append(mediaPlayer)
            
            mediaPlayer.playMedia(assetURL!, onView: uvMediaPreview)
            
            var formatter = NSNumberFormatter()
            formatter.numberStyle = NSNumberFormatterStyle.DecimalStyle
            formatter.maximumFractionDigits = 2
            formatter.roundingMode = NSNumberFormatterRoundingMode.RoundDown
            
            lvQuestion.text = result.question!.questionText
            lvAnswer.text = "Answer: " + result.answer!.value
            lvCorrectAnswer.text = "Correct answer: \(correctAnswerString)"
            lvResponseTime.text = "Response time: " + formatter.stringFromNumber(result.responseTime)! + "s"
            
            lvMediaReplays.text = "Media replays: " + result.mediaReplays.stringValue
            lvFilename.text = "Filename: " + result.question.filename
            
            returnCell = cell
        }
        
        if tableView.isEqual(self.tvParticipants) {
        
            let cellIdentifier = "participantCell"
            
            // variable type is inferred
            var cell = tableView.dequeueReusableCellWithIdentifier(cellIdentifier) as? DPCellBasic
            
            if cell == nil {
                
                // instantiate custom cell
                cell = DPCellBasic(
                    style: UITableViewCellStyle.Value1,
                    reuseIdentifier: cellIdentifier,
                    height: self.participantCellHeight,
                    width: self.menuWidth
                )
            }
            
            if let unwrappedCell = cell {
                unwrappedCell.lvTitle.text = self.participants[indexPath.row].id
                returnCell = unwrappedCell
            }
            
        }
        
        return returnCell
    }
    
    func tableView(tableView: UITableView!, didSelectRowAtIndexPath indexPath: NSIndexPath!) {
        
        if tableView.isEqual(self.tvIndividualResults) {
            self.tvIndividualResults.deselectRowAtIndexPath(indexPath!, animated: true)
            
            let result:Result = self.results[indexPath.row]
            
            if let recordingUrl = result.recordingUrl {
                var recordingView = self.storyboard?.instantiateViewControllerWithIdentifier("ReviewRecordingViewController") as ReviewRecordingViewController
                
                recordingView.result = results[indexPath.row] as Result
                
                self.navigationController?.pushViewController(recordingView, animated: true)
            }
            else {
                _infoView = DPInfoView(
                    messages: ["No video recording available."],
                    type: InfoViewType.Info
                )
                
                _infoView.showInView(self.view)
            }
            
        }
        
        if tableView.isEqual(self.tvParticipants) {
            self.tvParticipants.deselectRowAtIndexPath(indexPath!, animated: true)
            
            self.participant = self.participants[indexPath.row]
            self.refreshIndividualResults()
        }
        
    }
    
    func tableView(tableView:UITableView!, heightForRowAtIndexPath indexPath:NSIndexPath)->CGFloat
    {
        var rowSize:CGFloat = 0
        
        if tableView.isEqual(self.tvIndividualResults) {
            rowSize = self.resultRowHeight
        }
        
        if tableView.isEqual(self.tvParticipants) {
            rowSize = self.participantCellHeight
        }
        
        return rowSize
    }
    
    // MARK: DPSideMenu
    func initSideMenu() -> DPSideMenu {
        
        let menuItemEverybody = DPSideMenuItem(itemText: "Everybody", iconCharacter: "a")
        let menuItemExport = DPSideMenuItem(itemText: "Export", iconCharacter: "x")
        let menuItemHide = DPSideMenuItem(itemText: "Hide", iconCharacter: "h")
        
        let menuItems = [menuItemEverybody, menuItemHide]
        
        let sideMenu = DPSideMenu(
            menuItems: menuItems,
            menuWidth: self.menuWidth,
            itemHeight:self.menuItemHeight,
            delegate: self,
            tableView: tvParticipants,
            tableTitle: "Select Participant")
        
        return sideMenu
    }
    
    // MARK: DPSideMenu tapped
    func menuTapped() {
        self.sideMenu.showInViewWithNavbar(
            showIn: self.view,
            withNavBar: self.navigationController!.navigationBar
        )
    }
    
    // MARK: DPSideMenuDelegate
    func didSelectMenuItem(itemIndex:Int) {

        switch (itemIndex) {
        case 0:
            self.navigationController?.popViewControllerAnimated(true)
            
        case 1:
            println("export")
            
        case 2:
            self.menuTapped()
            
        default:
            println("default option")
        }
    }
    
    // MARK: MediaPlayerDelegate
    func didFinishPlayingMedia() {
        println("Media finished")
    }
    
}
