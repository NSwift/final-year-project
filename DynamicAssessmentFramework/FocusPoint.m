//
//  FocusPoint.m
//  DynamicAssessmentFramework
//
//  Created by Daniel Parkin on 17/02/2015.
//  Copyright (c) 2015 DanielParkin. All rights reserved.
//

#import "FocusPoint.h"
#import "Assessment.h"


@implementation FocusPoint

@dynamic assetURL;
@dynamic backgroundColor;
@dynamic height;
@dynamic rotation;
@dynamic scale;
@dynamic width;
@dynamic x;
@dynamic y;
@dynamic parentAssessment;

@end
