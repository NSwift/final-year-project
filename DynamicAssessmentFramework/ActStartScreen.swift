//
//  ActCountdown.swift
//  DynamicAssessmentFramework
//
//  Created by Daniel Parkin on 21/08/2014.
//  Copyright (c) 2014 Echoyo. All rights reserved.
//

import UIKit

class ActStartScreen:NSObject, AssessmentActProtocol {
    
    weak var delegate:ActResponseDelegate?

    private let _screenBounds = ScreenBounds()
    private let _colorHelper = ColorHelper()
    private let _fontHelper = FontHelper()
    
    private let _fontSize:CGFloat = 160
    
    init(delegate:ActResponseDelegate) {
        self.delegate = delegate
    }
    
    func beginDisplayOnView(view: UIView) {
        
        let actView = UIView(frame: _screenBounds.bounds())
        actView.backgroundColor = UIColor.grayColor()
        
        var button = UIButton(frame: CGRect(x: 0, y: 0, width: 450, height: 300))
        
        button.titleLabel!.font = _fontHelper.fontPrimary(_fontSize)
        button.contentHorizontalAlignment = UIControlContentHorizontalAlignment.Center
        
        button.setTitleColor(
            _colorHelper.colorPrimary4(),
            forState: UIControlState.Normal
        )
        
        button.setTitle("START", forState: UIControlState.Normal)
        
        button.addTarget(
            self,
            action: "didPressStart:",
            forControlEvents: UIControlEvents.TouchUpInside
        )
        
        _screenBounds.positionViewCentral(
            thisView: button,
            inCenterOf: actView
        )
        
        actView.addSubview(button)
        
        view.addSubview(actView)

    }
    
    func didPressStart(sender:AnyObject) {
        let status = ActResponseStatus.CompleteGetNextAct
        let response = ActResponse(status: status, bundle: nil)
        delegate?.actDidRespond(response)
    }
    
}