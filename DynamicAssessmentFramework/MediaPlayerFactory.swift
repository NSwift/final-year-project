//
//  AnswerPresenterFactory.swift
//  QuestionsTests
//
//  Created by Daniel Parkin on 19/08/2014.
//  Copyright (c) 2014 Echoyo. All rights reserved.
//

import Foundation

class MediaPlayerFactory {
  
    func getPlayer(appState:ApplicationState, type:String, delegate:MediaPlayerDelegate) -> MediaPlayerProtocol {
        
        switch(type) {
        case "Audio":
            return MediaPlayerAudio(appState: appState, delegate: delegate)
        case "Image":
            return MediaPlayerImage(appState: appState, delegate: delegate)
        case "Video":
            return MediaPlayerVideo(appState: appState, delegate: delegate)
        default:
            return MediaPlayerAudio(appState: appState, delegate: delegate)
        }
        
    }
    
}