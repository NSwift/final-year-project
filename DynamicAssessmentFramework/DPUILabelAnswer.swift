//
//  DPUILabel.swift
//  DynamicAssessmentFramework
//
//  Created by Daniel Parkin on 05/01/2015.
//  Copyright (c) 2015 DanielParkin. All rights reserved.
//

import UIKit

class DPUILabelAnswer: UILabel, AnswerProvidingControlProtocol {
    
    weak var answer:Answer?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func provideAnswer() -> Answer {
        return self.answer!
    }
    
    func getFrame() -> CGRect {
        return self.frame
    }
    
    func getView() -> UIView {
        return self
    }
    
}